<?php

function load_js($fjs = []){
	$scripts = "";
	foreach ($fjs as $js) {
		$js = base_url()."public/".$js;
		$scripts .= "<script src='{$js}'></script>";
	}
	echo $scripts;
}
function load_css($fcss = []){
	$links = "";
	foreach ($fcss as $css) {
		$css = base_url()."public/".$css;
		$links .= "<link rel='stylesheet' href='{$css}'>";
	}
	echo $links;
}

?>
