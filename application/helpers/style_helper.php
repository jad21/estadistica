<?php
/**
 * 
 * User: LS
 * Date: 15-6-20
 * Time: 下午11:52
 */

/**
 * 传入nav的item名称,判断该nav item是不是当前被选中
 * @param $navItem
 * @return string
 */
function get_active_nav_class($navItem) {

    $curAction = current_action();

    if(strpos($curAction, $navItem) !== false) {
        return 'active';
    }

    return '';
}