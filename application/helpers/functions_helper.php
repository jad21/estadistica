<?php
/**
 * 
 * User: LS
 * Date: 15-6-22
 * Time: 下午3:25
 */


/**
 * 过滤数据
 * @param $value
 * @param bool $htmlspecialchars
 * @return array|string
 */

function addslashes_deep($value,$htmlspecialchars=false)
{
    if (empty($value))
    {
        return $value;
    }
    else
    {
        if(is_array($value))
        {
            foreach($value as $key => $v)
            {
                unset($value[$key]);

                if($htmlspecialchars==true)
                {
                    $key=get_magic_quotes_gpc()? addslashes(stripslashes(htmlspecialchars($key,ENT_NOQUOTES))) : addslashes(htmlspecialchars($key,ENT_NOQUOTES));
                }
                else{
                    $key=get_magic_quotes_gpc()? addslashes(stripslashes($key)) : addslashes($key);
                }

                if(is_array($v))
                {
                    $value[$key]=addslashes_deep($v);
                }
                else{
                    if($htmlspecialchars==true)
                    {
                        $value[$key]=get_magic_quotes_gpc()? addslashes(stripslashes(htmlspecialchars($v,ENT_NOQUOTES))) : addslashes(htmlspecialchars($v,ENT_NOQUOTES));
                    }
                    else{
                        $value[$key]=get_magic_quotes_gpc()? addslashes(stripslashes($v)) : addslashes($v);
                    }
                }
            }
        }
        else{
            if($htmlspecialchars==true)
            {
                $value=get_magic_quotes_gpc()? addslashes(stripslashes(htmlspecialchars($value,ENT_NOQUOTES))) : addslashes(htmlspecialchars($value,ENT_NOQUOTES));
            }
            else{
                $value=get_magic_quotes_gpc()? addslashes(stripslashes($value)) : addslashes($value);
            }
        }

        return $value;
    }
}

/**
 * 两层stripslashes
 * @param $str
 * @return string
 */
function stripslashes_deep($str, $needReplace = true) {
    $str = stripslashes(stripslashes($str));
    if(!$needReplace) {
        return $str;
    }

    // > : markdown 里面的引用
    $str = str_replace('&amp;gt;', '>', $str);
    $str = str_replace('&amp;lt;', '<', $str);
    return $str;
}