<?php $this->load->view('common/admin_header');?>

      <!-- START Main section-->
      <section>
         <!-- START Page content-->
         <section class="main-content">
            <h3>Bienvenidos al sistema Estadístico LA CASA
               <br>
               <small>Bienvenidos <?php echo $curUser['nombre'];?></small>
            </h3>
            <div class="row">
               <!-- START dashboard main content-->
               <div class="col-md-12">
                  <!-- START table-->
                  <div class="row">
                     <div class="col-lg-12">
                        <!-- START panel-->
                        <div class="panel panel-default">
                           <div class="panel-heading">Estatus
                              <!-- <a href="#" data-perform="panel-dismiss" data-toggle="tooltip" title="Close Panel" class="pull-right">
                                 <em class="fa fa-times"></em>
                              </a> -->
                              <a href="#" data-perform="panel-collapse" data-toggle="tooltip" title="Collapse Panel" class="pull-right">
                                 <em class="fa fa-minus"></em>
                              </a>
                           </div>
                           <!-- START table-responsive-->
                           <div class="table-responsive">
                              <table class="table table-striped table-bordered table-hover">
                                 <thead>
                                    <tr>
                                       <th>Modulos</th>
                                       <th>Progreso</th>
                                       <th>Ultima vez</th>
                                       <!-- <th>Action</th> -->
                                    </tr>
                                 </thead>
                                 <tbody>
                                    <tr>
                                       <td>Planificación</td>
                                       <td>
                                          <div class="progress progress-striped progress-xs">
                                             <div role="progressbar" aria-valuenow="98" aria-valuemin="0" aria-valuemax="100" style="width: 98%;" class="progress-bar progress-bar-success">
                                                <span class="sr-only">98% Complete</span>
                                             </div>
                                          </div>
                                       </td>
                                       <td>
                                          <em class="fa fa-calendar fa-fw text-muted"></em>04/11/2015
                                       </td>
                                    </tr>
                                    <tr>
                                       <td>Distribución.</td>
                                       <td>
                                          <div class="progress progress-striped progress-xs">
                                             <div role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%;" class="progress-bar progress-bar-danger">
                                                <span class="sr-only">80% Complete</span>
                                             </div>
                                          </div>
                                       </td>
                                       <td>
                                          <em class="fa fa-calendar fa-fw text-muted"></em>08/11/2015
                                       </td>
                                    </tr>
                                    <!-- <tr>
                                       <td>Nullam sit amet magna vestibulum libero dapibus iaculis.</td>
                                       <td>
                                          <div class="progress progress-striped progress-xs">
                                             <div role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 50%;" class="progress-bar progress-bar-info">
                                                <span class="sr-only">50% Complete</span>
                                             </div>
                                          </div>
                                       </td>
                                       <td>
                                          <em class="fa fa-calendar fa-fw text-muted"></em>05/10/2014</td>
                                       <td class="text-center">
                                          <div class="btn-group"><a href="#" data-toggle="dropdown" class="dropdown-toggle"><i class="fa fa-cog"></i></a>
                                             <ul class="dropdown-menu pull-right text-left">
                                                <li><a href="#">Action</a>
                                                </li>
                                                <li><a href="#">Another action</a>
                                                </li>
                                                <li><a href="#">Something else here</a>
                                                </li>
                                                <li class="divider"></li>
                                                <li><a href="#">Separated link</a>
                                                </li>
                                             </ul>
                                          </div>
                                       </td>
                                    </tr> -->
                                 </tbody>
                              </table>
                           </div>
                           <!-- END table-responsive-->
                           <div class="panel-footer text-right">
                              <!-- <a href="#">
                                 <small>View all</small>
                              </a> -->
                           </div>
                        </div>
                        <!-- END panel-->
                     </div>
                  </div>
                  <!-- END table-->
               </div>
               <!-- END dashboard main content-->
            </div>
         </section>
         <!-- END Page content-->
      </section>
      <!-- END Main section-->

   <?php $this->load->view('common/admin_footer');?>