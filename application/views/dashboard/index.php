<?php $this->load->view('common/admin_header');?>

      <!-- START Main section-->
      <section>
         <!-- START Page content-->
         <section class="main-content">
            <h3>Bienvenidos al sistema Estadístico LA CASA
               <br>
               <small>Bienvenidos <?php echo $curUser['nombre'];?></small>
            </h3>
         </section>
         <!-- END Page content-->
      </section>
      <!-- END Main section-->

   <?php $this->load->view('common/admin_footer');?>