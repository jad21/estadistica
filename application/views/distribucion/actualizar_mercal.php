<?php $this->load->view('common/admin_header');?>
<!-- START Main section-->
	<section ng-app="#app.distribucion">
	<!-- START Page content-->
		<section class="main-content" ng-controller="DistribucionCtrl">
			<span ng-init='distribucion=<?=$distribucion?>'></span>
			<span ng-init='form=distribucion.form'></span>
			
			<span ng-init='estadosRubros=distribucion.estadosRubros'></span>
			<span ng-init='iniciar(distribucion.cant_estados)'></span>
			<div  class="form-horizontal" role="form" >
				<div class="panel panel-default ">
					<div class="panel-heading">Módulo de Distribución: actualizar </div>
					<div class="panel-body">
						<div  class="col-md-12">
							<div ng-repeat="m in mensajes" class="alert alert-{{m.type||'success'}} alert-dismissible" role="alert">
								<button type="button" class="close" data-dismiss="alert" ng-click="mensajes.splice($index,1);"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
								<span ng-bind="m.text"></span>
							</div>
							<div class="form-group">
								<label class="col-md-1 control-label" for="textinput">Año:</label>
								<div class="col-md-2">
									<select ng-model="form.ano" ng-options='s.des as s.des for s in anos'  ng-init='anos = <?=$anos?>' class="form-control" required> </select>
								</div>
								<label class="col-md-1 control-label" for="textinput">Mes:</label>
								<div class="col-md-2">
									<select ng-model="form.mes" ng-options='s.id_mes as s.corto for s in meses' ng-init='meses = <?=$meses?>' class="form-control" required> </select>
								</div>
								<div class="col-md-3">
									<div class="row" >
										<div class="col-md-1" >
											<input style="padding-top: 0px !important;" ng-model="form.radio" type="radio" name="radio" class="" value="consolidado" >
										</div>
										<label style="padding-top: 0px !important;" class="col-md-5 control-label" for="textinput">Consolidación:</label>
									</div>
									<div class="row">
										<div class="col-md-1">
											<input style="padding-top: 0px !important;" ng-model="form.radio" type="radio" name="radio" class="" value="preliminar" ng-init="form.radio = 'preliminar' ">
										</div>
										<label style="padding-top: 0px !important;" class="col-md-5 control-label" for="textinput">Preliminar:</label>
									</div>
								</div>
								<div class="col-md-2">
									<button type="file" ng-file-select="onFileSelect($files)" class="btn btn-labeled btn-primary pull-right">
										<span class="btn-label"><i class="fa fa-plus-circle"></i>
										</span>Archivo
									</button>
								</div>
							</div>
							<div class="row">
							
								<div class="col-md-8">
									<div class="panel panel-info">
										<div class="panel-heading">
											<div class="form-group">
												<div class="col-md-2">
													<label class="control-label" for="textinput">Rubro </label>
												</div>
												<div class="col-md-2">
													<label class="control-label" for="textinput">TM </label>
												</div>
												<div class="col-md-2">
													<label class="control-label" for="textinput">Rubro </label>
												</div>
												<div class="col-md-2">
													<label class="control-label" for="textinput">TM </label>
												</div>
												<div class="col-md-2">
													<label class="control-label" for="textinput">Rubro </label>
												</div>
												<div class="col-md-2">
													<label class="control-label" for="textinput">TM </label>
												</div>
											</div>
										</div>
										<div class="panel-body">
											<div class="form-group">
												<div class="col-md-2">
													<label class="control-label" for="textinput">{{rubros[0].nombre}}: </label>
												</div>
												<div class="col-md-2">
													<input  ng-model="estadosRubros[form.id_estado][1]" type="text" name="total" placeholder="0.00" class="form-control toneladas" required>
												</div>
												<div class="col-md-2">
													<label class="control-label" for="textinput">{{rubros[1].nombre}}: </label>
												</div>
												<div class="col-md-2">
													<input  ng-model="estadosRubros[form.id_estado][2]" type="text" name="total" placeholder="0.00" class="form-control toneladas" required>
												</div>
												<div class="col-md-2">
													<label class="control-label" for="textinput">{{rubros[2].nombre}}: </label>
												</div>
												<div class="col-md-2">
													<input  ng-model="estadosRubros[form.id_estado][3]" type="text" name="total" placeholder="0.00" class="form-control toneladas" required>
												</div>
											</div>
											<div class="form-group">
												<div class="col-md-2">
													<label class="control-label" for="textinput">{{rubros[3].nombre}}: </label>
												</div>
												<div class="col-md-2">
													<input  ng-model="estadosRubros[form.id_estado][4]" type="text" name="total" placeholder="0.00" class="form-control toneladas" required>
												</div>
												<div class="col-md-2">
													<label class="control-label" for="textinput">{{rubros[4].nombre}}: </label>
												</div>
												<div class="col-md-2">
													<input  ng-model="estadosRubros[form.id_estado][5]" type="text" name="total" placeholder="0.00" class="form-control toneladas" required>
												</div>
												<div class="col-md-2">
													<label class="control-label" for="textinput">{{rubros[5].nombre}}: </label>
												</div>
												<div class="col-md-2">
													<input  ng-model="estadosRubros[form.id_estado][6]" type="text" name="total" placeholder="0.00" class="form-control toneladas" required>
												</div>
											</div>
											<div class="form-group">
												<div class="col-md-2">
													<label class="control-label" for="textinput">{{rubros[6].nombre}}: </label>
												</div>
												<div class="col-md-2">
													<input  ng-model="estadosRubros[form.id_estado][7]" type="text" name="total" placeholder="0.00" class="form-control toneladas" required>
												</div>
												<div class="col-md-2">
													<label class="control-label" for="textinput">{{rubros[7].nombre}}: </label>
												</div>
												<div class="col-md-2">
													<input  ng-model="estadosRubros[form.id_estado][8]" type="text" name="total" placeholder="0.00" class="form-control toneladas" required>
												</div>
												<div class="col-md-2">
													<label class="control-label" for="textinput">{{rubros[8].nombre}}: </label>
												</div>
												<div class="col-md-2">
													<input  ng-model="estadosRubros[form.id_estado][9]" type="text" name="total" placeholder="0.00" class="form-control toneladas" required>
												</div>
											</div>
											<div class="form-group">
												<div class="col-md-2">
													<label class="control-label" for="textinput">{{rubros[9].nombre}}: </label>
												</div>
												<div class="col-md-2">
													<input  ng-model="estadosRubros[form.id_estado][10]" type="text" name="total" placeholder="0.00" class="form-control toneladas" required>
												</div>
												<div class="col-md-2">
													<label class="control-label" for="textinput">{{rubros[10].nombre}}: </label>
												</div>
												<div class="col-md-2">
													<input  ng-model="estadosRubros[form.id_estado][11]" type="text" name="total" placeholder="0.00" class="form-control toneladas" required>
												</div>
												<div class="col-md-2">
													<label class="control-label" for="textinput">{{rubros[11].nombre}}: </label>
												</div>
												<div class="col-md-2">
													<input  ng-model="estadosRubros[form.id_estado][12]" type="text" name="total" placeholder="0.00" class="form-control toneladas" required>
												</div>
											</div>
											<div class="form-group">
												<div class="col-md-2">
													<label class="control-label" for="textinput">{{rubros[12].nombre}}: </label>
												</div>
												<div class="col-md-2">
													<input  ng-model="estadosRubros[form.id_estado][13]" type="text" name="total" placeholder="0.00" class="form-control toneladas" required>
												</div>
												<div class="col-md-2">
													<label class="control-label" for="textinput">{{rubros[13].nombre}}: </label>
												</div>
												<div class="col-md-2">
													<input  ng-model="estadosRubros[form.id_estado][14]" type="text" name="total" placeholder="0.00" class="form-control toneladas" required>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="col-md-3">
										<label class="control-label" for="textinput">Estados: </label>
									</div>
									<div class="col-md-7">
										<select ng-model="form.id_estado" ng-options='s.id_estado as s.des_estado for s in estados'  ng-init='estados = <?=json_encode($estados)?>;form.id_estado = estados[0].id_estado' class="form-control" required> </select>
									</div>
								</div>
							</div>

							<!-- <legend></legend> -->
							<div class="panel panel-default">
								<div class="panel-heading">
									Tabla de rubros por estados
									<a href="#" data-perform="panel-collapse" data-toggle="tooltip" title="Collapse Panel" class="pull-right">
										<em class="fa fa-minus"></em>
									</a>	
									<br>
								</div>
								<div class="panel-body">
									<table id="tabla_rubros" class="table table-striped table-bordered table-hover">
										<thead>
											<tr>
												<th>Estados</th>
												<?php foreach ($rubros as $rubro) {?>
												<th><?=$rubro["des_rubro"]?></th>
												<?php }?>	
											</tr>
										</thead>
										<tbody>
											<?php foreach ($estados as $est) {?>
											<tr>
												<td><?=$est["des_estado"]?></td>
												<?php foreach ($rubros as $rubro) {?>
												<td>
													<span ng-bind="estadosRubros['<?=$est["id_estado"]?>'][<?=$rubro['id_rubro']?>]"></span> 
													<!-- <label  ng-modl="rubros[12].cantidad" type="text" name="total" placeholder="0.0" class="form-control toneladas" style="padding:0px !important;padding-right: 3px !important;" required> -->
												</td>
												<?php }?>
											</tr>
											<?php }?>
										</tbody>
									</table>
								</div>
							</div>
							<!-- <div class="form-group">
								<div class="col-md-3">
									<label class="control-label" for="textinput">{{rubros[12].nombre}}: </label>
								</div>
								<div class="col-md-3">
									<input  ng-model="rubros[12].cantidad" type="text" name="total" placeholder="0.00" class="form-control toneladas" required>
								</div>
								<div class="col-md-3">
									<label class="control-label" for="textinput">{{rubros[13].nombre}}: </label>
								</div>
								<div class="col-md-3">
									<input  ng-model="rubros[13].cantidad" type="text" name="total" placeholder="0.00" class="form-control toneladas" required>
								</div>
							</div> -->
						</div>
					</div>
					<div class="panel-footer clearfix" >
						<div class="pull-right">
							<span >
								<button type="reset" ng-click="limpiar()" class="btn btn-default">Limpiar</button>
								<button ng-click="actualizar(form)" class="btn btn-primary">Actualizar</button>
							</span>
						</div>
					</div>
				</div>
			</div>
		</section>
         <!-- END Page content-->
    </section>
    <!-- END Main section-->

<?php $this->load->view('common/admin_footer');?>
