<!-- 
24;"Aceite Crudo De Palma"
25;"Aceite crudo de soya"
26;"Arroz paddy"
27;"Azucar crudo"
28;"Maiz amarillo"
29;"Maiz blanco"
30;"Sorgo"
31;"Torta de soya"
32;"Trigo"
 --> 
<?php $this->load->view('common/admin_header');?>
<!-- START Main section-->
	<section ng-app="#app.distribucion">
	<!-- START Page content-->
		<section class="main-content" ng-controller="DistribucionOtrasRedesCtrl">
			<span ng-init='form=<?=$distribucion?>'></span>
			
			<div  class="form-horizontal" role="form" >
				<div class="panel panel-default ">
					<div class="panel-heading">Módulo de Distribución: materias primas publicas/privadas </div>
					<div class="panel-body">
						<div  class="col-md-12">
							<div ng-repeat="m in mensajes" class="alert alert-{{m.type||'success'}} alert-dismissible" role="alert">
								<button type="button" class="close" data-dismiss="alert" ng-click="mensajes.splice($index,1);"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
								<span ng-bind="m.text"></span>
							</div>
							<div class="form-group">
								<label class="col-md-1 control-label" for="textinput">Año:</label>
								<div class="col-md-2">
									<select ng-model="form.ano" ng-options='s.des as s.des for s in anos'  ng-init='anos = <?=$anos?>' class="form-control" required> </select>
								</div>
								<label class="col-md-1 control-label" for="textinput">Mes:</label>
								<div class="col-md-2">
									<select ng-model="form.mes" ng-options='s.id_mes as s.corto for s in meses' ng-init='meses = <?=$meses?>' class="form-control" required> </select>
								</div>
								<div class="col-md-5">
									<div class="col-md-3">
										<label class="control-label" for="textinput">Tipo de red: </label>
									</div>
									<div class="col-md-7">
										<span ng-init='tipo_redes = [{id_tipo:4,des_tipo:"Publica"},{id_tipo:5,des_tipo:"Privada"}]'></span>
										<select ng-model="form.id_tipo_red" ng-options='s.id_tipo as s.des_tipo for s in tipo_redes' class="form-control" required></select>
									</div>
								</div>
							</div>
							<div class="">
								<div class="col-md-12">
									
								</div>
								<div class="col-md-12">
									<div class="panel panel-info">
										<div class="panel-heading">
											<div class="form-group">
												<div class="col-md-2">
													<label class="control-label" for="textinput">Rubro </label>
												</div>
												<div class="col-md-2">
													<label class="control-label" for="textinput">TM </label>
												</div>
												<div class="col-md-2">
													<label class="control-label" for="textinput">Rubro </label>
												</div>
												<div class="col-md-2">
													<label class="control-label" for="textinput">TM </label>
												</div>
												<div class="col-md-2">
													<label class="control-label" for="textinput">Rubro </label>
												</div>
												<div class="col-md-2">
													<label class="control-label" for="textinput">TM </label>
												</div>
											</div>
										</div>
										<div class="panel-body">
											<div class="form-group">
												<div class="col-md-2">
													<label class="control-label" for="textinput">Aceite Crudo De Palma: </label>
												</div>
												<div class="col-md-2">
													<input  ng-model="form.rubros[24]" type="text" name="total" placeholder="0.00" class="form-control toneladas" required>
												</div>
												<div class="col-md-2">
													<label class="control-label" for="textinput">Aceite crudo de soya: </label>
												</div>
												<div class="col-md-2">
													<input  ng-model="form.rubros[25]" type="text" name="total" placeholder="0.00" class="form-control toneladas" required>
												</div>
												<div class="col-md-2">
													<label class="control-label" for="textinput">Arroz paddy: </label>
												</div>
												<div class="col-md-2">
													<input  ng-model="form.rubros[26]" type="text" name="total" placeholder="0.00" class="form-control toneladas" required>
												</div>
											</div>
											<div class="form-group">
												<div class="col-md-2">
													<label class="control-label" for="textinput">Azucar crudo: </label>
												</div>
												<div class="col-md-2">
													<input  ng-model="form.rubros[27]" type="text" name="total" placeholder="0.00" class="form-control toneladas" required>
												</div>
												<div class="col-md-2">
													<label class="control-label" for="textinput">Maiz amarillo: </label>
												</div>
												<div class="col-md-2">
													<input  ng-model="form.rubros[28]" type="text" name="total" placeholder="0.00" class="form-control toneladas" required>
												</div>
												<div class="col-md-2">
													<label class="control-label" for="textinput">Maiz blanco: </label>
												</div>
												<div class="col-md-2">
													<input  ng-model="form.rubros[29]" type="text" name="total" placeholder="0.00" class="form-control toneladas" required>
												</div>
											</div>
											<div class="form-group">
												<div class="col-md-2">
													<label class="control-label" for="textinput">Sorgo: </label>
												</div>
												<div class="col-md-2">
													<input  ng-model="form.rubros[30]" type="text" name="total" placeholder="0.00" class="form-control toneladas" required>
												</div>
												<div class="col-md-2">
													<label class="control-label" for="textinput">Torta de soya: </label>
												</div>
												<div class="col-md-2">
													<input  ng-model="form.rubros[31]" type="text" name="total" placeholder="0.00" class="form-control toneladas" required>
												</div>
												<div class="col-md-2">
													<label class="control-label" for="textinput">Trigo: </label>
												</div>
												<div class="col-md-2">
													<input  ng-model="form.rubros[32]" type="text" name="total" placeholder="0.00" class="form-control toneladas" required>
												</div>
											</div>
										</div>
										<!-- <div class="panel-footer clearfix">
											<div class="pull-right">
												<button type="button" class="btn btn-labeled btn-info " ng-click="agregar(form)">
													<span class="btn-label"><i class="fa fa-plus-circle"></i> </span>Agregar Rubro
												</button>
											</div>
										</div> -->
									</div>
								</div>
								
							</div>
						</div>
					</div>
					<div class="panel-footer clearfix" >
						<div class="pull-right">
							<span >
								<button type="reset" ng-click="atras()" class="btn btn-default">Cancelar</button>
								<button ng-click="actualizar(form)" class="btn btn-primary">Guardar Cambios</button>
							</span>
						</div>
					</div>
				</div>
			</div>
		</section>
         <!-- END Page content-->
    </section>
    <!-- END Main section-->



<?php $this->load->view('common/admin_footer');?>
