<?php $this->load->view('common/admin_header');?>
<!-- START Main section-->

<section ng-app="#app.distribucion">
	<!-- START Page content-->
	<section class="main-content" ng-controller="DistribucionOtrasRedesCtrl">
	
			<h3>Lista de distribuciones de materia prima</h3>

			<div  class="form-horizontal" role="form" >
				<div class="panel panel-default ">
					<div class="panel-heading">Procesar las distribuciones</div>
					<div class="panel-body">
						<div ng-repeat="m in mensajes" class="alert alert-{{m.type||'success'}} alert-dismissible" role="alert">
							<button type="button" class="close" data-dismiss="alert" ng-click="mensajes.splice($index,1);"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
							<span ng-bind="m.text"></span>
						</div>
						<table class="dataTable table table-striped table-bordered table-hover">
							<!-- {"id_dist":"23","id_tipo_red":"5","id_usuario":"1","ano":"2016","id_mes":"1","des_mes":"Enero","fecha_registro":"2015-11-16","cantidad":"110" -->
							<thead>
								<tr>
									<td>Año</td>
									<td>Mes</td>
									<td>Tipo</td>
									<td>Fecha de registro</td>
									<td>Total de toneladas</td>
									<td>Usuario</td>
									<td>Botones</td>
								</tr>
							</thead>
							<tbody>
								<?php foreach ($lista as $l): ?>
									<tr>
										<td><?=$l["ano"]?></td>
										<td><?=$l["des_mes"]?></td>

									<?php if($l["id_tipo_red"]==4){?>
										<td>Publico</td>
									<?php }else{ ?>
										<td>Privado</td>
									<?php } ?>

										<td><?=$l["fecha_registro"]?></td>
										<td><?=$l["cantidad"]?></td>
										<td><?=$l["nombre"]?></td>
										<td>
											<p>
												<a href="javascript:;" ng-click="eliminar_prima(<?=$l['id_dist']?>)">eliminar</a>
											</p>
											<p>
												<a href="<?=site_url('distribucion/actualizar_otras_redes_primas/'.$l['id_dist'])?>">modificar</a>
											</p> 

										</td>
									</tr>
								<?php endforeach ?>
							</tbody>
						</table>
					</div>
					<div class="panel-footer clearfix" >
						<!-- <div class="pull-right">
							<span >
								<button type="reset" ng-click="form={}" class="btn btn-default">Limpiar</button>
								<button ng-click="guardar(form)" class="btn btn-primary">Guardar</button>
							</span>
						</div> -->
					</div>
				</div>
			</div>
		</section>
		<!-- END Page content-->
	</section>
	<!-- END Main section-->



<?php $this->load->view('common/admin_footer');?>
