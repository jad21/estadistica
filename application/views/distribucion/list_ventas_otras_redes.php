<?php $this->load->view('common/admin_header');?>
<!-- START Main section-->
<section ng-app="#app.distribucion">
	<!-- START Page content-->
	<section class="main-content" ng-controller="DistribucionOtrasRedesVentasCtrl">
			<!-- <h3>Lista de ventas de otras redes publicas y privadas</h3> -->
			<div  class="form-horizontal" role="form" >
				<div class="panel panel-default ">
					<div class="panel-heading">Lista de ventas de otras redes publicas y privadas</div>
					<div class="panel-body">
						<div ng-repeat="m in mensajes" class="alert alert-{{m.type||'success'}} alert-dismissible" role="alert">
							<button type="button" class="close" data-dismiss="alert" ng-click="mensajes.splice($index,1);"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
							<span ng-bind="m.text"></span>
						</div>
						<table class="dataTable table table-striped table-bordered table-hover">
							<thead>
								<tr>
									<td>Año</td>
									<td>Mes</td>
									<td>Fecha de registro</td>
									<td>Total de toneladas</td>
									<td>Usuario</td>
									<td>Botones</td>
								</tr>
							</thead>
							<tbody>
								<?php foreach ($lista as $l): ?>
									<tr>
										<td><?=$l["ano"]?></td>
										<td><?=$l["des_mes"]?></td>
										<td><?=$l["fecha_registro"]?></td>
										<td><?=$l["cantidad"]?></td>
										<td><?=$l["usuario"]?></td>
										<td>
											<p>
												<a href="#" ng-click="eliminar(<?=$l['id_venta']?>)">eliminar</a>
											</p>
											<p>
												<a href="<?=site_url('distribucion/actualizar_ventas_otras_redes/'.$l['id_venta'])?>">modificar</a>
											</p> 
										</td>
									</tr>
								<?php endforeach ?>
							</tbody>
						</table>
					</div>
					<div class="panel-footer clearfix" >
					</div>
				</div>
			</div>
		</section>
		<!-- END Page content-->
	</section>
	<!-- END Main section-->


<?php $this->load->view('common/admin_footer');?>
