<!--
dist_redes
	id_dist
	id_tipo_red
	cantidad 
1;"Mercal"
6;"Becentenario (Producto Terminado)"
7;"PDVAL (Producto Terminado)"
8;"Lacteos los Andes (Producto Terminado)"
9;"FRIOSA (Producto Terminado)"
10;"VENALCASA (Producto Terminado)"
11;"Otras (Producto Terminado)"
4;"Materia Prima (Publico)"
5;"Materia Prima (Privado)"

 -->
<?php $this->load->view('common/admin_header');?>
<!-- START Main section-->
	<section ng-app="#app.distribucion">
	<!-- START Page content-->
		<section class="main-content" ng-controller="DistribucionOtrasRedesVentasCtrl">
		<!-- <span ng-init='prefixDomain="<?=base_url()?>"'></span> -->
			<!-- <button type="button" class="btn btn-labeled btn-primary pull-right">
			<span class="btn-label"><i class="fa fa-plus-circle"></i>
			</span>Add Item</button> -->
			<!-- <h3> -->
				<!-- <br> -->
				<!-- <small>Registrar las planificaciones</small> -->
			<!-- </h3> -->
			<div  class="form-horizontal" role="form" >
				<div class="panel panel-default ">
					<div class="panel-heading">Ventas a otras redes publicas y privadas </div>
					<div class="panel-body">
						<div  class="col-md-12">
							<div ng-repeat="m in mensajes" class="alert alert-{{m.type||'success'}} alert-dismissible" role="alert">
								<button type="button" class="close" data-dismiss="alert" ng-click="mensajes.splice($index,1);"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
								<span ng-bind="m.text"></span>
							</div>
							<div class="form-group">
								<label class="col-md-1 control-label" for="textinput">Año:</label>
								<div class="col-md-2">
									<select ng-model="form.ano" ng-options='s.des as s.des for s in anos'  ng-init='anos = <?=$anos?>' class="form-control" required> </select>
								</div>
								<label class="col-md-1 control-label" for="textinput">Mes:</label>
								<div class="col-md-2">
									<select ng-model="form.mes" ng-options='s.id_mes as s.des_mes for s in meses' ng-init='meses = <?=$meses?>' class="form-control" required> </select>
								</div>
							</div>
							<div class="">
									<div class="col-md-12">
									<div class="panel panel-info">
										<div class="panel-heading">
											<div class="form-group">
												<div class="col-md-3">
													<label class="control-label" for="textinput">Red </label>
												</div>
												<div class="col-md-3">
													<label class="control-label" for="textinput">TM </label>
												</div>
												<div class="col-md-3">
													<label class="control-label" for="textinput">Red </label>
												</div>
												<div class="col-md-3">
													<label class="control-label" for="textinput">TM </label>
												</div>
												
											</div>
										</div>
										<div class="panel-body">
											<div class="form-group">
												<div class="col-md-3">
													<label class="control-label" for="textinput">Mercal: </label>
												</div>
												<div class="col-md-3">
													<input  ng-model="form.redes[1]" type="text" name="total" placeholder="0.00" class="form-control toneladas" required>
												</div>
												<div class="col-md-3">
													<label class="control-label" for="textinput">Becentenario (Producto Terminado): </label>
												</div>
												<div class="col-md-3">
													<input  ng-model="form.redes[6]" type="text" name="total" placeholder="0.00" class="form-control toneladas" required>
												</div>
											</div>
											<div class="form-group">
												<div class="col-md-3">
													<label class="control-label" for="textinput">PDVAL (Producto Terminado): </label>
												</div>
												<div class="col-md-3">
													<input  ng-model="form.redes[7]" type="text" name="total" placeholder="0.00" class="form-control toneladas" required>
												</div>
												<div class="col-md-3">
													<label class="control-label" for="textinput">Lacteos los Andes (Producto Terminado): </label>
												</div>
												<div class="col-md-3">
													<input  ng-model="form.redes[8]" type="text" name="total" placeholder="0.00" class="form-control toneladas" required>
												</div>
											</div>
											<div class="form-group">
												<div class="col-md-3">
													<label class="control-label" for="textinput">FRIOSA (Producto Terminado): </label>
												</div>
												<div class="col-md-3">
													<input  ng-model="form.redes[9]" type="text" name="total" placeholder="0.00" class="form-control toneladas" required>
												</div>
												<div class="col-md-3">
													<label class="control-label" for="textinput">VENALCASA (Producto Terminado): </label>
												</div>
												<div class="col-md-3">
													<input  ng-model="form.redes[10]" type="text" name="total" placeholder="0.00" class="form-control toneladas" required>
												</div>
											</div>
											<div class="form-group">
												<div class="col-md-3">
													<label class="control-label" for="textinput">Otras (Producto Terminado): </label>
												</div>
												<div class="col-md-3">
													<input  ng-model="form.redes[11]" type="text" name="total" placeholder="0.00" class="form-control toneladas" required>
												</div>
												<div class="col-md-3">
													<label class="control-label" for="textinput">Materia Prima (Publico): </label>
												</div>
												<div class="col-md-3">
													<input  ng-model="form.redes[4]" type="text" name="total" placeholder="0.00" class="form-control toneladas" required>
												</div>
											</div>
											<div class="form-group">
												<div class="col-md-3">
													<label class="control-label" for="textinput">Materia Prima (Privado): </label>
												</div>
												<div class="col-md-3">
													<input  ng-model="form.redes[5]" type="text" name="total" placeholder="0.00" class="form-control toneladas" required>
												</div>
												
											</div>
											
										</div>
										<!-- <div class="panel-footer clearfix">
											<div class="pull-right">
												<button type="button" class="btn btn-labeled btn-info " ng-click="agregar(form)">
													<span class="btn-label"><i class="fa fa-plus-circle"></i> </span>Agregar Rubro
												</button>
											</div>
										</div> -->
									</div>
								</div>
								
							</div>
						</div>
					</div>
					<div class="panel-footer clearfix" >
						<div class="pull-right">
							<span >
								<button type="reset" ng-click="limpiar()" class="btn btn-default">Limpiar</button>
								<button ng-click="guardar(form)" class="btn btn-primary">Guardar</button>
							</span>
						</div>
					</div>
				</div>
			</div>
		</section>
         <!-- END Page content-->
    </section>
    <!-- END Main section-->



<?php $this->load->view('common/admin_footer');?>
