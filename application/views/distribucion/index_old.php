<?php $this->load->view('common/admin_header');?>
<!-- START Main section-->
	<section ng-app="#app.distribucion">
	<!-- START Page content-->
		<section class="main-content" ng-controller="DistribucionCtrl">
		<!-- <span ng-init='prefixDomain="<?=base_url()?>"'></span> -->
			<!-- <button type="button" class="btn btn-labeled btn-primary pull-right">
			<span class="btn-label"><i class="fa fa-plus-circle"></i>
			</span>Add Item</button> -->
			<!-- <h3> -->
				<!-- <br> -->
				<!-- <small>Registrar las planificaciones</small> -->
			<!-- </h3> -->
			<div  class="form-horizontal" role="form" >
				<div class="panel panel-default ">
					<div class="panel-heading">Módulo de Distribución</div>
					<div class="panel-body">
						<div  class="col-md-12">
							<div ng-repeat="m in mensajes" class="alert alert-{{m.type||'success'}} alert-dismissible" role="alert">
								<button type="button" class="close" data-dismiss="alert" ng-click="mensajes.splice($index,1);"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
								<span ng-bind="m.text"></span>
							</div>
							<div class="form-group">
								<label class="col-md-1 control-label" for="textinput">Año:</label>
								<div class="col-md-2">
									<select ng-model="form.ano" ng-options='s.id as s.des for s in anos'  ng-init='anos = <?=$anos?>' class="form-control" required> </select>
								</div>
								<label class="col-md-1 control-label" for="textinput">Mes:</label>
								<div class="col-md-2">
									<select ng-model="form.mes" ng-options='s.id_mes as s.corto for s in meses' ng-init='meses = <?=$meses?>' class="form-control" required> </select>
								</div>
								<div class="col-md-3">
									<div class="row" >
										<div class="col-md-1" >
											<input style="padding-top: 0px !important;" ng-model="form.consolidacion" type="radio" name="radio" class="" >
										</div>
										<label style="padding-top: 0px !important;" class="col-md-5 control-label" for="textinput">Consolidación:</label>
									</div>
									<div class="row">
										<div class="col-md-1">
											<input style="padding-top: 0px !important;" ng-model="form.Preliminar" type="radio" name="radio" class="" >
										</div>
										<label style="padding-top: 0px !important;" class="col-md-5 control-label" for="textinput">Preliminar:</label>
									</div>
								</div>
								<div class="col-md-2">
									<button type="file" ng-file-select="onFileSelect($files)" class="btn btn-labeled btn-primary pull-right">
										<span class="btn-label"><i class="fa fa-plus-circle"></i>
										</span>Archivo
									</button>
								</div>
							</div>
							
							<div class="form-group">
								<div class="col-md-3">
									<label class="control-label" for="textinput">Rubro </label>
								</div>
								<div class="col-md-3">
									<label class="control-label" for="textinput">TM </label>
								</div>
								<div class="col-md-3">
									<label class="control-label" for="textinput">Rubro </label>
								</div>
								<div class="col-md-3">
									<label class="control-label" for="textinput">TM </label>
								</div>
							</div>

							<div class="form-group">
								<div class="col-md-3">
									<label class="control-label" for="textinput">{{rubros[0].nombre}}: </label>
								</div>
								<div class="col-md-3">
									<input  ng-model="rubros[0].cantidad" type="text" name="total" placeholder="0.00" class="form-control toneladas" required>
								</div>
								<div class="col-md-3">
									<label class="control-label" for="textinput">{{rubros[1].nombre}}: </label>
								</div>
								<div class="col-md-3">
									<input  ng-model="rubros[1].cantidad" type="text" name="total" placeholder="0.00" class="form-control toneladas" required>
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-3">
									<label class="control-label" for="textinput">{{rubros[2].nombre}}: </label>
								</div>
								<div class="col-md-3">
									<input  ng-model="rubros[2].cantidad" type="text" name="total" placeholder="0.00" class="form-control toneladas" required>
								</div>
								<div class="col-md-3">
									<label class="control-label" for="textinput">{{rubros[3].nombre}}: </label>
								</div>
								<div class="col-md-3">
									<input  ng-model="rubros[3].cantidad" type="text" name="total" placeholder="0.00" class="form-control toneladas" required>
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-3">
									<label class="control-label" for="textinput">{{rubros[4].nombre}}: </label>
								</div>
								<div class="col-md-3">
									<input  ng-model="rubros[4].cantidad" type="text" name="total" placeholder="0.00" class="form-control toneladas" required>
								</div>
								<div class="col-md-3">
									<label class="control-label" for="textinput">{{rubros[5].nombre}}: </label>
								</div>
								<div class="col-md-3">
									<input  ng-model="rubros[5].cantidad" type="text" name="total" placeholder="0.00" class="form-control toneladas" required>
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-3">
									<label class="control-label" for="textinput">{{rubros[6].nombre}}: </label>
								</div>
								<div class="col-md-3">
									<input  ng-model="rubros[6].cantidad" type="text" name="total" placeholder="0.00" class="form-control toneladas" required>
								</div>
								<div class="col-md-3">
									<label class="control-label" for="textinput">{{rubros[7].nombre}}: </label>
								</div>
								<div class="col-md-3">
									<input  ng-model="rubros[7].cantidad" type="text" name="total" placeholder="0.00" class="form-control toneladas" required>
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-3">
									<label class="control-label" for="textinput">{{rubros[8].nombre}}: </label>
								</div>
								<div class="col-md-3">
									<input  ng-model="rubros[8].cantidad" type="text" name="total" placeholder="0.00" class="form-control toneladas" required>
								</div>
								<div class="col-md-3">
									<label class="control-label" for="textinput">{{rubros[9].nombre}}: </label>
								</div>
								<div class="col-md-3">
									<input  ng-model="rubros[9].cantidad" type="text" name="total" placeholder="0.00" class="form-control toneladas" required>
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-3">
									<label class="control-label" for="textinput">{{rubros[10].nombre}}: </label>
								</div>
								<div class="col-md-3">
									<input  ng-model="rubros[10].cantidad" type="text" name="total" placeholder="0.00" class="form-control toneladas" required>
								</div>
								<div class="col-md-3">
									<label class="control-label" for="textinput">{{rubros[11].nombre}}: </label>
								</div>
								<div class="col-md-3">
									<input  ng-model="rubros[11].cantidad" type="text" name="total" placeholder="0.00" class="form-control toneladas" required>
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-3">
									<label class="control-label" for="textinput">{{rubros[12].nombre}}: </label>
								</div>
								<div class="col-md-3">
									<input  ng-model="rubros[12].cantidad" type="text" name="total" placeholder="0.00" class="form-control toneladas" required>
								</div>
								<div class="col-md-3">
									<label class="control-label" for="textinput">{{rubros[13].nombre}}: </label>
								</div>
								<div class="col-md-3">
									<input  ng-model="rubros[13].cantidad" type="text" name="total" placeholder="0.00" class="form-control toneladas" required>
								</div>
							</div>
						</div>
					</div>
					<div class="panel-footer clearfix" >
						<div class="pull-right">
							<span >
								<button type="reset" ng-click="form={}" class="btn btn-default">Limpiar</button>
								<button ng-click="guardar(form)" class="btn btn-primary">Guardar</button>
							</span>
						</div>
					</div>
				</div>
			</div>
		</section>
         <!-- END Page content-->
    </section>
    <!-- END Main section-->



<?php $this->load->view('common/admin_footer');?>
