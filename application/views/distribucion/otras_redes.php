<?php $this->load->view('common/admin_header');?>
<!-- START Main section-->
	<section ng-app="#app.distribucion">
	<!-- START Page content-->
		<section class="main-content" ng-controller="DistribucionOtrasRedesCtrl">
		<!-- <span ng-init='prefixDomain="<?=base_url()?>"'></span> -->
			<!-- <button type="button" class="btn btn-labeled btn-primary pull-right">
			<span class="btn-label"><i class="fa fa-plus-circle"></i>
			</span>Add Item</button> -->
			<!-- <h3> -->
				<!-- <br> -->
				<!-- <small>Registrar las planificaciones</small> -->
			<!-- </h3> -->
			<div  class="form-horizontal" role="form" >
				<div class="panel panel-default ">
					<div class="panel-heading">Módulo de Distribución </div>
					<div class="panel-body">
						<div  class="col-md-12">
							<div ng-repeat="m in mensajes" class="alert alert-{{m.type||'success'}} alert-dismissible" role="alert">
								<button type="button" class="close" data-dismiss="alert" ng-click="mensajes.splice($index,1);"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
								<span ng-bind="m.text"></span>
							</div>
							<div class="form-group">
								<label class="col-md-1 control-label" for="textinput">Año:</label>
								<div class="col-md-2">
									<select ng-model="form.ano" ng-options='s.des as s.des for s in anos'  ng-init='anos = <?=$anos?>' class="form-control" required> </select>
								</div>
								<label class="col-md-1 control-label" for="textinput">Mes:</label>
								<div class="col-md-2">
									<select ng-model="form.mes" ng-options='s.id_mes as s.corto for s in meses' ng-init='meses = <?=$meses?>' class="form-control" required> </select>
								</div>
								<div class="col-md-5">
									<div class="col-md-3">
										<label class="control-label" for="textinput">Tipo de red: </label>
									</div>
									<div class="col-md-7">
										<span ng-init='tipo_redes = [{id_tipo:1,des_tipo:"Publica"},{id_tipo:1,des_tipo:"Privada"}]'></span>
										<select ng-model="form.id_tipo_red" ng-options='s.id_tipo as s.des_tipo for s in tipo_redes' class="form-control" required></select>
									</div>
								</div>
							</div>
							<div class="container">
								<div class="col-md-12">
									
								</div>
								<div class="col-md-12">
									<div class="panel panel-info">
										<div class="panel-heading">
											<div class="form-group">
												<div class="col-md-2">
													<label class="control-label" for="textinput">Estado </label>
												</div>
												<div class="col-md-2">
													<label class="control-label" for="textinput">TM </label>
												</div>
												<div class="col-md-2">
													<label class="control-label" for="textinput">Estado </label>
												</div>
												<div class="col-md-2">
													<label class="control-label" for="textinput">TM </label>
												</div>
												<div class="col-md-2">
													<label class="control-label" for="textinput">Estado </label>
												</div>
												<div class="col-md-2">
													<label class="control-label" for="textinput">TM </label>
												</div>
											</div>
										</div>
										<div class="panel-body">
											<div class="form-group">
												<div class="col-md-2">
													<label class="control-label" for="textinput">Amazonas: </label>
												</div>
												<div class="col-md-2">
													<input  ng-model="form.cantidad[1]" type="text" name="total" placeholder="0.00" class="form-control toneladas" required>
												</div>
												<div class="col-md-2">
													<label class="control-label" for="textinput">Anzoátegui: </label>
												</div>
												<div class="col-md-2">
													<input  ng-model="form.cantidad[2]" type="text" name="total" placeholder="0.00" class="form-control toneladas" required>
												</div>
												<div class="col-md-2">
													<label class="control-label" for="textinput">Apure: </label>
												</div>
												<div class="col-md-2">
													<input  ng-model="form.cantidad[3]" type="text" name="total" placeholder="0.00" class="form-control toneladas" required>
												</div>
											</div>
											<div class="form-group">
												<div class="col-md-2">
													<label class="control-label" for="textinput">Aragua: </label>
												</div>
												<div class="col-md-2">
													<input  ng-model="form.cantidad[4]" type="text" name="total" placeholder="0.00" class="form-control toneladas" required>
												</div>
												<div class="col-md-2">
													<label class="control-label" for="textinput">Barinas: </label>
												</div>
												<div class="col-md-2">
													<input  ng-model="form.cantidad[5]" type="text" name="total" placeholder="0.00" class="form-control toneladas" required>
												</div>
												<div class="col-md-2">
													<label class="control-label" for="textinput">Bolívar: </label>
												</div>
												<div class="col-md-2">
													<input  ng-model="form.cantidad[6]" type="text" name="total" placeholder="0.00" class="form-control toneladas" required>
												</div>
											</div>
											<div class="form-group">
												<div class="col-md-2">
													<label class="control-label" for="textinput">Carabobo: </label>
												</div>
												<div class="col-md-2">
													<input  ng-model="form.cantidad[7]" type="text" name="total" placeholder="0.00" class="form-control toneladas" required>
												</div>
												<div class="col-md-2">
													<label class="control-label" for="textinput">Cojedes: </label>
												</div>
												<div class="col-md-2">
													<input  ng-model="form.cantidad[8]" type="text" name="total" placeholder="0.00" class="form-control toneladas" required>
												</div>
												<div class="col-md-2">
													<label class="control-label" for="textinput">Delta Amacuro: </label>
												</div>
												<div class="col-md-2">
													<input  ng-model="form.cantidad[9]" type="text" name="total" placeholder="0.00" class="form-control toneladas" required>
												</div>
											</div>
											<div class="form-group">
												<div class="col-md-2">
													<label class="control-label" for="textinput">Distrito Capital: </label>
												</div>
												<div class="col-md-2">
													<input  ng-model="form.cantidad[10]" type="text" name="total" placeholder="0.00" class="form-control toneladas" required>
												</div>
												<div class="col-md-2">
													<label class="control-label" for="textinput">Falcón: </label>
												</div>
												<div class="col-md-2">
													<input  ng-model="form.cantidad[11]" type="text" name="total" placeholder="0.00" class="form-control toneladas" required>
												</div>
												<div class="col-md-2">
													<label class="control-label" for="textinput">Guárico: </label>
												</div>
												<div class="col-md-2">
													<input  ng-model="form.cantidad[12]" type="text" name="total" placeholder="0.00" class="form-control toneladas" required>
												</div>
											</div>
											<div class="form-group">
												<div class="col-md-2">
													<label class="control-label" for="textinput">Lara: </label>
												</div>
												<div class="col-md-2">
													<input  ng-model="form.cantidad[13]" type="text" name="total" placeholder="0.00" class="form-control toneladas" required>
												</div>
												<div class="col-md-2">
													<label class="control-label" for="textinput">Mérida: </label>
												</div>
												<div class="col-md-2">
													<input  ng-model="form.cantidad[14]" type="text" name="total" placeholder="0.00" class="form-control toneladas" required>
												</div>
												<div class="col-md-2">
													<label class="control-label" for="textinput">Miranda: </label>
												</div>
												<div class="col-md-2">
													<input  ng-model="form.cantidad[15]" type="text" name="total" placeholder="0.00" class="form-control toneladas" required>
												</div>
											</div>
											<div class="form-group">
												<div class="col-md-2">
													<label class="control-label" for="textinput">Monagas: </label>
												</div>
												<div class="col-md-2">
													<input  ng-model="form.cantidad[16]" type="text" name="total" placeholder="0.00" class="form-control toneladas" required>
												</div>
												<div class="col-md-2">
													<label class="control-label" for="textinput">Nueva Esparta: </label>
												</div>
												<div class="col-md-2">
													<input  ng-model="form.cantidad[17]" type="text" name="total" placeholder="0.00" class="form-control toneladas" required>
												</div>
												<div class="col-md-2">
													<label class="control-label" for="textinput">Portuguesa: </label>
												</div>
												<div class="col-md-2">
													<input  ng-model="form.cantidad[18]" type="text" name="total" placeholder="0.00" class="form-control toneladas" required>
												</div>
											</div>
											<div class="form-group">
												<div class="col-md-2">
													<label class="control-label" for="textinput">Sucre: </label>
												</div>
												<div class="col-md-2">
													<input  ng-model="form.cantidad[19]" type="text" name="total" placeholder="0.00" class="form-control toneladas" required>
												</div>
												<div class="col-md-2">
													<label class="control-label" for="textinput">Táchira: </label>
												</div>
												<div class="col-md-2">
													<input  ng-model="form.cantidad[20]" type="text" name="total" placeholder="0.00" class="form-control toneladas" required>
												</div>
												<div class="col-md-2">
													<label class="control-label" for="textinput">Trujillo: </label>
												</div>
												<div class="col-md-2">
													<input  ng-model="form.cantidad[21]" type="text" name="total" placeholder="0.00" class="form-control toneladas" required>
												</div>
											</div>
											<div class="form-group">
												<div class="col-md-2">
													<label class="control-label" for="textinput">Vargas: </label>
												</div>
												<div class="col-md-2">
													<input  ng-model="form.cantidad[22]" type="text" name="total" placeholder="0.00" class="form-control toneladas" required>
												</div>
												<div class="col-md-2">
													<label class="control-label" for="textinput">Yaracuy: </label>
												</div>
												<div class="col-md-2">
													<input  ng-model="form.cantidad[23]" type="text" name="total" placeholder="0.00" class="form-control toneladas" required>
												</div>
												<div class="col-md-2">
													<label class="control-label" for="textinput">Zulia: </label>
												</div>
												<div class="col-md-2">
													<input  ng-model="form.cantidad[24]" type="text" name="total" placeholder="0.00" class="form-control toneladas" required>
												</div>
											</div>
										</div>
										<div class="panel-footer clearfix">
											<div class="pull-right">
												<div class="form-group">
													<div class="col-md-2">
														<label class="control-label" for="textinput">Rubros: </label>
													</div>
													<div class="col-md-5">
														<span ng-init='rubros_terminados = <?=json_encode($rubros_terminados)?>;'></span>
														<span ng-init='rubros_prima = <?=json_encode($rubros_prima)?>;'></span>
														<select ng-model="form.id_rubro" class="form-control" required> 
															<option disabled="disabled">Producto terminado</option>
															<optgroup >
																<option ng-repeat="r in rubros_terminados" value="r.id_rubro" ng-bind="r.des_rubro"></option>
															</optgroup>
															<option disabled="disabled">Materia prima</option>
															<optgroup>
																<option ng-repeat="r in rubros_prima" value="r.id_rubro" ng-bind="r.des_rubro"></option>
															</optgroup>
														</select>
													</div>
													<button type="button" class="btn btn-labeled btn-info " ng-click="agregar(form)">
														<span class="btn-label"><i class="fa fa-plus-circle"></i> </span>Agregar Rubro
													</button>
												</div>
											</div>
										</div>
									</div>
								</div>
								
							</div>

							<!-- <legend></legend> -->
							<div class="panel panel-default">
								<div class="panel-heading">
									Tabla de materia prima y productos terminados
									<a href="#" data-perform="panel-collapse" data-toggle="tooltip" title="Collapse Panel" class="pull-right">
										<em class="fa fa-minus"></em>
									</a>	
									<br>
								</div>
								<div class="panel-body">
									<!-- <consulta data="" campos="" /> -->
									<table id="tabla_rubros" class="table table-striped table-bordered table-hover">
										<thead>
											<tr>
												<th>Rubros</th>
												<th>Tipo Rubro</th>
												<?php foreach ($estados as $estado) {?>
												<th><?=$estado["des_estado"]?></th>
												<?php }?>	
											</tr>
										</thead>
										<tbody ng-init="registros = []">

											<tr ng-repeat="reg in registros">
												<td><b><span ng-bind="reg.des_rubro"></span></b></td>							
												<td><span ng-bind="reg.des_tipo"></span></td>							
												<td><span ng-bind="reg.des_tipo"></span></td>							
											</tr>
										</tbody>
									</table>
								</div>
							</div>
							<!-- <div class="form-group">
								<div class="col-md-3">
									<label class="control-label" for="textinput">{{rubros[12].nombre}}: </label>
								</div>
								<div class="col-md-3">
									<input  ng-model="rubros[12].cantidad" type="text" name="total" placeholder="0.00" class="form-control toneladas" required>
								</div>
								<div class="col-md-3">
									<label class="control-label" for="textinput">{{rubros[13].nombre}}: </label>
								</div>
								<div class="col-md-3">
									<input  ng-model="rubros[13].cantidad" type="text" name="total" placeholder="0.00" class="form-control toneladas" required>
								</div>
							</div> -->
						</div>
					</div>
					<div class="panel-footer clearfix" >
						<div class="pull-right">
							<span >
								<button type="reset" ng-click="limpiar()" class="btn btn-default">Limpiar</button>
								<button ng-click="guardar(form)" class="btn btn-primary">Guardar</button>
							</span>
						</div>
					</div>
				</div>
			</div>
		</section>
         <!-- END Page content-->
    </section>
    <!-- END Main section-->



<?php $this->load->view('common/admin_footer');?>
