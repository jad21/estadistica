<!-- 
1;"Aceite"
2;"Arroz"
3;"Arvejas"
4;"Azucar"
33;"Azucar Glass Pulverizada"
34;"Cafe"
5;"Caraotas"
6;"Carne"
35;"Harina de Maiz Precocida"
8;"Harina Trigo"
36;Jarabe de Fructosa y Glucosa
10;"Leche"
18;"Leche UHT"
9;"Lentejas"
11;"Margarina"
20;"Palmito"
13;"Pasta"
21;"Pernil"
14;"Pollo"
37;"Atun(Mercal)"
38;"Carne(carnes vzla)"
39;"Pernil(carnes vzla)"
40;"Leche UHT(MERCAL)"
 --> 
<?php $this->load->view('common/admin_header');?>
<!-- START Main section-->
	<section ng-app="#app.distribucion">
	<!-- START Page content-->
		<section class="main-content" ng-controller="DistribucionOtrasRedesCtrl">
			<span ng-init='form=<?=$distribucion?>'></span>
		<!-- <span ng-init='prefixDomain="<?=base_url()?>"'></span> -->
			<!-- <button type="button" class="btn btn-labeled btn-primary pull-right">
			<span class="btn-label"><i class="fa fa-plus-circle"></i>
			</span>Add Item</button> -->
			<!-- <h3> -->
				<!-- <br> -->
				<!-- <small>Registrar las planificaciones</small> -->
			<!-- </h3> -->
			<div  class="form-horizontal" role="form" >
				<div class="panel panel-default ">
					<div class="panel-heading">Módulo de Distribución: Productos Terminados publicas/privadas </div>
					<div class="panel-body">
						<div  class="col-md-12">
							<div ng-repeat="m in mensajes" class="alert alert-{{m.type||'success'}} alert-dismissible" role="alert">
								<button type="button" class="close" data-dismiss="alert" ng-click="mensajes.splice($index,1);"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
								<span ng-bind="m.text"></span>
							</div>
							<div class="form-group">
								<label class="col-md-1 control-label" for="textinput">Año:</label>
								<div class="col-md-2">
									<select ng-model="form.ano" ng-options='s.des as s.des for s in anos'  ng-init='anos = <?=$anos?>' class="form-control" required> </select>
								</div>
								<label class="col-md-1 control-label" for="textinput">Mes:</label>
								<div class="col-md-2">
									<select ng-model="form.mes" ng-options='s.id_mes as s.corto for s in meses' ng-init='meses = <?=$meses?>' class="form-control" required> </select>
								</div>
								<div class="col-md-5">
									<div class="col-md-3">
										<label class="control-label" for="textinput">Tipo de red: </label>
									</div>
									<div class="col-md-7">
										<span ng-init='tipo_redes = [{id_tipo:2,des_tipo:"Publica"},{id_tipo:3,des_tipo:"Privada"}]'></span>
										<select ng-model="form.id_tipo_red" ng-options='s.id_tipo as s.des_tipo for s in tipo_redes' class="form-control" required></select>
									</div>
								</div>
							</div>
							<div class="">
								<div class="col-md-12">
									
								</div>
								<div class="col-md-12">
									<div class="panel panel-info">
										<div class="panel-heading">
											<div class="form-group">
												<div class="col-md-2">
													<label class="control-label" for="textinput">Rubro </label>
												</div>
												<div class="col-md-2">
													<label class="control-label" for="textinput">TM </label>
												</div>
												<div class="col-md-2">
													<label class="control-label" for="textinput">Rubro </label>
												</div>
												<div class="col-md-2">
													<label class="control-label" for="textinput">TM </label>
												</div>
												<div class="col-md-2">
													<label class="control-label" for="textinput">Rubro </label>
												</div>
												<div class="col-md-2">
													<label class="control-label" for="textinput">TM </label>
												</div>
											</div>
										</div>
										<div class="panel-body">
											<div class="form-group">
												<div class="col-md-2">
													<label class="control-label" for="textinput">Aceite: </label>
												</div>
												<div class="col-md-2">
													<input  ng-model="form.rubros[1]" type="text" name="total" placeholder="0.00" class="form-control toneladas" required>
												</div>
												<div class="col-md-2">
													<label class="control-label" for="textinput">Arroz: </label>
												</div>
												<div class="col-md-2">
													<input  ng-model="form.rubros[2]" type="text" name="total" placeholder="0.00" class="form-control toneladas" required>
												</div>
												<div class="col-md-2">
													<label class="control-label" for="textinput">Arvejas: </label>
												</div>
												<div class="col-md-2">
													<input  ng-model="form.rubros[3]" type="text" name="total" placeholder="0.00" class="form-control toneladas" required>
												</div>
											</div>
											<div class="form-group">
												<div class="col-md-2">
													<label class="control-label" for="textinput">Azucar: </label>
												</div>
												<div class="col-md-2">
													<input  ng-model="form.rubros[4]" type="text" name="total" placeholder="0.00" class="form-control toneladas" required>
												</div>
												<div class="col-md-2">
													<label class="control-label" for="textinput">Azucar&#160;Glass&#160;Pulverizada: </label>
												</div>
												<div class="col-md-2">
													<input  ng-model="form.rubros[33]" type="text" name="total" placeholder="0.00" class="form-control toneladas" required>
												</div>
												<div class="col-md-2">
													<label class="control-label" for="textinput">Cafe: </label>
												</div>
												<div class="col-md-2">
													<input  ng-model="form.rubros[34]" type="text" name="total" placeholder="0.00" class="form-control toneladas" required>
												</div>
											</div>
											<div class="form-group">
												<div class="col-md-2">
													<label class="control-label" for="textinput">Caraotas: </label>
												</div>
												<div class="col-md-2">
													<input  ng-model="form.rubros[5]" type="text" name="total" placeholder="0.00" class="form-control toneladas" required>
												</div>
												<div class="col-md-2">
													<label class="control-label" for="textinput">Carne: </label>
												</div>
												<div class="col-md-2">
													<input  ng-model="form.rubros[6]" type="text" name="total" placeholder="0.00" class="form-control toneladas" required>
												</div>
												<div class="col-md-2">
													<label class="control-label" for="textinput">Carne(carnes vzla): </label>
												</div>
												<div class="col-md-2">
													<input  ng-model="form.rubros[38]" type="text" name="total" placeholder="0.00" class="form-control toneladas" required>
												</div>
											</div>
											<div class="form-group">
												<div class="col-md-2">
													<label class="control-label" for="textinput">Pernil: </label>
												</div>
												<div class="col-md-2">
													<input  ng-model="form.rubros[21]" type="text" name="total" placeholder="0.00" class="form-control toneladas" required>
												</div>
												<div class="col-md-2">
													<label class="control-label" for="textinput">Pernil(carnes vzla): </label>
												</div>
												<div class="col-md-2">
													<input  ng-model="form.rubros[39]" type="text" name="total" placeholder="0.00" class="form-control toneladas" required>
												</div>
												<div class="col-md-2">
													<label class="control-label" for="textinput">Pollo: </label>
												</div>
												<div class="col-md-2">
													<input  ng-model="form.rubros[14]" type="text" name="total" placeholder="0.00" class="form-control toneladas" required>
												</div>
											</div>
											<div class="form-group">
												<div class="col-md-2">
													<label class="control-label" for="textinput">Atun(Mercal): </label>
												</div>
												<div class="col-md-2">
													<input  ng-model="form.rubros[37]" type="text" name="total" placeholder="0.00" class="form-control toneladas" required>
												</div>
												<div class="col-md-2">
													<label class="control-label" for="textinput">Harina&#160;de&#160;Maiz&#160;Precocida: </label>
												</div>
												<div class="col-md-2">
													<input  ng-model="form.rubros[35]" type="text" name="total" placeholder="0.00" class="form-control toneladas" required>
												</div>
												<div class="col-md-2">
													<label class="control-label" for="textinput">Harina Trigo: </label>
												</div>
												<div class="col-md-2">
													<input  ng-model="form.rubros[8]" type="text" name="total" placeholder="0.00" class="form-control toneladas" required>
												</div>
											</div>
											<div class="form-group">
												<div class="col-md-2">
													<label class="control-label" for="textinput">Leche: </label>
												</div>
												<div class="col-md-2">
													<input  ng-model="form.rubros[10]" type="text" name="total" placeholder="0.00" class="form-control toneladas" required>
												</div>
												<div class="col-md-2">
													<label class="control-label" for="textinput">Leche UHT: </label>
												</div>
												<div class="col-md-2">
													<input  ng-model="form.rubros[18]" type="text" name="total" placeholder="0.00" class="form-control toneladas" required>
												</div>
												<div class="col-md-2">
													<label class="control-label" for="textinput">Leche UHT(MERCAL): </label>
												</div>
												<div class="col-md-2">
													<input  ng-model="form.rubros[40]" type="text" name="total" placeholder="0.00" class="form-control toneladas" required>
												</div>
											</div>
											<div class="form-group">
												<div class="col-md-2">
													<label class="control-label" for="textinput">Lentejas: </label>
												</div>
												<div class="col-md-2">
													<input  ng-model="form.rubros[9]" type="text" name="total" placeholder="0.00" class="form-control toneladas" required>
												</div>
												<div class="col-md-2">
													<label class="control-label" for="textinput">Margarina: </label>
												</div>
												<div class="col-md-2">
													<input  ng-model="form.rubros[11]" type="text" name="total" placeholder="0.00" class="form-control toneladas" required>
												</div>
												<div class="col-md-2">
													<label class="control-label" for="textinput">Palmito: </label>
												</div>
												<div class="col-md-2">
													<input  ng-model="form.rubros[20]" type="text" name="total" placeholder="0.00" class="form-control toneladas" required>
												</div>
											</div>
											<div class="form-group">
												<div class="col-md-2">
													<label class="control-label" for="textinput">Pasta: </label>
												</div>
												<div class="col-md-2">
													<input  ng-model="form.rubros[13]" type="text" name="total" placeholder="0.00" class="form-control toneladas" required>
												</div>
												<div class="col-md-2">
													<label class="control-label" for="textinput">Jarabe de Fructosa y Glucosa: </label>
												</div>
												<div class="col-md-2">
													<input  ng-model="form.rubros[36]" type="text" name="total" placeholder="0.00" class="form-control toneladas" required>
												</div>
											</div>
										</div>
										<!-- <div class="panel-footer clearfix">
											<div class="pull-right">
												<button type="button" class="btn btn-labeled btn-info " ng-click="agregar(form)">
													<span class="btn-label"><i class="fa fa-plus-circle"></i> </span>Agregar Rubro
												</button>
											</div>
										</div> -->
									</div>
								</div>
								
							</div>
						</div>
					</div>
					<div class="panel-footer clearfix" >
						<div class="pull-right">
							<span >
								<button type="reset" ng-click="atras()" class="btn btn-default">Cancelar</button>
								<button ng-click="actualizar(form)" class="btn btn-primary">Guardar Cambios</button>
							</span>
						</div>
					</div>
				</div>
			</div>
		</section>
         <!-- END Page content-->
    </section>
    <!-- END Main section-->



<?php $this->load->view('common/admin_footer');?>
