
<!-- END Main section-->
</section>
<!-- END Main wrapper-->
<!-- START Scripts-->
<!-- Main vendor Scripts-->
<script src="<?php echo $adminResPath;?>vendor/jquery/jquery.min.js"></script>
<script src="<?php echo $adminResPath;?>vendor/bootstrap/js/bootstrap.min.js"></script>
<!-- Plugins-->
<script src="<?php echo $adminResPath;?>vendor/chosen/chosen.jquery.min.js"></script>
<script src="<?php echo $adminResPath;?>vendor/slider/js/bootstrap-slider.js"></script>
<script src="<?php echo $adminResPath;?>vendor/filestyle/bootstrap-filestyle.min.js"></script>
<!-- Animo-->
<script src="<?php echo $adminResPath;?>vendor/animo/animo.min.js"></script>
<!-- Sparklines-->
<script src="<?php echo $adminResPath;?>vendor/sparklines/jquery.sparkline.min.js"></script>
<!-- Slimscroll-->
<script src="<?php echo $adminResPath;?>vendor/slimscroll/jquery.slimscroll.min.js"></script>
<!-- START Page Custom Script-->
<!-- Markdown Area Codemirror and dependencies -->
<script src="<?php echo $adminResPath;?>vendor/codemirror/lib/codemirror.js"></script>
<script src="<?php echo $adminResPath;?>vendor/codemirror/addon/mode/overlay.js"></script>
<script src="<?php echo $adminResPath;?>vendor/codemirror/mode/markdown/markdown.js"></script>
<script src="<?php echo $adminResPath;?>vendor/codemirror/mode/xml/xml.js"></script>
<script src="<?php echo $adminResPath;?>vendor/codemirror/mode/gfm/gfm.js"></script>
<script src="<?php echo $adminResPath;?>vendor/marked/marked.js"></script>
<!-- MomentJs and Datepicker-->
<script src="<?php echo $adminResPath;?>vendor/moment/min/moment-with-langs.min.js"></script>
<script src="<?php echo $adminResPath;?>vendor/datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
<!-- Tags input-->
<script src="<?php echo $adminResPath;?>vendor/tagsinput/bootstrap-tagsinput.min.js"></script>
<!-- Input Mask-->
<script src="<?php echo $adminResPath;?>vendor/inputmask/jquery.inputmask.bundle.min.js"></script>
<!-- END Page Custom Script-->

<!--[if lt IE 8]><script src="js/excanvas.min.js"></script><![endif]-->
<!-- END Page Custom Script-->
<!-- App Main-->
<script src="<?php echo $adminResPath;?>app/js/app.js"></script>
<!--业务js-->
<script src="<?php echo $adminResPath;?>app/js/app.biz.js"></script>
<!-- END Scripts-->
</body>

</html>