<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie ie6 lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="ie ie7 lt-ie9 lt-ie8"        lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="ie ie8 lt-ie9"               lang="en"> <![endif]-->
<!--[if IE 9]>    <html class="ie ie9"                      lang="en"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-ie">
<!--<![endif]-->

<head>
    <!-- Meta-->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
    <meta name="description" content="mercal">
    <meta name="keywords" content="distribucion,boostrap,angularjs,jQuery,codeIgniter">
    <meta name="author" content="Jose Angel Delgado">
    <meta name="telefono" content="+58-412-0309959">
    <meta name="email" content="esojangel@gmail.com">
    
    <title>Estadistica - Inicio</title>
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]><script src="<?php echo $publicPath;?>/html5shiv.js"></script><script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script><![endif]-->
    <?php load_css($css); ?>
    <!-- END Page Custom CSS-->
    <script src="<?php echo $adminResPath;?>vendor/modernizr/modernizr.js" type="application/javascript"></script>
    <!-- FastClick for mobiles-->
    <script src="<?php echo $adminResPath;?>vendor/fastclick/fastclick.js" type="application/javascript"></script>
    <script type="text/javascript">
        window.prefixDomain = '<?=site_url("/")?>';
    </script>
    <style type="text/css">
        .datepicker >.datepicker-dropdown >.dropdown-menu> .datepicker-orient-left> .datepicker-orient-top{
            z-index: 9999 !important;
        }
    </style>
</head>
<body>
<!-- START Main wrapper-->
<section class="wrapper">
<!-- START Top Navbar-->
<nav role="navigation" class="navbar navbar-default navbar-top navbar-fixed-top" style="z-index: 20;">
<!-- START navbar header-->
<div class="navbar-header">
    <a href="<?php echo base_url();?>" class="navbar-brand">
        <div class="brand-logo">Estadistica</div>
        <div class="brand-logo-collapsed">Es<i class="fa fa-bar-chart-o"></i></div>
    </a>
</div>
<!-- END navbar header-->
<!-- START Nav wrapper-->
<div class="nav-wrapper" >
<!-- START Left navbar-->
<ul class="nav navbar-nav">
    <li>
        <a href="#" data-toggle="aside">
            <em class="fa fa-align-left"></em>
        </a>
    </li>
    <!--<li>
        <a href="#" data-toggle="navbar-search">
            <em class="fa fa-search"></em>
        </a>
    </li>-->
    <li>
       <!--  <a href="<?php //echo site_url('dashboard/post_create');?>">
            <em class="fa fa-pencil"></em>
        </a> -->
    </li>
</ul>
<!-- END Left navbar-->
<!-- START Right Navbar-->
<ul class="nav navbar-nav navbar-right">
    <!-- START Messages menu (dropdown-list)-->
    <li class="dropdown dropdown-list">
        <img style="z-index: -1!important" src="<?php echo base_url()?>public/admin/app/img/cintillo.png">
        <!--<a href="#" data-toggle="dropdown" data-play="bounceIn" class="dropdown-toggle">
            <em class="fa fa-envelope"></em>
            <div class="label label-danger">300</div>
        </a>-->
    
    </li>

</ul>
<!-- END Right Navbar-->
</div>
<!-- END Nav wrapper-->
<!-- START Search form-->
<form role="search" class="navbar-form">
    <div class="form-group has-feedback">
        <input type="text" placeholder="Type and hit Enter.." class="form-control">
        <div data-toggle="navbar-search-dismiss" class="fa fa-times form-control-feedback"></div>
    </div>
    <button type="submit" class="hidden btn btn-default">Submit</button>
</form>
<!-- END Search form-->
</nav>
<!-- END Top Navbar-->
<!-- START aside-->
<aside class="aside">
    <!-- START Sidebar (left)-->
    <nav class="sidebar">
        <ul class="nav">
            <!-- START user info-->
            <li>
                <div data-toggle="collapse-next" class="item user-block has-submenu">
                    <!-- User picture-->
                    <div class="user-block-picture">
                        <img src="<?php echo base_url()?>public/admin/app/img/logocasa.gif" alt="Avatar" width="60" height="60" class="img-thumbnail img-circle">
                        <!-- <img src="<?php echo base_url()?>public/front/img/profile.png" alt="Avatar" width="60" height="60" class="img-thumbnail img-circle"> -->

                    </div>
                    <!-- Name and Role-->
                    <div class="user-block-info">
                        <span class="user-block-name item-text"><?php echo $curUser['username'];?></span>
                        <span class="user-block-role"><?php echo $curUser['nombre'];?></span>

                    </div>
                </div>
                <!-- START User links collapse-->
                <ul class="nav collapse">
                    <li><a href="#">Correo: <?php echo $curUser['correo'];?> </a>
                    </li>

                    <li><a href="#">rol: <?php echo $curUser['rol'];?></a>
                    </li>

                </ul>
                <!-- END User links collapse-->
            </li>
            <!-- END user info-->
            <!-- START Menu-->

            <!-- <li class="<?php echo get_active_nav_class('consultas');?>">
                <a href="<?php echo site_url('consultas/p');?>" title="consultas">
                    <em class="fa fa-bar-chart-o"></em>
                    <span class="item-text">Grafico     </span>
                </a>
            </li> -->
            <li class="<?php echo get_active_nav_class('consultas');?>">
                <a href="<?php echo site_url('consultas');?>" title="consultas">
                    <em class="fa fa-file-pdf-o"></em>
                    <span class="item-text">Reportes</span>
                </a>
            </li>
            <li class="<?php echo get_active_nav_class('planificacion');?>">
                <a href="javascript:;" title="Planificación" data-toggle="collapse-next" class="has-submenu">
                    <em class="fa fa-book"></em>
                    <!-- <div class="label label-primary pull-right">new</div> -->
                    <span class="item-text">Planificación</span>
                </a>
                <!-- START SubMenu item-->
                <ul class="nav collapse ">
                    <li>
                        <a href="<?php echo site_url('planificacion');?>" title="Distribución Mercal" data-toggle="" class="no-submenu">
                            <span class="item-text">Nuevo Proyecto</span>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo site_url('planificacion/ver');?>" title="" data-toggle="" class="no-submenu">
                            <span class="item-text">Ver Proyectos</span>
                        </a>
                    </li>
                </ul>
                <!-- END SubMenu item-->
            </li> 

            <li class="<?php echo get_active_nav_class('asa');?>">
                <a href="javascript:;" title="Distribución" data-toggle="collapse-next" class="has-submenu">
                    <em class="fa fa-file-text"></em>
                    <!-- <div class="label label-primary pull-right">new</div> -->
                    <span class="item-text">Distribución</span>
                </a>
                <!-- START SubMenu item-->
                <ul class="nav collapse ">
                    <li>
                        <a href="<?php echo site_url('distribucion/mercal');?>" title="Distribución Mercal" data-toggle="" class="no-submenu">
                            <span class="item-text">Mercal</span>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo site_url('distribucion/ver');?>" title="" data-toggle="" class="no-submenu">
                            <span class="item-text">Ver&#160;Dist.&#160;Mercal</span>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo site_url('distribucion/otras_redes_primas');?>" title="" data-toggle="" class="no-submenu">
                            <span class="item-text">Otras&#160;Redes-Materia&#160;prima</span>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo site_url('distribucion/otras_redes_terminados');?>" title="" data-toggle="" class="no-submenu">
                            <span class="item-text">Otras&#160;Redes-Producto&#160;terminados</span>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo site_url('distribucion/listar_red_materia_prima');?>" title="" data-toggle="" class="no-submenu">
                            <span class="item-text">Listar-Materia&#160;prima</span>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo site_url('distribucion/listar_red_productos_terminados');?>" title="" data-toggle="" class="no-submenu">
                            <span class="item-text">Listar-Producto&#160;terminados</span>
                        </a>
                    </li>
                </ul>
                <!-- END SubMenu item-->
            </li>
            <li class="<?php echo get_active_nav_class('asa');?>">
                <a href="javascript:;" title="Distribución" data-toggle="collapse-next" class="has-submenu">
                    <em class="fa fa-file-o"></em>
                    <!-- <div class="label label-primary pull-right">new</div> -->
                    <span class="item-text">Ventas a otras redes publicas y privadas</span>
                </a>
                <!-- START SubMenu item-->
                <ul class="nav collapse ">
                    <li>
                        <a href="<?php echo site_url('distribucion/ventas_otras_redes_pp');?>" title="" data-toggle="" class="no-submenu">
                            <span class="item-text">Registrar</span>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo site_url('distribucion/list_ventas_otras_redes');?>" title="" data-toggle="" class="no-submenu">
                            <span class="item-text">Listar</span>
                        </a>
                    </li>
                </ul>
                <!-- END SubMenu item-->
            </li>
            <?php if($curUser['id_rol']==1){?>
            <li class="<?php echo get_active_nav_class('mantenimiento');?>">
                <a href="#" title="Mantenimiento" data-toggle="collapse-next" class="has-submenu">
                    <em class="fa fa-wrench"></em>
                    <span class="item-text">Mantenimiento</span>
                </a>
                <!-- START SubMenu item-->
                <ul class="nav collapse ">

                    <li>
                        <a href="<?php echo site_url('mantenimiento/usuarios');?>" title="Mantenimiento de usuarios" data-toggle="" class="no-submenu">
                            <span class="item-text">Usuarios</span>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo site_url('mantenimiento/cambiarClave');?>" title="Cambio de Clave a los usuarios" data-toggle="" class="no-submenu">
                            <span class="item-text">Cambio de clave a usuarios</span>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo site_url('mantenimiento/backup');?>" title="Respaldar DataBase" data-toggle="" class="no-submenu">
                            <span class="item-text">Respaldar DataBase</span>
                        </a>
                    </li>
                </ul>
                <!-- END SubMenu item-->
            </li>
            <?php }?>
            <li class="<?php echo get_active_nav_class('mantenimiento/cambiarmiclave');?>">
            <a href="<?php echo site_url('mantenimiento/cambiarmiclave');?>" title="Cambiar mi clave">
                    <em class="fa fa-key"></em>
                    <!-- <div class="label label-primary pull-right">12</div> -->
                    <span class="item-text">Cambiar mi clave</span>
                </a>
            </li>
            <!-- END Menu-->
            <!-- Sidebar footer    -->
            <li class="nav-footer">
                <div class="nav-footer-divider"></div>
                <!-- START button group-->
                <div class="btn-group text-center">
                    <!-- <button type="button" data-toggle="tooltip" data-title="Add Contact" class="btn btn-link">
                        <em class="fa fa-user text-muted"><sup class="fa fa-plus"></sup>
                        </em>
                    </button> -->
                    <a href="<?php echo site_url('auth/logout');?>" data-toggle="tooltip" data-title="Cerrar Sesion" class="btn btn-link">
                        <em class="fa fa-sign-out text-muted"></em>
                    </a>
                </div>
                <!-- END button group-->
            </li>
        </ul>
    </nav>
    <!-- END Sidebar (left)-->
</aside>
<!-- End aside-->