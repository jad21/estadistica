<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie ie6 lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="ie ie7 lt-ie9 lt-ie8"        lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="ie ie8 lt-ie9"               lang="en"> <![endif]-->
<!--[if IE 9]>    <html class="ie ie9"                      lang="en"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-ie">
<!--<![endif]-->

<head>
    <!-- Meta-->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="">
    <title>HiBlog - Dashboard</title>
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]><script src="<?php echo $publicPath;?>/html5shiv.js"></script><script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script><![endif]-->
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="<?php echo $adminResPath;?>app/css/bootstrap.css">
    <!-- Vendor CSS-->
    <link rel="stylesheet" href="<?php echo $adminResPath;?>vendor/fontawesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo $adminResPath;?>vendor/animo/animate+animo.css">
    <link rel="stylesheet" href="<?php echo $adminResPath;?>vendor/csspinner/csspinner.min.css">
    <!-- START Page Custom CSS-->
    <link rel="stylesheet" href="<?php echo $adminResPath;?>vendor/slider/css/slider.css">
    <link rel="stylesheet" href="<?php echo $adminResPath;?>vendor/chosen/chosen.min.css">
    <link rel="stylesheet" href="<?php echo $adminResPath;?>vendor/datetimepicker/css/bootstrap-datetimepicker.min.css">
    <!-- Codemirror -->
    <link rel="stylesheet" href="<?php echo $adminResPath;?>vendor/codemirror/lib/codemirror.css">
    <!-- Bootstrap tags-->
    <link rel="stylesheet" href="<?php echo $adminResPath;?>vendor/tagsinput/bootstrap-tagsinput.css">
    <!-- END Page Custom CSS-->

    <!-- App CSS-->
    <link rel="stylesheet" href="<?php echo $adminResPath;?>app/css/app.css">
    <!-- Modernizr JS Script-->
    <script src="<?php echo $adminResPath;?>vendor/modernizr/modernizr.js" type="application/javascript"></script>
    <!-- FastClick for mobiles-->
    <script src="<?php echo $adminResPath;?>vendor/fastclick/fastclick.js" type="application/javascript"></script>
</head>

<body>
<!-- START Main wrapper-->
<section class="wrapper">
<!-- START Top Navbar-->
<nav role="navigation" class="navbar navbar-default navbar-top navbar-fixed-top">
<!-- START navbar header-->
<div class="navbar-header">
    <a href="<?php echo base_url();?>" class="navbar-brand">
        <div class="brand-logo">HiBlog</div>
        <div class="brand-logo-collapsed">HB</div>
    </a>
</div>
<!-- END navbar header-->
<!-- START Nav wrapper-->
<div class="nav-wrapper">
<!-- START Left navbar-->
<ul class="nav navbar-nav">
    <li>
        <a href="#" data-toggle="aside">
            <em class="fa fa-align-left"></em>
        </a>
    </li>
    <!--<li>
        <a href="#" data-toggle="navbar-search">
            <em class="fa fa-search"></em>
        </a>
    </li>-->
    <li>
        <a href="<?php echo site_url('dashboard/post_create');?>">
            <em class="fa fa-pencil"></em>
        </a>
    </li>
</ul>
<!-- END Left navbar-->
<!-- START Right Navbar-->
<ul class="nav navbar-nav navbar-right">
    <!-- START Messages menu (dropdown-list)-->
    <li class="dropdown dropdown-list">
        <!--<a href="#" data-toggle="dropdown" data-play="bounceIn" class="dropdown-toggle">
            <em class="fa fa-envelope"></em>
            <div class="label label-danger">300</div>
        </a>-->
        <!-- START Dropdown menu-->
        <ul class="dropdown-menu">
            <li class="dropdown-menu-header">You have 5 new messages</li>
            <li>
                <div class="scroll-viewport">
                    <!-- START list group-->
                    <div class="list-group scroll-content">
                        <!-- START list group item-->
                        <a href="#" class="list-group-item">
                            <div class="media">
                                <div class="pull-left">
                                    <img style="width: 48px; height: 48px;" src="<?php echo $adminResPath;?>app/img/user/01.jpg" alt="Image" class="media-object img-rounded">
                                </div>
                                <div class="media-body clearfix">
                                    <small class="pull-right">2h</small>
                                    <strong class="media-heading text-primary">
                                        <div class="point point-success point-lg"></div>Sheila Carter</strong>
                                    <p class="mb-sm">
                                        <small>Cras sit amet nibh libero, in gravida nulla. Nulla...</small>
                                    </p>
                                </div>
                            </div>
                        </a>
                        <!-- END list group item-->
                        <!-- START list group item-->
                        <a href="#" class="list-group-item">
                            <div class="media">
                                <div class="pull-left">
                                    <img style="width: 48px; height: 48px;" src="<?php echo $adminResPath;?>app/img/user/04.jpg" alt="Image" class="media-object img-rounded">
                                </div>
                                <div class="media-body clearfix">
                                    <small class="pull-right">3h</small>
                                    <strong class="media-heading text-primary">
                                        <div class="point point-success point-lg"></div>Rich Reynolds</strong>
                                    <p class="mb-sm">
                                        <small>Cras sit amet nibh libero, in gravida nulla. Nulla...</small>
                                    </p>
                                </div>
                            </div>
                        </a>
                        <!-- END list group item-->
                        <!-- START list group item-->
                        <a href="#" class="list-group-item">
                            <div class="media">
                                <div class="pull-left">
                                    <img style="width: 48px; height: 48px;" src="<?php echo $adminResPath;?>app/img/user/03.jpg" alt="Image" class="media-object img-rounded">
                                </div>
                                <div class="media-body clearfix">
                                    <small class="pull-right">4h</small>
                                    <strong class="media-heading text-primary">
                                        <div class="point point-danger point-lg"></div>Beverley Pierce</strong>
                                    <p class="mb-sm">
                                        <small>Cras sit amet nibh libero, in gravida nulla. Nulla...</small>
                                    </p>
                                </div>
                            </div>
                        </a>
                        <!-- END list group item-->
                        <!-- START list group item-->
                        <a href="#" class="list-group-item">
                            <div class="media">
                                <div class="pull-left">
                                    <img style="width: 48px; height: 48px;" src="<?php echo $adminResPath;?>app/img/user/05.jpg" alt="Image" class="media-object img-rounded">
                                </div>
                                <div class="media-body clearfix">
                                    <small class="pull-right">4h</small>
                                    <strong class="media-heading text-primary">
                                        <div class="point point-danger point-lg"></div>Perry Cole</strong>
                                    <p class="mb-sm">
                                        <small>Cras sit amet nibh libero, in gravida nulla. Nulla...</small>
                                    </p>
                                </div>
                            </div>
                        </a>
                        <!-- END list group item-->
                        <!-- START list group item-->
                        <a href="#" class="list-group-item">
                            <div class="media">
                                <div class="pull-left">
                                    <img style="width: 48px; height: 48px;" src="<?php echo $adminResPath;?>app/img/user/06.jpg" alt="Image" class="media-object img-rounded">
                                </div>
                                <div class="media-body clearfix">
                                    <small class="pull-right">4h</small>
                                    <strong class="media-heading text-primary">
                                        <div class="point point-danger point-lg"></div>Carolyn Carpenter</strong>
                                    <p class="mb-sm">
                                        <small>Cras sit amet nibh libero, in gravida nulla. Nulla...</small>
                                    </p>
                                </div>
                            </div>
                        </a>
                        <!-- END list group item-->
                    </div>
                    <!-- END list group-->
                </div>
            </li>
            <!-- START dropdown footer-->
            <li class="p">
                <a href="#" class="text-center">
                    <small class="text-primary">READ ALL</small>
                </a>
            </li>
            <!-- END dropdown footer-->
        </ul>
        <!-- END Dropdown menu-->
    </li>
    <!-- END Messages menu (dropdown-list)-->
    <!-- START Alert menu-->
    <li class="dropdown dropdown-list">
        <!--<a href="#" data-toggle="dropdown" data-play="bounceIn" class="dropdown-toggle">
            <em class="fa fa-bell"></em>
            <div class="label label-info">120</div>
        </a>-->
        <!-- START Dropdown menu-->
        <ul class="dropdown-menu">
            <li>
                <!-- START list group-->
                <div class="list-group">
                    <!-- list item-->
                    <a href="#" class="list-group-item">
                        <div class="media">
                            <div class="pull-left">
                                <em class="fa fa-envelope-o fa-2x text-success"></em>
                            </div>
                            <div class="media-body clearfix">
                                <div class="media-heading">Unread messages</div>
                                <p class="m0">
                                    <small>You have 10 unread messages</small>
                                </p>
                            </div>
                        </div>
                    </a>
                    <!-- list item-->
                    <a href="#" class="list-group-item">
                        <div class="media">
                            <div class="pull-left">
                                <em class="fa fa-cog fa-2x"></em>
                            </div>
                            <div class="media-body clearfix">
                                <div class="media-heading">New settings</div>
                                <p class="m0">
                                    <small>There are new settings available</small>
                                </p>
                            </div>
                        </div>
                    </a>
                    <!-- list item-->
                    <a href="#" class="list-group-item">
                        <div class="media">
                            <div class="pull-left">
                                <em class="fa fa-fire fa-2x"></em>
                            </div>
                            <div class="media-body clearfix">
                                <div class="media-heading">Updates</div>
                                <p class="m0">
                                    <small>There are
                                        <span class="text-primary">2</span>new updates available</small>
                                </p>
                            </div>
                        </div>
                    </a>
                    <!-- last list item -->
                    <a href="#" class="list-group-item">
                        <small>Unread notifications</small>
                        <span class="badge">14</span>
                    </a>
                </div>
                <!-- END list group-->
            </li>
        </ul>
        <!-- END Dropdown menu-->
    </li>
    <!-- END Alert menu-->
</ul>
<!-- END Right Navbar-->
</div>
<!-- END Nav wrapper-->
<!-- START Search form-->
<form role="search" class="navbar-form">
    <div class="form-group has-feedback">
        <input type="text" placeholder="Type and hit Enter.." class="form-control">
        <div data-toggle="navbar-search-dismiss" class="fa fa-times form-control-feedback"></div>
    </div>
    <button type="submit" class="hidden btn btn-default">Submit</button>
</form>
<!-- END Search form-->
</nav>
<!-- END Top Navbar-->
<!-- START aside-->
<aside class="aside">
    <!-- START Sidebar (left)-->
    <nav class="sidebar">
        <ul class="nav">
            <!-- START user info-->
            <li>
                <div data-toggle="collapse-next" class="item user-block has-submenu">
                    <!-- User picture-->
                    <div class="user-block-picture">
                        <img src="<?php echo $curUser['avatar'];?>" alt="Avatar" width="60" height="60" class="img-thumbnail img-circle">

                    </div>
                    <!-- Name and Role-->
                    <div class="user-block-info">
                        <span class="user-block-name item-text"><?php echo $curUser['username'];?></span>
                        <span class="user-block-role">Life writer.</span>

                    </div>
                </div>
                <!-- START User links collapse-->
                <ul class="nav collapse">
                    <li><a href="#">Profile</a>
                    </li>

                    <li><a href="#">About Me</a>
                    </li>

                </ul>
                <!-- END User links collapse-->
            </li>
            <!-- END user info-->
            <!-- START Menu-->
            <li class="<?php echo get_active_nav_class('index');?>">
                <a href="<?php echo site_url('dashboard/index');?>" title="Dashboard">
                    <em class="fa fa-dashboard"></em>
                    <!--<div class="label label-primary pull-right">12</div>-->
                    <span class="item-text">Dashboard</span>
                </a>
            </li>

            <li class="<?php echo get_active_nav_class('post');?>">
                <a href="#" title="Pages" data-toggle="collapse-next" class="has-submenu">
                    <em class="fa fa-file-text"></em>
                    <!--<div class="label label-primary pull-right">new</div>-->
                    <span class="item-text">Words</span>
                </a>
                <!-- START SubMenu item-->
                <ul class="nav collapse ">
                    <li>
                        <a href="<?php echo site_url('dashboard/post_create');?>" title="Words create" data-toggle="" class="no-submenu">
                            <span class="item-text">Write words</span>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo site_url('dashboard/post_list');?>" title="Words list" data-toggle="" class="no-submenu">
                            <span class="item-text">All Words</span>
                        </a>
                    </li>
                </ul>
                <!-- END SubMenu item-->
            </li>



            <li class="<?php echo get_active_nav_class('setting');?>">
                <a href="#" title="Settings" data-toggle="collapse-next" class="has-submenu">
                    <em class="fa fa-wrench"></em>
                    <span class="item-text">Settings</span>
                </a>
                <!-- START SubMenu item-->
                <ul class="nav collapse ">

                    <li>
                        <a href="typo.html" title="Typography" data-toggle="" class="no-submenu">
                            <span class="item-text">Typography</span>
                        </a>
                    </li>
                    <li>
                        <a href="grid.html" title="Grid" data-toggle="" class="no-submenu">
                            <span class="item-text">Grid</span>
                        </a>
                    </li>
                </ul>
                <!-- END SubMenu item-->
            </li>

            <!-- END Menu-->
            <!-- Sidebar footer    -->
            <li class="nav-footer">
                <div class="nav-footer-divider"></div>
                <!-- START button group-->
                <div class="btn-group text-center">
                    <button type="button" data-toggle="tooltip" data-title="Add Contact" class="btn btn-link">
                        <em class="fa fa-user text-muted"><sup class="fa fa-plus"></sup>
                        </em>
                    </button>
                    <button type="button" data-toggle="tooltip" data-title="Logout" class="btn btn-link">
                        <em class="fa fa-sign-out text-muted"></em>
                    </button>
                </div>
                <!-- END button group-->
            </li>
        </ul>
    </nav>
    <!-- END Sidebar (left)-->
</aside>
<!-- End aside-->