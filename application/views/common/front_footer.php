<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <span class="copyright">Copyright &copy; Your Website 2014</span>
            </div>
            <div class="col-md-4">
                <ul class="list-inline social-buttons">
                    <li><a href="#"><i class="fa fa-twitter"></i></a>
                    </li>
                    <li><a href="#"><i class="fa fa-facebook"></i></a>
                    </li>
                    <li><a href="#"><i class="fa fa-linkedin"></i></a>
                    </li>
                </ul>
            </div>
            <div class="col-md-4">
                <ul class="list-inline quicklinks">
                    <li><a href="#">Privacy Policy</a>
                    </li>
                    <li><a href="#">Terms of Use</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</footer>

<!-- jQuery -->
<script src="<?php echo $frontResPath;?>js/jquery.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="<?php echo $frontResPath;?>js/bootstrap.min.js"></script>

<!-- Plugin JavaScript -->
<script src="<?php echo $publicPath;?>js/jquery.easing.min.js"></script>
<script src="<?php echo $frontResPath;?>js/classie.js"></script>
<script src="<?php echo $frontResPath;?>js/cbpAnimatedHeader.js"></script>

<!-- Contact Form JavaScript -->
<script src="<?php echo $frontResPath;?>js/jqBootstrapValidation.js"></script>
<script src="<?php echo $frontResPath;?>js/contact_me.js"></script>

<!-- Custom Theme JavaScript -->
<script src="<?php echo $frontResPath;?>js/agency.js"></script>

</body>

</html>
