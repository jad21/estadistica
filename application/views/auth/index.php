<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie ie6 lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="ie ie7 lt-ie9 lt-ie8"        lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="ie ie8 lt-ie9"               lang="en"> <![endif]-->
<!--[if IE 9]>    <html class="ie ie9"                      lang="en"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-ie">
<!--<![endif]-->

<head>
   <!-- Meta-->
   <meta charset="utf-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
   <meta name="description" content="">
   <meta name="keywords" content="">
   <meta name="author" content="">
   <title>Estadistica - inicia session</title>
   <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
   <!--[if lt IE 9]><script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script><script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script><![endif]-->
   <?php load_css($css);?>
   <!-- Modernizr JS Script-->
	<script src="<?php echo $adminResPath;?>vendor/modernizr/modernizr.js" type="application/javascript"></script>
	<!-- FastClick for mobiles-->
	<script src="<?php echo $adminResPath;?>vendor/fastclick/fastclick.js" type="application/javascript"></script>
	<script type="text/javascript">
		window.BASE = "<?=base_url()?>";
	</script>
</head>

<body>
   <!-- START wrapper-->
   <div style="height: 100%; padding: 50px 0; background-color: #2c3037" class="row row-table">
	  <div class="col-lg-3 col-md-6 col-sm-8 col-xs-12 align-middle">
		 <!-- START panel-->
		 <div data-toggle="play-animation" data-play="fadeInUp" data-offset="0" class="panel panel-default panel-flat">
			<p class="text-center mb-lg">
			   <br>
			   <a href="#">
			   
				  <img src="<?php echo base_url()?>public/admin/app/img/logocasa.gif" alt="Imagen de logo" class="block-center img-rounded img-thumbnail " width="150" height="150" >
			   </a>
			</p>
			<p class="text-center mb-lg">
			   <strong>INICIA PARA CONTINUAR.</strong>
			</p>
			<div class="panel-body">
			   <form role="form" class="mb-lg" method="POST" action="<?=site_url("/").'auth/login'?>">
				  <!-- <div class="text-right mb-sm"><a href="#" class="text-muted">Need to Signup?</a>
				  </div> -->
				  <?php $error = $this->session->flashdata('msg::error');
				  if($error){?>
				  <div class="alert alert-danger" role="alert"><?=$error?></div>
				  <?php }?>
				  <div class="alert" id="alert" role="alert" style="display: none;"></div>

				  
				  <div class="form-group has-feedback">
					 <input id="exampleInputEmail1" type="text" placeholder="Usuario" name="username" class="form-control">
					 <span class="fa fa-user form-control-feedback text-muted"></span>
				  </div>
				  <div class="form-group has-feedback">
					 <input id="exampleInputPassword1" type="password" placeholder="Contraseña" name="password" class="form-control">
					 <span class="fa fa-lock form-control-feedback text-muted"></span>
				  </div>
				  <!-- <div class="clearfix">
					 <div class="checkbox c-checkbox pull-left mt0">
						<label>
						   <input type="checkbox" value="">
						   <span class="fa fa-check"></span>Remember Me
						</label>
					 </div>
					 <div class="pull-right"><a href="#" class="text-muted">Forgot your password?</a>
					 </div>
				  </div> -->
				  <button type="submit" class="btn btn-block btn-primary">Entrar</button>
				  <div class="pull-right"> <a data-toggle="modal" data-target=".bs-example-modal-sm" href="javascript:void(0);">Olvide mi contraseña</a></div>
			   </form>
			</div>
		 </div>
		 <!-- END panel-->
	  </div>
    </div>

    <div id="modal" class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
		<div class="modal-dialog modal-sm">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="exampleModalLabel">Solicitar contraseña</h4>
				</div>
				<div class="modal-body">
					<form >
						<div class="form-group">
							<label for="email" class="control-label">Correo Electronico:</label>
							<input type="text" class="form-control" id="email" required>

						</div>
					</form>
					<p><small>La contraseña le sera enviada a su correo</small></p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-block btn-primary" id="btnSolicitud" onclick="auth.solicitarClave($(this))">Solicitar</button>
					<button type="button" class="btn btn-block btn-default" data-dismiss="modal">Cerrar</button>
				</div>
			</div>
		</div>
    </div>
   <?php load_js($js) ?>
</body>

</html>