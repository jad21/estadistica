<?php $this->load->view('common/admin_header');?>
<!-- START Main section-->
<section ng-app="#app.mantenimiento">
	<!-- START Page content-->
	<section class="main-content" ng-controller="UsuarioCtrl">
		<span ng-init='prefixDomain="<?=site_url('/')?>"'></span>
			<span  class="form-horizontal" role="form" >
				<div class="panel panel-default ">
					<div class="panel-heading">Cambiar mi clave </div>
					<div class="panel-body">
						<div >
							<div ng-repeat="m in mensajes" class="alert alert-{{m.type||'success'}} alert-dismissible" role="alert">
								<button type="button" class="close" data-dismiss="alert" ng-click="mensajes.splice($index,1);"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
								<span ng-bind="m.text"></span>
							</div>
							<div class="form-group">
								<label class="col-md-3 control-label" for="textinput">Contraseña actual:</label>
								<div class="col-md-6">
									<input ng-model="form.password_actual" type="password" name="password" placeholder="Contraseña actual" class="form-control" ng-required="true" >
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-3 control-label" for="textinput">Contraseña nueva:</label>
								<div class="col-md-6">
									<input ng-model="form.password" type="password" name="password" placeholder="Contraseña nueva" class="form-control" ng-required="true" >
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-3 control-label" for="textinput">Repita contraseña nueva:</label>
								<div class="col-md-6">
									<input ng-model="password2" type="password" name="password2" placeholder="Repita la contraseña" class="form-control" ng-required="true" >
								</div>
							</div>
							
							<div class="col-md-offset-2 col-md-10">
							</div>
						</div>
					</div>
					<div class="panel-footer clearfix" >
						<div class="pull-right">
							<button type="reset" ng-click="atras()" class="btn btn-default">Cancelar</button>
							<button ng-click="cambiarMiClave(form)" class="btn btn-primary" >Cambiar</button>
						</div>
					</div>
				</div>
			</span>
		</section>
         <!-- END Page content-->
    </section>
    <!-- END Main section-->

<?php $this->load->view('common/admin_footer');?>
