<?php $this->load->view('common/admin_header');?>
<!-- START Main section-->
<section ng-app="#app.mantenimiento">
	<!-- START Page content-->
	<section class="main-content" ng-controller="UsuarioCtrl">
		<span ng-init='prefixDomain="<?=base_url()?>"'></span>
			<!-- <button type="button" class="btn btn-labeled btn-primary pull-right">
			<span class="btn-label"><i class="fa fa-plus-circle"></i>
			</span>Add Item</button> -->
			<h3>Mantenimiento de Usuarios
				<!-- <br> -->
				<!-- <small>Procesar Usuario</small> -->
			</h3>
			<div  class="form-horizontal" role="form" >
				<div class="panel panel-default ">
					<div class="panel-heading">Procesar Usuario</div>
					<div class="panel-body">
						<div >
							<div ng-repeat="m in mensajes" class="alert alert-{{m.type||'success'}} alert-dismissible" role="alert">
								<button type="button" class="close" data-dismiss="alert" ng-click="mensajes.splice($index,1);"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
								<span ng-bind="m.text"></span>
							</div>
							<div class="form-group">
								<label class="col-md-3 control-label" for="textinput">Nombre&nbsp;de&nbsp;usuario: </label>
								<div class="col-md-6">
									<div class='input-group'>
										<input ng-model="form.username" type="text" name="username" placeholder="usuario" class="form-control" ng-disabled="accion == acciones[1]" required>
										<span class="input-group-btn ">
											<a ng-click="buscar(form)" data-ng-disabled="!(form.username && form.username.length>3)" class="btn btn-default"  type="submit"><i class=" glyphicon glyphicon-search "></i></a>
										</span>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-3 control-label" for="textinput">Nombre&nbsp;completo: </label>
								<div class="col-md-6">
									<input ng:valtexto="nombre" ng-model="form.nombre" type="text" name="nombre" placeholder="Nombre" class="form-control" required>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-3 control-label" for="textinput">Correo&nbsp;electronico: </label>
								<div class="col-md-6">
									<input ng-model="form.correo" type="text" name="correo" placeholder="Correo&nbsp;electronico" class="form-control" required>
								</div>
							</div>
							<span ng-show="accion == acciones[0]">
								<div class="form-group">
									<label class="col-md-3 control-label" for="textinput">Contraseña:</label>
									<div class="col-md-6">
										<input ng-model="form.password" type="password" name="password" placeholder="Contraseña" class="form-control" ng-required="accion == acciones[0]">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-3 control-label" for="textinput">Contraseña:</label>
									<div class="col-md-6">
										<input type="password" name="password2" placeholder="Repita la contraseña" class="form-control" ng-required="accion == acciones[0]">
									</div>
								</div>
							</span>
							<div class="form-group">
								<label class="col-md-3 control-label" for="textinput">Privilegio:</label>
								<div class="col-md-2">
									<span ng-init='privilegios=<?=$roles?>'></span>
									<select ng-model="form.id_rol" ng-options='s.id_rol as s.des_rol for s in privilegios' class="form-control" required> </select>
								</div>
								<label class="col-md-2 control-label" >Activo:</label>
								<div class="col-md-2">
									<input class="form-control" type="checkbox" ng-model="form.activo"></label>
								</div>
							</div>
							<div class="col-md-offset-2 col-md-10">
							</div>
						</div>
					</div>
					<div class="panel-footer clearfix" >
						<div class="pull-right">
							<span ng-show="accion == acciones[0]">
								<button type="reset" ng-click="form={};accion = acciones[0]" class="btn btn-default">Limpiar</button>
								<button ng-click="guardar(form)" class="btn btn-primary">Guardar</button>
							</span>
							<span ng-show="accion == acciones[1]">
								<button type="reset" ng-click="form={};accion = acciones[0]" class="btn btn-primary">Nuevo</button>
								<button ng-click="actualizar(form)" class="btn btn-primary">Actualizar</button>
							</span>
						</div>
					</div>
				</div>
			</div>
		</section>
         <!-- END Page content-->
    </section>
    <!-- END Main section-->

<?php $this->load->view('common/admin_footer');?>
