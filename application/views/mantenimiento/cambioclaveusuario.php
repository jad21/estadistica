<?php $this->load->view('common/admin_header');?>
<!-- START Main section-->
<section ng-app="#app.mantenimiento">
	<!-- START Page content-->
	<section class="main-content" ng-controller="UsuarioCtrl">
		<span ng-init='prefixDomain="<?=site_url('/')?>"'></span>
			<!-- <button type="button" class="btn btn-labeled btn-primary pull-right">
			<span class="btn-label"><i class="fa fa-plus-circle"></i>
			</span>Add Item</button> -->
			<h3>Mantenimiento de Usuarios
				<!-- <br> -->
				<!-- <small>Procesar Usuario</small> -->
			</h3>
			<span  class="form-horizontal" role="form" >
				<div class="panel panel-default ">
					<div class="panel-heading">Cambiar clave de Usuario</div>
					<div class="panel-body">
						<div >
							<div ng-repeat="m in mensajes" class="alert alert-{{m.type||'success'}} alert-dismissible" role="alert">
								<button type="button" class="close" data-dismiss="alert" ng-click="mensajes.splice($index,1);"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
								<span ng-bind="m.text"></span>
							</div>
							<div class="form-group">
								<label class="col-md-3 control-label" for="textinput">Nombre&nbsp;de&nbsp;usuario: </label>
								<div class="col-md-6">
									<!-- <input ng-model="form.username" type="text" name="username" placeholder="usuario" class="form-control"  required> -->
									<div class='input-group'>
										<input ng-model="form.username" type="text" name="username" placeholder="usuario" class="form-control" ng-disabled="accion == acciones[1]" required>
										<span class="input-group-btn ">
											<a ng-click="buscar(form)" data-ng-disabled="!(form.username && form.username.length>3)" class="btn btn-default"  type="submit"><i class=" glyphicon glyphicon-search "></i></a>
										</span>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-3 control-label" for="textinput">Contraseña nueva:</label>
								<div class="col-md-6">
									<input ng-model="form.password" type="password" name="password" placeholder="Contraseña" class="form-control" ng-required="true" ng-disabled="!seEncontro">
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-3 control-label" for="textinput">Repita contraseña nueva:</label>
								<div class="col-md-6">
									<input ng-model="password2" type="password" name="password2" placeholder="Repita la contraseña" class="form-control" ng-required="true" ng-disabled="!seEncontro">
								</div>
							</div>
							
							<div class="col-md-offset-2 col-md-10">
							</div>
						</div>
					</div>
					<div class="panel-footer clearfix" >
						<div class="pull-right">
						<button type="reset" ng-click="cancelarCambioUsuario()" class="btn btn-default">Cancelar</button>
							<button ng-click="cambiarClave(form)" class="btn btn-primary" ng-disabled="!seEncontro">Cambiar</button>
						</div>
					</div>
				</div>
			</span>
		</section>
         <!-- END Page content-->
    </section>
    <!-- END Main section-->

<?php $this->load->view('common/admin_footer');?>
