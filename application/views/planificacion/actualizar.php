<?php $this->load->view('common/admin_header');?>
<!-- START Main section-->
<section ng-app="#app.planificacion">
	<!-- START Page content-->
	<section class="main-content" ng-controller="PlanificacionCtrl">
		<span ng-init='form=<?=$planificacion?>'></span>
		<span ng-init='form.cant_fisica_mascara=form.cant_fisica'></span>
		<span ng-init='prefixDomain="<?=base_url()?>"'></span>
			<h3>Módulo de planificación
			</h3>
			<form  class="form-horizontal" role="form" >
				<div class="panel panel-default ">
					<div class="panel-heading">Procesar las planificaciones</div>
					<div class="panel-body">
						<div  class="col-md-8">
							<div ng-repeat="m in mensajes" class="alert alert-{{m.type||'success'}} alert-dismissible" role="alert">
								<button type="button" class="close" data-dismiss="alert" ng-click="mensajes.splice($index,1);"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
								<span ng-bind="m.text"></span>
							</div>
							<div class="form-group">
								<label class="col-md-3 control-label" for="textinput">Nombre del proyecto: </label>
								<div class="col-md-8">
										<input ng-model="form.nombre" type="text" name="nombre" placeholder="Nombre&nbsp;del&nbsp;proyecto" class="form-control" required>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-3 control-label" for="textinput">Fecha&nbsp;Inicio: </label>
								<div class="col-md-3">
									<input  ng-model="form.fecha_inicio" type="text" name="nombre" placeholder="   Fecha&nbsp;Inicio" class="form-control datepicker" required>
								</div>
								<label class="col-md-2 control-label" for="textinput">Fecha&nbsp;Fin: </label>
								<div class="col-md-3">
									<input  ng-model="form.fecha_fin" type="text" name="nombre" placeholder="  Fecha&nbsp;Fin" class="form-control datepicker" required>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-3 control-label" for="textinput">Total(TM): </label>
								<div class="col-md-3">
									
									<input  ng-model="form.cant_fisica_mascara" type="text" name="total" placeholder="0.00" class="form-control toneladas" required>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-3 control-label" >Objectivo:</label>
								<div class="col-md-8">
									<textarea class="form-control" ng-model="form.meta"></textarea>
								</div>
							</div>
							<div class="col-md-offset-2 col-md-10">
							</div>
						</div>
						<div class="col-md-4">
							<!-- START panel-->
	                        <div class="panel panel-default">
	                           <div class="panel-heading" align="center">

	                           	<button type="button" class="btn btn-labeled btn-primary " ng-click="openModalAgregar()" 
	                           	ng-disabled="(!form.cant_fisica_mascara || !form.nombre || !form.fecha_inicio || !form.fecha_fin)">
	                           		<span class="btn-label"><i class="fa fa-plus-circle"></i> </span>Agregar accion
	                           	</button>
	                              <!-- <a href="#" data-perform="panel-collapse" data-toggle="tooltip" title="Collapse Panel" class="pull-right">
	                                 <em class="fa fa-minus"></em>
	                              </a> -->
	                           </div>
	                           <!-- START table-responsive-->
	                           <div ng-hide="!form.acciones || form.acciones.length==0" class="table-responsive">
	                              <table class="table table-striped table-bordered table-hover">
	                                 <thead>
	                                    <tr>
	                                       <th>#</th>
	                                       <th>Acciones</th>
	                                       <th>Boton</th>
	                                    </tr>
	                                 </thead>
	                                 <tbody>
	                                    <tr ng-repeat="accion in form.acciones">
	                                       <td><p ng-bind="$index + 1"></p></td>
	                                       <td><p ng-bind="cortarAccion(accion.des_accion)"></p></td>
	                                       <td class="text-center">
	                                          <div class="btn-group"><a href="#" data-toggle="dropdown" class="dropdown-toggle"><i class="fa fa-cog"></i></a>
	                                             <ul class="dropdown-menu pull-right text-left">
	                                                <li><a href="#" ng-click="eliminarAccion($index)">Eliminar</a> </li>
	                                                <li><a href="#" ng-click="openModalAgregar($index)">Modificar</a> </li>
	                                             </ul>
	                                          </div>
	                                       </td>
	                                    </tr>
	                                 </tbody>
	                              </table>
	                           </div>
	                        </div>
                        	<!-- END panel-->
						</div>
					</div>
					<div class="panel-footer clearfix" >
						<span class="text-left">
							<a href="<?php echo site_url('planificacion/ver');?>">
								<small>Ver todas</small>
							</a>
						</span>
						<div class="pull-right">
							<span >
								<button onclick="window.history.back()" class="btn btn-default">Cancelar</button>
								<button ng-click="actualizar(form)" class="btn btn-primary">Actualizar</button>
							</span>
						</div>
					</div>
				</div>
			</form>
		</section>
         <!-- END Page content-->
    </section>
    <!-- END Main section-->



<?php $this->load->view('common/admin_footer');?>
