<!-- modal de accion -->
<div class="modal-header">
	<h3 class="modal-title">N# {{item}} Accion <span class="pull-right">Total:{{total}}</span></h3>
</div>
<div class="modal-body">
	<form class="form-horizontal">
		<div class="form-group">
			<label for="inputEmail3" class="col-sm-2 control-label">Nombre</label>
			<div class="col-sm-10">
				<input ng-model="formModal.des_accion" type="text" name="des_accion" placeholder="Nombre" class="form-control" required>
			</div>
		</div>
		<div class="form-group">
			<label for="inputEmail3" class="col-sm-2 control-label">Meta fisica</label>
			<div class="col-sm-10">
				<span ng-bind="formModal.meta_fisica" type="text" ng-keypress="limpiarMeses()" name="meta_fisica" placeholder="0.00" class="form-control toneladas" required>
			</div>
		</div>
		<span class="">
			<legend class="scheduler-border">Programación mensual</legend>
			<div class="table-responsive">
				<table class="table table-striped table-bordered table-hover">
					<thead>
						<tr>
							<th>Ene</th>
							<th>Feb</th>
							<th>Mar</th>
							<th>Abr</th>
							<th>May</th>
							<th>Jun</th>
						</tr>
					</thead>
					<tbody>
						<tr> 
							<td> <input ng-model="formModal.meses['Ene']" ng-keypress="inspeccionarCantidad()" ng-blur="inspeccionarCantidad()" name="Ene" type="text"  placeholder="0.0" class="form-control toneladas" > </td>
							<td> <input ng-model="formModal.meses['Feb']" ng-keypress="inspeccionarCantidad()" ng-blur="inspeccionarCantidad()" name="Feb" type="text"  placeholder="0.0" class="form-control toneladas" > </td>
							<td> <input ng-model="formModal.meses['Mar']" ng-keypress="inspeccionarCantidad()" ng-blur="inspeccionarCantidad()" name="Mar" type="text"  placeholder="0.0" class="form-control toneladas" > </td>
							<td> <input ng-model="formModal.meses['Abr']" ng-keypress="inspeccionarCantidad()" ng-blur="inspeccionarCantidad()" name="Abr" type="text"  placeholder="0.0" class="form-control toneladas" > </td>
							<td> <input ng-model="formModal.meses['May']" ng-keypress="inspeccionarCantidad()" ng-blur="inspeccionarCantidad()" name="May" type="text"  placeholder="0.0" class="form-control toneladas" > </td>
							<td> <input ng-model="formModal.meses['Jun']" ng-keypress="inspeccionarCantidad()" ng-blur="inspeccionarCantidad()" name="Jun" type="text"  placeholder="0.0" class="form-control toneladas" > </td>
						</tr>
					</tbody>
				</table>
				<table class="table table-striped table-bordered table-hover">
					<thead>
						<tr>
							<th>Jul</th>
							<th>Ago</th>
							<th>Sep</th>
							<th>Oct</th>
							<th>Nov</th>
							<th>Dic</th>
						</tr>
					</thead>
					<tbody>
						<tr> 
							<td> <input ng-model="formModal.meses['Jul']" ng-keypress="inspeccionarCantidad()" ng-blur="inspeccionarCantidad()" name="Jul" type="text"  placeholder="0.0" class="form-control toneladas" > </td>
							<td> <input ng-model="formModal.meses['Ago']" ng-keypress="inspeccionarCantidad()" ng-blur="inspeccionarCantidad()" name="Ago" type="text"  placeholder="0.0" class="form-control toneladas" > </td>
							<td> <input ng-model="formModal.meses['Sep']" ng-keypress="inspeccionarCantidad()" ng-blur="inspeccionarCantidad()" name="Sep" type="text"  placeholder="0.0" class="form-control toneladas" > </td>
							<td> <input ng-model="formModal.meses['Oct']" ng-keypress="inspeccionarCantidad()" ng-blur="inspeccionarCantidad()" name="Oct" type="text"  placeholder="0.0" class="form-control toneladas" > </td>
							<td> <input ng-model="formModal.meses['Nov']" ng-keypress="inspeccionarCantidad()" ng-blur="inspeccionarCantidad()" name="Nov" type="text"  placeholder="0.0" class="form-control toneladas" > </td>
							<td> <input ng-model="formModal.meses['Dic']" ng-keypress="inspeccionarCantidad()" ng-blur="inspeccionarCantidad()" name="Dic" type="text"  placeholder="0.0" class="form-control toneladas" > </td>
						</tr>
					</tbody>
				</table>
			</div>
		</span>
	</form>
</div>
<div class="modal-footer">
	<button class="btn btn-warning" type="button" ng-click="cancel()">Cerrar</button>
	<button class="btn btn-primary" ng-disabled="total<0 || !formModal.meta_fisica || !formModal.des_accion" type="button" ng-click="guardar()" >Guardar</button>
</div>
<!-- modal de accion -->