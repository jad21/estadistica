<?php $this->load->view('common/admin_header');?>
<!-- START Main section-->

<section ng-app="#app.planificacion">
	<!-- START Page content-->
	<section class="main-content" ng-controller="PlanificacionCtrl">
	
			<h3>Lista de planificaciones </h3>

			<div  class="form-horizontal" role="form" >
				<div class="panel panel-default ">
					<div class="panel-heading">Procesar las planificaciones</div>
					<div class="panel-body">
						<div ng-repeat="m in mensajes" class="alert alert-{{m.type||'success'}} alert-dismissible" role="alert">
							<button type="button" class="close" data-dismiss="alert" ng-click="mensajes.splice($index,1);"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
							<span ng-bind="m.text"></span>
						</div>
						<table id="tabla_datatable" class="table table-striped table-bordered table-hover">
							<thead>
								<tr>
									<td>Nombre Proyecto</td>
									<td>Total</td>
									<td>Fecha de inicio</td>
									<td>Fecha de fin</td>
									<td>Fecha de registro</td>
									<td>Objetivo</td>
									<td>Usuario</td>
									<td>Cantidad de acciones</td>
									<td>Botones</td>
								</tr>
							</thead>
							<tbody>
								<?php foreach ($lista as $l): ?>
									<tr>
										<td><?=$l["nombre_proyecto"]?></td>
										<td><?=$l["total"]?></td>
										<td><?=$l["fecha_inicio"]?></td>
										<td><?=$l["fecha_fin"]?></td>
										<td><?=$l["fecha_registro"]?></td>
										<td><?=$l["meta"]?></td>
										<td><?=$l["nombre"]?></td>
										<td><?=$l["acciones"]?></td>
										<td>
											<p>
												<a href="javascript:;" ng-click="eliminar(<?=$l['id_plan']?>)">eliminar</a>
											</p>
											<p>
												<a href="<?=site_url('planificacion/actualizar/'.$l['id_plan'])?>">modificar</a>
											</p> 

										</td>
									</tr>
								<?php endforeach ?>
							</tbody>
						</table>
					</div>
					<div class="panel-footer clearfix" >
						<!-- <div class="pull-right">
							<span >
								<button type="reset" ng-click="form={}" class="btn btn-default">Limpiar</button>
								<button ng-click="guardar(form)" class="btn btn-primary">Guardar</button>
							</span>
						</div> -->
					</div>
				</div>
			</div>
		</section>
		<!-- END Page content-->
	</section>
	<!-- END Main section-->



<?php $this->load->view('common/admin_footer');?>
