
<div class="panel panel-default">
	<div class="panel-heading"> 
		Distribución total otras redes publicas y privadas
		<a href="#" data-perform="panel-collapse" data-toggle="tooltip" title="Collapse Panel" class="pull-right">
			<em class="fa fa-minus"></em>
		</a>
	</div>
	<div class="panel-body" style="background-color: white;">
		<table class="table table-bordered"> 
			<thead> 
				<tr> 
					<th>Rubro</th>
					<th ng-repeat="m in meses"  ng-bind="m.corto"></th>
				</tr> 
			</thead> 
			<tbody> 
				<tr ng-repeat="r in rubros"> 
					<th ng-bind="r.des_rubro"></th>
					<td ng-repeat="m in meses"  ng-bind="lista[m.id_mes][r.id_rubro].total"></td>
				</tr>
				<tr>
					<th>Total</th>
					<td ng-repeat="m in meses"  ng-bind="sumaTotalCol(lista[m.id_mes])"></td>
				</tr>
			</tbody> 
		</table>
		<br>
		
		<div id="lineWrap" class="container" style="padding: 50px;width: 800px;">
			<!-- <canvas id="line" class="chart chart-line" chart-data="grafico.data" chart-labels="grafico.labels" 
				chart-legend="true" chart-series="grafico.series" chart-click="onClick">
			</canvas>  -->
			<canvas id="bar" class="chart chart-bar" chart-data="bar.data" chart-labels="bar.labels" chart-series="bar.series">
			</canvas>
			<br>
			<canvas id="pie" class="chart chart-pie" chart-data="pie.data" chart-labels="pie.labels" chart-legend="true">
			</canvas>
			</div>
	</div>
	<div class="panel-footer text-right">
		<a class="btn btn-success" ng-click="exportar()">Exportar</a>
	</div>
</div>

