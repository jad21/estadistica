
<div class="panel panel-default">
	<div class="panel-heading"> 
		Distribución de Alimentos a la Red Mercal (Estado y Mes)
		<a href="#" data-perform="panel-collapse" data-toggle="tooltip" title="Collapse Panel" class="pull-right">
			<em class="fa fa-minus"></em>
		</a>
	</div>
	<div class="panel-body" style="background-color: white;">
		<table class="table table-bordered"> 
			<thead> 
				<tr> 
					<th>Estado</th>
					<th ng-repeat="m in meses"  ng-bind="m.corto"></th>
				</tr> 
			</thead> 
			<tbody> 
				<tr ng-repeat="e in estados"> 
					<th ng-bind="e.des_estado"></th>
					<td ng-repeat="m in meses"  ng-bind="lista[m.id_mes][e.id_estado].total"></td>
				</tr>
				<tr>
					<th>Total</th>
					<td ng-repeat="m in meses"  ng-bind="sumaTotalCol(lista[m.id_mes])"></td>
				</tr>
			</tbody> 
		</table>
		
	</div>
	<div class="panel-footer text-right">
		<a class="btn btn-success" ng-click="exportar()">Exportar</a>
	</div>
</div>

