
<div class="panel panel-default">
	<div class="panel-heading"> 
		Modulo de Reportes
		<a href="#" data-perform="panel-collapse" data-toggle="tooltip" title="Collapse Panel" class="pull-right">
			<em class="fa fa-minus"></em>
		</a>
	</div>
	<div class="panel-body" >
		<div class="form-horizontal">
			
			<div class="col-md-offset-2  col-md-8">
				<div class="col-md-2">
					<label class="control-label" for="textinput">Reportes: </label>
				</div>
				<div class="col-md-4">
					<select ng-model="from.id_reporte" ng-options='s.id_reporte as s.des_reporte for s in reportes' class="form-control" required> </select>
				</div>
				<div class="col-md-2">
					<label  class="control-label" for="textinput">Año: </label>
				</div>
				<div class="col-md-4">
					<input ng-model="from.ano" placeholder="Escribo un Año" type="text" class="form-control" required>
				</div>
			</div>
			
		</div>
	</div>
	<div class="panel-footer text-right">
		<a class="btn btn-success" ng-click="Ir(from)">Procesar</a>
	</div>
</div>

