
<div class="panel panel-default">
	<div class="panel-heading"> 
		Materia Prima: Redes Públicas y Privadas
		<a href="#" data-perform="panel-collapse" data-toggle="tooltip" title="Collapse Panel" class="pull-right">
			<em class="fa fa-minus"></em>
		</a>
	</div>
	<div class="panel-body" style="background-color: white;">
		<table class="table table-bordered"> 
			<thead> 
				<tr> 
					<th>Rubro</th>
					<th ng-repeat="m in meses"  ng-bind="m.corto"></th>
				</tr> 
			</thead> 
			<tbody> 
				<tr ng-repeat="r in rubros"> 
					<th ng-bind="r.des_rubro"></th>
					<td ng-repeat="m in meses"  ng-bind="lista[m.id_mes][r.id_rubro].total"></td>
				</tr>
				<tr>
					<th>Total</th>
					<td ng-repeat="m in meses"  ng-bind="sumaTotalCol(lista[m.id_mes])"></td>
				</tr>
			</tbody> 
		</table>
		<br>
		
		<div id="lineWrap" class="container" style="padding: 50px;width: 800px;height: 500px">
			<canvas id="line" class="chart chart-line" chart-data="grafico.data" chart-labels="grafico.labels" 
				chart-legend="true" chart-series="grafico.series" chart-click="onClick">
			</canvas> 
		</div>
		<br>
		<!-- <table class="table table-bordered">
			<thead> 
  				<tr>
	  				<th></th>
		  			<th ng-repeat="m in meses"  ng-bind="m.corto"></th>
  				</tr>
			</thead> 
			<tbody> 
				<tr>
					<th ng-bind="grafico.series[0]"></th>
					<td ng-repeat="v in grafico.data[0] track by $index" ng-bind="v"></td>
				</tr>
				<tr>
					<th ng-bind="grafico.series[1]"></th>
					<td ng-repeat="v in grafico.data[1] track by $index" ng-bind="v"></td>
				</tr>
			</tbody>
		</table> -->
		
	</div>
	<div class="panel-footer text-right">
		<a class="btn btn-success" ng-click="exportar()">Exportar</a>
	</div>
</div>

