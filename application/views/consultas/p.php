<?php $this->load->view('common/admin_header');?>

      <!-- START Main section-->
      <!-- // <script src="http://html2canvas.hertzen.com/build/html2canvas.js"></script> -->
      <!-- // <script src="http://www.nihilogic.dk/labs/canvas2image/base64.js"></script> -->
      <!-- // <script src="http://www.nihilogic.dk/labs/canvas2image/canvas2image.js"></script> -->
      <section ng-app="#app.consultas">
         <!-- START Page content-->
         <section class="main-content" ng-controller="LineCtrl">
            <!-- <h3>Bienvenidos al sistema Estadístico LA CASA </h3> -->
            <div class="row">
               <!-- START dashboard main content-->
               <div class="col-md-12">
                  <!-- START table-->
                  <div class="row">
                     <div class="col-lg-12">
                        <!-- START panel-->
                        <div class="panel panel-default">
                           <div class="panel-heading">Grafico 
                              <a href="javascript:open()">abrir</a>
                              <a href="javascript:;" ng-click="abrir()">ng::open</a>

                              <a href="#" data-perform="panel-collapse" data-toggle="tooltip" title="Collapse Panel" class="pull-right">
                                 <em class="fa fa-minus"></em>
                              </a>
                           </div>
                           <div class="panel-body" style="background-color: white;">
                              
                              <!-- START table-responsive-->
                              <div id="tabla" class="container" style="width: 800px;">
                                 <table class="table table-condensed">
                                    <thead> <tr> <th>#</th> <th>First Name</th> <th>Last Name</th> <th>Username</th> </tr> </thead> <tbody> <tr> <th scope="row">1</th> <td>Mark</td> <td>Otto</td> <td>@mdo</td> </tr> <tr> <th scope="row">2</th> <td>Jacob</td> <td>Thornton</td> <td>@fat</td> </tr> <tr> <th scope="row">3</th> <td colspan="2">Larry the Bird</td> <td>@twitter</td> </tr> </tbody> 
                                 </table>
                              </div>
                              <!-- END table-responsive-->
                              <div id="lineWrap" class="container" style="padding: 50px;width: 800px;height: 500px">
                                 <canvas id="line" class="chart chart-line" chart-data="data" chart-labels="labels" 
                                 chart-legend="true" chart-series="series" chart-click="onClick"></canvas> 
                              </div>
                              <div class="panel-footer text-right">
                                 <!-- <a href="#">
                                    <small>View all</small>
                                 </a> -->
                              </div>
                           </div>
                        </div>
                        
                        <!-- END panel-->
                     </div>
                  </div>
                  <!-- END table-->
               </div>
               <!-- END dashboard main content-->
            </div>
         </section>
         <!-- END Page content-->
      </section>
      <!-- END Main section-->


<!-- mis imagenes -->
<img id="img_mercal" src="<?=base_url()?>public/admin/app/img/mercal_small.png">
<!-- fin -->
   <?php $this->load->view('common/admin_footer');?>