<?php $this->load->view('common/admin_header');?>

      <!-- START Main section-->
      <!-- // <script src="http://html2canvas.hertzen.com/build/html2canvas.js"></script> -->
      <!-- // <script src="http://www.nihilogic.dk/labs/canvas2image/base64.js"></script> -->
      <!-- // <script src="http://www.nihilogic.dk/labs/canvas2image/canvas2image.js"></script> -->
      <section ng-app="#app.consultas">
         <!-- START Page content-->
         <section class="main-content" ng-controller="ResportesCtrl">
            <!-- <h3>Bienvenidos al sistema Estadístico LA CASA </h3> -->
            <div class="row">
               <!-- START dashboard main content-->
               <div class="col-md-12">
                  <!-- START table-->
                  <div class="row">
                     <div class="col-lg-12">
                        <!-- START panel-->
                        <ng-view></ng-view>
                        <!-- END panel-->
                     </div>
                  </div>
                  <!-- END table-->
               </div>
               <!-- END dashboard main content-->
            </div>
         </section>
         <!-- END Page content-->
      </section>
      <!-- END Main section-->

<?php $this->load->view('common/admin_footer');?>

