<?php

class Distribucion_model extends Base_Model{

    protected function _init() {
      $this->_table = 'distribucion';
    }

    public function save(Array $data,$id_user) {
      try {
        $this->db->trans_begin();

        $form = $data["form"];
        if(!isset($form["id_dist"])){
          $res = $this->selectOne(["ano"=>$form["ano"],"id_mes"=>$form["mes"],"radio"=>$form["radio"],"id_tipo_red"=>1]);
          if (count($res)>0) {
            return ["code"=>0];
          }
        }
        $estadosRubros = $data["estadosRubros"];
        $distribucion = json_decode("{}");
        // $distribucion->{"id_plan"} = 
        $distribucion->{"id_tipo_red"} = 1;
        $distribucion->{"ano"} = $form["ano"];
        $distribucion->{"radio"} = $form["radio"];
        $distribucion->{"id_mes"} = $form["mes"];
        if ($form["radio"] == "consolidado") {
          $distribucion->{"id_documento"} = $form["id_documento"];
        }else{
          $distribucion->{"id_documento"} = 0;
        }
        
        $distribucion->{"id_usuario"} = $id_user;
        if(isset($form["id_dist"])){
          $id_dist = $form["id_dist"];
          $result['res_delete'] = $this->eliminarEstadosRubros($form["id_dist"]);
          $this->db->where("id_dist",$form["id_dist"]);
          $result['res_update'] = $this->db->update($this->_table, $distribucion);
        }else{
          $insert = $this->db->insert("distribucion",$distribucion);
          $id_dist = $this->getLastIdDist();
          $result["insert"] = $insert;
        }
        
        $result["distribucion"] = $distribucion;
        $result["estados"] = [];
        $result["dist_rubro"] = [];
        foreach ($estadosRubros as $id_estado => $rubros) {
          $rubro_estado = json_decode("{}");
          $rubro_estado->{"id_dist"} = $id_dist;
          $rubro_estado->{"id_estado"} = $id_estado;
          
          // $result["estados"][] = $rubro_estado;
          // $result["est"][] = $est;

          $this->db->insert("dist_rubro_estado", $rubro_estado);
          $id_dist_rubro_estado = $this->getLastIdDistRubroEstado();

          foreach($rubros as $id_rubro => $cantidad) {
            $dist_rubro = json_decode("{}");
            $dist_rubro->{"id_dist_rubro_estado"} = $id_dist_rubro_estado;
            $dist_rubro->{"id_rubro"} = $id_rubro;
            if (isset($cantidad)) {
              $cantidad = str_replace(".","",$cantidad);
              $cantidad = str_replace(",",".",$cantidad);
              $dist_rubro->{"cantidad"} = $cantidad;
            }else{
              $dist_rubro->{"cantidad"} = 0;
            }
            // $result["dist_rubro"][] = $dist_rubro;
            $this->db->insert("dist_rubro", $dist_rubro);
          }

        }
        $result['code'] = 1;
        $this->db->trans_commit();
        // $this->db->trans_rollback();
      } catch (Exception $e) {
        $this->db->trans_rollback();
        $result['code'] = -1;
        $result["error"] = "ocurrio un error";
      }
      $response = $result;
      
      return $response;
    }
    
    /*
     * Para Guardar Una ditribución de materia prima 
     * Parametros $data y id_usuario
     */
    public function guardar_prima(Array $data,$id_user) {
      try {
        $this->db->trans_begin();

        if(!isset($data["id_dist"])){
          $res = $this->selectOne(["ano"=>$data["ano"],"id_mes"=>$data["mes"],"id_tipo_red"=>$data["id_tipo_red"]]);
          if (count($res)>0){
            return ["code"=>0];
          }
        }
        $distribucion = json_decode("{}");
        // $distribucion->{"id_plan"} = 
        $distribucion->{"id_tipo_red"} = $data["id_tipo_red"];
        $distribucion->{"ano"} = $data["ano"];
        $distribucion->{"radio"} = "";
        $distribucion->{"id_mes"} = $data["mes"];
        
        $distribucion->{"id_documento"} = 0;
        
        $distribucion->{"id_usuario"} = $id_user;
        if(isset($data["id_dist"])){
          $id_dist = $data["id_dist"];
          $result['res_delete'] = $this->eliminarOtrasRedesRubros($data["id_dist"]);
          $this->db->where("id_dist",$data["id_dist"]);
          $result['res_update'] = $this->db->update($this->_table, $distribucion);
        }else{
          $insert = $this->db->insert("distribucion",$distribucion);
          $id_dist = $this->getLastIdDist();
          $result["insert"] = $insert;
        }
        
        $result["dist_otras_redes"] = [];
        foreach ($data["rubros"] as $id_rubro => $cantidad) {
          $data_prima = json_decode("{}");
          $data_prima->{"id_dist"}  = $id_dist;
          $data_prima->{"id_rubro"} = $id_rubro;
          
          if (isset($cantidad)) {
            $cantidad = str_replace(".","",$cantidad);
            $cantidad = str_replace(",",".",$cantidad);
          }else{
            $cantidad = 0;
          }
          
          $data_prima->{"cantidad"} = $cantidad;
          $result["dist_otras_redes"][] = $this->db->insert("dist_otras_redes",$data_prima);
        }
       
        $result['code'] = 1;
        $this->db->trans_commit();
        // $this->db->trans_rollback();
      } catch (Exception $e) {
        $this->db->trans_rollback();
        $result['code'] = -1;
        $result["error"] = "ocurrio un error";
      }
      $response = $result;
      
      return $response;
    }
    
    /*
     * Parametros $data y id_usuario
     */
    public function guardar_ventas(Array $data,$id_user) {
      try {
        $this->db->trans_begin();

        if(!isset($data["id_venta"])){
          $sql = "SELECT * FROM ventas_otras_redes_pp WHERE ano = '".$data['ano']."' AND id_mes = {$data['mes']};";
          $res = $this->query($sql);
          $result['ventas_otras_redes_pp'] = $res;
          if (count($res)>0){
            return ["code"=>0];
          }
        }

        $ventas = json_decode("{}");
        $ventas->{"ano"} = $data["ano"];
        $ventas->{"id_mes"} = $data["mes"];
        $ventas->{"id_usuario"} = $id_user;
        if(isset($data["id_venta"])){
          $id_venta = $data["id_venta"];
          $result['res_delete'] = $this->db->delete("dist_redes",["id_venta" => $id_venta]);
          $this->db->where("id_venta",$data["id_venta"]);
          $result['res_update'] = $this->db->update("ventas_otras_redes_pp", $ventas);
        }else{
          $insert = $this->db->insert("ventas_otras_redes_pp",$ventas);
          $id_venta = $this->getLastIdVenta();
          $result["insert"] = $insert;
        }
        
        $result["dist_redes"] = [];
        foreach ($data["redes"] as $id_tipo_red => $cantidad) {
          $data_red = json_decode("{}");
          $data_red->{"id_venta"}  = $id_venta;
          $data_red->{"id_tipo_red"} = $id_tipo_red;
          
          if (isset($cantidad)) {
            $cantidad = str_replace(".","",$cantidad);
            $cantidad = str_replace(",",".",$cantidad);
          }else{
            $cantidad = 0;
          }

          $data_red->{"cantidad"} = $cantidad;
          $result["dist_redes"][] = $this->db->insert("dist_redes",$data_red);
        }
       
        $result['code'] = 1;
        $this->db->trans_commit();
        // $this->db->trans_rollback();
      } catch (\Exception $e) {
        $this->db->trans_rollback();
        $result['code'] = -1;
        $result["error"] = "ocurrio un error";
      }
      $response = $result;
      
      return $response;
    }


    public function eliminarEstadosRubros($id_dist){
      $data = $this->query("SELECT * FROM dist_rubro_estado WHERE id_dist = {$id_dist};");
      $res = [];
      foreach ($data as $row) {
        $res[] = $this->db->delete("dist_rubro",["id_dist_rubro_estado" => $row["id"]]);
      }
      $res[] = $this->db->delete("dist_rubro_estado",["id_dist" => $id_dist]);
      return $res;
    }

    public function eliminarOtrasRedesRubros($id_dist){
      $res[] = $this->db->delete("dist_otras_redes",["id_dist" => $id_dist]);
      return $res;
    }

    public function allFullRow($id_dist){
      $dist_rubro_estado = $this->db->query(
        "SELECT id FROM dist_rubro_estado WHERE id_dist = {$id_dist} "
      )->result_array();
      
      if(count($dist_rubro_estado)>0){
        $id_dist_rubro_estado = "(";
        foreach ($dist_rubro_estado as $row) {
          $id_dist_rubro_estado .= $row['id'].",";
        }
        $id_dist_rubro_estado .= "0)";
        return $this->db->query(
          "SELECT d.id_dist,u.username as nombre,d.id_usuario,d.ano,m.des_mes,d.fecha_registro,
            (SELECT count(1) as cantidad FROM dist_rubro_estado WHERE id_dist = {$id_dist}) as cant_estados,
            (SELECT sum(cantidad) FROM dist_rubro WHERE id_dist_rubro_estado in {$id_dist_rubro_estado}) as total_toneladas 
            FROM distribucion d 
            join usuarios u on u.id = d.id_usuario
            join meses m on m.id_mes = d.id_mes
            WHERE d.id_dist = {$id_dist};"
          )->result_array();
      }
      return [];
    }
    public function allFull(){
      $ds = $this->db->query(
        "SELECT id_dist from distribucion"
      )->result_array();
      $distribuciones = [];
      foreach ($ds as $d) {
        $res = $this->allFullRow($d["id_dist"]);
        if(count($res)>0){
          $distribuciones[] = $res[0];
        }
      }
      return $distribuciones;
    }

    function getLastIdDist(){
      return $this->db->query("SELECT max(id_dist) as id FROM distribucion")->row()->id;
    }
    function getLastIdVenta(){
      return $this->db->query("SELECT max(id_venta) as id FROM ventas_otras_redes_pp")->row()->id;
    }
    function getLastIdDistRubroEstado(){
      return $this->db->query("SELECT max(id) as id FROM dist_rubro_estado")->row()->id;
    }

    public function getDistById($id_dist = 12){
      $distribucion =  $this->query("SELECT id_dist,radio,ano,id_mes as mes FROM distribucion WHERE id_dist = {$id_dist};");
      if(count($distribucion)>0){
        $dist["form"] = $distribucion[0];
        $dist["estadosRubros"] = [];
        $estadosRubros =  $this->query("SELECT * FROM dist_rubro_estado WHERE id_dist = {$id_dist};");
        $dist["cant_estados"] = count($estadosRubros);
        foreach($estadosRubros as $row) {
          $id_estados_rubro = $row["id"];
          $rubros = $this->query("SELECT id_rubro,cantidad FROM dist_rubro WHERE id_dist_rubro_estado  = {$id_estados_rubro};");
          $dist["estadosRubros"][$row["id_estado"]] = [];
          foreach ($rubros as $rubro) {
           $dist["estadosRubros"][$row["id_estado"]][$rubro["id_rubro"]] = $rubro["cantidad"];
          }
          // $dist["estadosRubros"][$row["id_estado"]] = [
          //   "id_estado" => $row["id_estado"],
          //   "data" => $data
          // ];
        }
        return $dist;
      }
      return false;
    }
    public function eliminar($id){
      try {
        $this->db->trans_begin();
        $return["delete_eliminarEstadosRubros"] = $this->eliminarEstadosRubros($id);
        $res = $this->db->delete($this->_table,["id_dist" => $id]);
        $return["delete"] = $res;
        $this->db->trans_commit();
      } catch (Exception $e) {
        $this->db->trans_rollback();
        $return["delete"] = FALSE;
      }
      return $return;
    }

    public function eliminarOtrasRedes($id){
      try {
        $this->db->trans_begin();
        $return["delete_eliminarEstadosRubros"] = $this->eliminarOtrasRedesRubros($id);
        $res = $this->db->delete($this->_table,["id_dist" => $id]);
        $return["delete"] = $res;
        $this->db->trans_commit();
      } catch (Exception $e) {
        $this->db->trans_rollback();
        $return["delete"] = FALSE;
      }
      return $return;
    }

    public function eliminarVentaOtrasRedes($id_venta){
      try {
        $this->db->trans_begin();
        $result['res_delete'] = $this->db->delete("dist_redes",["id_venta" => $id_venta]);
        $res = $this->db->delete("ventas_otras_redes_pp",["id_venta" => $id_venta]);
        $return["delete"] = $res;
        $this->db->trans_commit();
      } catch (Exception $e) {
        $this->db->trans_rollback();
        $return["delete"] = FALSE;
      }
      return $return;
    }

    /**
     * listar ditribuciòn materia prima
     */
    public function list_dist_materia_prima(){
      return $this->query(
        "SELECT d.id_dist,d.id_tipo_red,d.id_usuario,u.username as nombre,d.ano,d.id_mes,m.des_mes,d.fecha_registro
          ,(SELECT sum(cantidad) as cantidad FROM dist_otras_redes WHERE id_dist = d.id_dist) as cantidad
          FROM distribucion d
          JOIN meses m on m.id_mes = d.id_mes
          JOIN usuarios u on u.id = d.id_usuario
          WHERE d.id_tipo_red in (4,5);"
        );
    }
    /**
     * listar ditribuciòn productos terminados
     */
    public function list_dist_productos_terminados(){
      return $this->query(
        "SELECT d.id_dist,d.id_tipo_red,d.id_usuario,u.username as nombre,d.ano,d.id_mes,m.des_mes,d.fecha_registro
          ,(SELECT sum(cantidad) as cantidad FROM dist_otras_redes WHERE id_dist = d.id_dist) as cantidad
          FROM distribucion d
          JOIN meses m on m.id_mes = d.id_mes
          JOIN usuarios u on u.id = d.id_usuario
          WHERE d.id_tipo_red in (2,3);"
        );
    }
    /*
     * listar ventas productos terminados publicos y privados
     */
    public function list_ventas_otras_redes(){
      return $this->query(
        "SELECT 
            v.id_venta,ano,username as usuario,des_mes,fecha_registro,
            (SELECT sum(cantidad) as cantidad FROM dist_redes WHERE id_venta = v.id_venta) as cantidad
          FROM ventas_otras_redes_pp v 
          join meses m on m.id_mes = v.id_mes
          JOIN usuarios u on u.id = v.id_usuario"
        );
    }
    /*
     * obtener distribucion otras redes por id
     */
    public function getDistOtrasRedesById($id_dist,$tipo) {
      if($tipo==1){
        $sql = "SELECT d.id_dist,d.id_usuario,d.ano ,d.id_mes as mes,id_tipo_red FROM distribucion d WHERE id_tipo_red in (4,5) AND id_dist = {$id_dist}";
      }else{
        $sql = "SELECT d.id_dist,d.id_usuario,d.ano ,d.id_mes as mes,id_tipo_red FROM distribucion d WHERE id_tipo_red in (2,3) AND id_dist = {$id_dist}";
      }
      $dist = $this->query($sql);
      if(count($dist)>0){
        $dist = $dist[0];
        $dist["id_tipo_red"] = intval($dist["id_tipo_red"]);
        $dist_otras_redes = $this->query("SELECT * FROM dist_otras_redes WHERE id_dist = {$id_dist}");
        $dist["rubros"] = [];
        foreach ($dist_otras_redes as $row){
          $dist["rubros"][$row["id_rubro"]] = $row["cantidad"];
        }
      }
      return $dist;
    }
     /*
     * obtener distribucion de ventas otras redes publicas y privadas
     */
    public function getVentaOtrasRedesById($id_venta) {
      $sql = "SELECT *,id_mes as mes FROM ventas_otras_redes_pp WHERE id_venta = {$id_venta}";
      
      $dist = $this->query($sql);
      if(count($dist)>0){
        $dist = $dist[0];

        $dist_otras_redes = $this->query("SELECT id_tipo_red, cantidad FROM dist_redes WHERE id_venta = {$id_venta}");
        $dist["redes"] = [];
        foreach ($dist_otras_redes as $row){
          $dist["redes"][$row["id_tipo_red"]] = $row["cantidad"];
        }
      }
      return $dist;
    }
    /**
     *   des: Retorta todas las distribuciones tipo mercal de un año divididas por mes y productos
     *  @author Jose Angel Delgado
     *  @param $ano
     *  @return mixed
     */
    public function list_dist_mercal_estado_y_producto($ano){
      $this->loadModel('catalogo');
      $result = [];
      $catalogo = new Catalogo();
      $meses = $catalogo->getMeses();
      foreach ($meses as $mes) {
        $id_mes = $mes["id_mes"];
        $rubro_mes = $this->query(
          "SELECT d.id_mes, r.id_rubro,p.des_rubro,sum(r.cantidad) total 
            FROM distribucion d
            JOIN dist_rubro_estado e ON e.id_dist = d.id_dist
            JOIN dist_rubro r        ON r.id_dist_rubro_estado = e.id
            JOIN rubros p            ON p.id_rubro =  r.id_rubro
            WHERE d.id_tipo_red = 1  AND ano = '{$ano}' AND d.id_mes = $id_mes
            GROUP BY r.id_rubro,p.des_rubro,d.id_mes
            ORDER BY d.id_mes asc,r.id_rubro asc"
          );
        $result_rubro_mes = [];
        foreach ($rubro_mes as $r) {
          $result_rubro_mes[$r["id_rubro"]] = $r;
        }
         $result[$id_mes] = $result_rubro_mes;
      }
      return $result;
    }
    public function list_dist_mercal_estado_y_mes($ano)
    {
      $estado_mes = $this->query(
        "SELECT d.id_mes, e.id_estado,edo.des_estado,sum(r.cantidad) total 
          FROM distribucion d
          JOIN dist_rubro_estado e ON e.id_dist = d.id_dist
          JOIN dist_rubro r        ON r.id_dist_rubro_estado = e.id
          JOIN estados edo         ON edo.id_estado =  e.id_estado
          WHERE d.id_tipo_red = 1  AND ano = '{$ano}' 
          GROUP BY e.id_estado,d.id_mes,edo.des_estado
          ORDER BY d.id_mes asc,e.id_estado asc"
        );
      $estado_mes_result = [];
      array_map(function($item) use (&$estado_mes_result){
        $estado_mes_result[$item["id_mes"]][$item["id_estado"]] = $item;
      } , $estado_mes);
      return $estado_mes_result;
    }
    public function list_dist_producto_terminado($ano) {
      return $this->list_dist_otras_redes($ano, $tipo = 1);
    }
    public function list_dist_materia_prima_rep($ano) {
      return $this->list_dist_otras_redes($ano, 2);
    }
    public function list_dist_mercancia_arribada($ano) {
      return $this->list_dist_otras_redes($ano);
    }
    public function list_dist_otras_redes($ano,$tipo=false)
    {
      if($tipo==1){
        $tipo = '(2,3)';
      }else if($tipo == 2 ){
        $tipo = '(4,5)';
      }else{
        $tipo = '(2,3,4,5)';
      }

      $result = $this->query(
        "SELECT d.id_mes,o.id_rubro,sum(o.cantidad) total
          FROM distribucion d
          JOIN dist_otras_redes o on o.id_dist = d.id_dist
          JOIN meses m on m.id_mes = d.id_mes
          WHERE d.id_tipo_red in ".$tipo." AND ano = '{$ano}' 
          GROUP BY d.id_mes,o.id_rubro
          ORDER BY d.id_mes asc,o.id_rubro asc"
        );
      /*rubros en las distribucion*/
      $result_rubros = $this->query(
        "SELECT o.id_rubro,r.des_rubro
          FROM distribucion d
          JOIN dist_otras_redes o on o.id_dist = d.id_dist
          join rubros r on r.id_rubro = o.id_rubro
          WHERE d.id_tipo_red in ".$tipo." AND ano = '{$ano}' 
          GROUP BY o.id_rubro,r.des_rubro 
          ORDER BY o.id_rubro asc"
        );

      /*end*/
      $productos_result = [];
      array_map(function($item) use (&$productos_result){
        $productos_result[$item["id_mes"]][$item["id_rubro"]] = $item;
      } , $result);
      return [
        "dist"=>$productos_result,
        "rubros"=>$result_rubros,
        ];
    }
    public function area_distribion_otras_resdes($ano='')
    {
      $terminados = $this->db->query(
        "SELECT sum(o.cantidad) total
          FROM distribucion d
          JOIN dist_otras_redes o on o.id_dist = d.id_dist
          WHERE d.id_tipo_red in (2,3) AND ano = '{$ano}' "
        )->row();
      $primas = $this->db->query(
        "SELECT sum(o.cantidad) total
          FROM distribucion d
          JOIN dist_otras_redes o on o.id_dist = d.id_dist
          WHERE d.id_tipo_red in (4,5) AND ano = '{$ano}' "
        )->row();
      $result = [
        // "terminados" => $terminados,
        // "primas"     => $primas,
        "terminado" => $terminados->total ? $terminados->total : 0,
        "prima"     => $primas->total ? $primas->total : 0,
      ];
      return $result;
    }
}

// para las consulta de ventas por meses
/*
SELECT des_red,des_mes,cantidad FROM dist_redes r
join ventas_otras_redes_pp v on v.id_venta = r.id_venta
join tipo_redes t on t.id_tipo_red = r.id_tipo_red
join meses m on m.id_mes = v.id_mes
where ano = '2015'
*/