<?php
class Planificacion_model extends Base_Model{

	protected function _init() {
		$this->_table = 'planificacion';
	}
	public function save(Array $plan,$id_user) {
		try {
			$this->db->trans_begin();

			$now = date("Y-m-d");
				// $plan["nombre"] = $plan["nombre_proyecto"];
			$plan["cant_fisica"] = str_replace(".", "", $plan["cant_fisica"]);
			$plan["cant_fisica"] = str_replace(",", ".", $plan["cant_fisica"]);
			$acciones = $plan["acciones"];
			
			unset($plan["acciones"]);
				// unset($plan["nombre_proyecto"]);
			unset($plan["cant_fisica_mascara"]);

			$plan["fecha_registro"] = $now;
			$plan["id_usuario"] = $id_user;
				$plan["id_estatus"] = 2;//activa
				
				//si siente id_plan, es actulaizar, sino es nuevo registro
				if(isset($plan["id_plan"])){
					$opc = "actulaizar";
					//elimilar las acciones que tenga ese proyecto
					$response['res_delete'] = $this->eliminarAcciones($plan["id_plan"]);
					$this->db->where("id_plan",$plan["id_plan"]);
					$response['res_update'] = $this->db->update($this->_table, $plan);
					$id_insert_last = $plan["id_plan"];
				}else{
					$opc = "registrar";
					$response['res_insert'] = $this->insert($plan);
					$id_insert_last = $this->getLastIdPlan();
				}
				$response["opc"] = $opc;
				$response['id_insert_last'] = $id_insert_last;
				// $this->db->trans_rollback();
				// echo json_encode($response);exit;
				
				foreach ($acciones as $accion) {
					$meses = $accion["meses"];
					unset($accion["meses"]);
					$accion["id_planificacion"] = $id_insert_last;
					$flag = $this->db->insert("acciones", $accion);

					$id_accion = $this->getLastIdAccion();
					foreach ($meses as $mes => $cantidad) {
						$q = $this->db->get_where("meses",["corto"=>$mes]);
						$res_mes = $q->row();
						// var_export($res_mes);
						$flag = $this->db->insert("accion_mes", [
							"id_mes"    => $res_mes->id_mes,
							"id_accion" => $id_accion,
							"cantidad"  => $cantidad
							]);
					}
				}
				$this->db->trans_commit();
			} catch (Exception $e) {	
				$this->db->trans_rollback();
			}
			return $response;
		}
		public function getLastIdPlan(){
			return $this->db->query("select max(id_plan) as id from planificacion")->row()->id;
		}
		public function getLastIdAccion(){
			return $this->db->query("select max(id_accion) as id from acciones")->row()->id;
		}
		public function eliminarAcciones($id_plan){
			$res = [];
			$acciones = $this->db->query("SELECT * FROM acciones where id_planificacion = {$id_plan}")->result_array();
			foreach ($acciones as $accion) {
				$res[] = $this->db->delete("accion_mes",["id_accion" => $accion["id_accion"]]);
				$res[] = $this->db->delete("acciones",["id_accion" => $accion["id_accion"]]);
			}
			return $res;
		}
		public function eliminar($id){
			try {
				$this->db->trans_begin();
				$this->eliminarAcciones($id);
				$res = $this->db->delete($this->_table,["id_plan" => $id]);
				$return["delete"] = $res;
				$this->db->trans_commit();
			} catch (Exception $e) {
				$this->db->trans_rollback();
				$return["delete"] = FALSE;
			}
			return $return;
		}
		public function all(){
			return $this->db->query("SELECT * FROM v_list_planificaiones;")->result_array();
		}
		public function getPlanificaionFull($id_plan = 8){
			$plan_array = $this->db->query("SELECT * FROM planificacion WHERE id_plan = {$id_plan};")->result_array();
			if(count($plan_array)>0){
				$plan = $plan_array[0];
				$acciones_array = $this->db->query("SELECT * FROM acciones WHERE id_planificacion = {$id_plan};")->result_array();
				$acciones = [];
				foreach ($acciones_array as $accion) {
					$id_accion = $accion["id_accion"];
					$accion_mes = $this->db->query("SELECT * FROM accion_mes a JOIN meses m ON a.id_mes = m.id_mes WHERE id_accion = {$id_accion};")->result_array();
					$meses = [];
					foreach ($accion_mes as $row){
						$meses[$row["corto"]] = $row["cantidad"];
					}
					$accion["meses"] = $meses;
					$acciones[] = $accion;
				}
				$plan["acciones"] = $acciones;
				return $plan;
			}else{
				return [];
			}
		}
		/**
		 * 	des: retorna todos los totales de las plantificaciones por estado
		 *	@param $ano
		 *	@return mixed
		 */
		public function list_totales_plan_totales($ano) {
			return $this->query(" 	SELECT  am.id_mes,sum(am.cantidad) total FROM planificacion p
									JOIN acciones a    ON a.id_planificacion = p.id_plan
									JOIN accion_mes am ON am.id_accion = a.id_accion
									WHERE split_part(p.fecha_inicio::text, '-',1) = '{$ano}'
									GROUP BY am.id_mes
									ORDER BY am.id_mes");
		}
	}
