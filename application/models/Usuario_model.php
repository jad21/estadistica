<?php

class Usuario_model extends Base_Model{

    protected function _init() {
        $this->_table = 'usuarios';

    }
    public function verificar($d){
        $this->load->model("roles_model");
        $return = [];
        $user = $this->selectOne(["username"=>$d["username"]]);
        if(count($user)>0){
            $user = $user[0];
            if($user["password"]==md5($d["password"])){
                $r = new Roles_model();
                if ($user['activo']=='t') {
                    $res = $r->selectOne(["id_rol"=>$user["id_rol"]])[0];
                    $user["rol"] = $res["des_rol"];
                    $return = [
                        "code"=>1,
                        "user"=>$user
                    ];    
                }else{
                    $return = [
                        "code"=>2,
                        "msg"=>"Usuario esta inactivo"
                    ];
                }
            }else{
                $return = [
                    "code"=>2,
                    "msg"=>"Clave incorrecta"
                ];
            }
        }else{
            $return = [
                "code"=>2,
                "msg"=>"No Existe el usuario"
            ];    
        }
        return $return;
    }

    public function save(Array $user) {
        $user["password"] = md5($user["password"]);
        return $this->insert($user);
    }
    public function modificar(Array $user) {
        $id = $user["id"];
        unset($user["id"]);
        return $this->update($id,$user);
    }
    
}