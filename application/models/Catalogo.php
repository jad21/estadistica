<?php

class Catalogo extends Base_Model{

    public function getPeriodos(){
    	return $this->db->query("SELECT * FROM periodos;")->result_array();
    }
    public function getMeses(){
    	return $this->db->query("SELECT * FROM meses;")->result_array();
    }
    public function getEstados(){
        return $this->db->query("SELECT * FROM estados;")->result_array();
    }
    public function getRubros($hasta = 14){
        return $this->db->query("SELECT * FROM rubros LIMIT {$hasta};")->result_array();
    }
    public function getRubros_materia_mercal(){
        return $this->db->query("SELECT * FROM rubros WHERE id_rubro<=14;")->result_array();
    }
    public function getRubros_materia_prima(){
        return $this->db->query("SELECT * FROM rubros WHERE id_rubro<=14;")->result_array();
    }
    public function getRubros_producto_terminado(){
    	return $this->db->query("SELECT * FROM rubros WHERE id_rubro>14")->result_array();
    }
    public function getYear(){
    	return $this->db->query("SELECT id_año as id,des FROM años;")->result_array();
    }
    
    
}