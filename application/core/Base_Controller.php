<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Base_Controller extends CI_Controller{

	protected $publicPath = '';
	protected $frontResPath = '';
	protected $adminResPath = '';
	protected $css = [];
	protected $js  = [];
	protected $loadBasePublic = true;
	protected $css_base = [
		"admin/app/css/bootstrap.css",
		"admin/vendor/fontawesome/css/font-awesome.min.css",
		// "admin/vendor/animo/animate+animo.css",
		"admin/vendor/csspinner/csspinner.min.css",
		"admin/vendor/slider/css/slider.css",
		"admin/vendor/chosen/chosen.min.css",
		"admin/vendor/datetimepicker/css/bootstrap-datetimepicker.min.css",
		"admin/vendor/codemirror/lib/codemirror.css",
		"admin/vendor/tagsinput/bootstrap-tagsinput.css",
		"admin/app/css/app.css",
	];
	protected $js_base  = [
		"admin/vendor/jquery/jquery.min.js",
		"admin/vendor/angular/angular.js",
		"admin/vendor/bootstrap/js/bootstrap.min.js",
		"admin/vendor/chosen/chosen.jquery.min.js",
		"admin/vendor/slider/js/bootstrap-slider.js",
		"admin/vendor/filestyle/bootstrap-filestyle.min.js",
		"admin/vendor/animo/animo.min.js",
		"admin/vendor/sparklines/jquery.sparkline.min.js",
		"admin/vendor/slimscroll/jquery.slimscroll.min.js",
		"admin/vendor/codemirror/lib/codemirror.js",
		"admin/vendor/codemirror/addon/mode/overlay.js",
		"admin/vendor/codemirror/mode/markdown/markdown.js",
		"admin/vendor/codemirror/mode/xml/xml.js",
		"admin/vendor/codemirror/mode/gfm/gfm.js",
		"admin/vendor/marked/marked.js",
		"admin/vendor/moment/min/moment-with-langs.min.js",
		"admin/vendor/datetimepicker/js/bootstrap-datetimepicker.min.js",
		"admin/vendor/tagsinput/bootstrap-tagsinput.min.js",
		"admin/vendor/inputmask/jquery.inputmask.bundle.min.js",
		"admin/app/js/app.js",
		"admin/app/js/app.biz.js"
	];


	protected $needLogin = true;

	protected $curSubDomain = 'blog';
	protected function _init() {
		$this->_parseSubDomain();
		
		$this->publicPath = base_url().'public/';
		$this->frontResPath = $this->publicPath.'front/';
		$this->adminResPath = $this->publicPath.'admin/';
	}

	protected function _afterInit() {
		$this->verify();
	}


	/**
	 * TODO 未兼容类似这样的域名 .com.cn, 只支持类似 .com 等一级域名
	 * 解析当前子域名
	 */
	private function _parseSubDomain($filterSubs = array('www')) {
		$host = strtolower($_SERVER['HTTP_HOST']);
		$arrSub = explode('.', $host);
		$countSub = count($arrSub);

		if($countSub > 2) {
			if(!in_array($arrSub[0],$filterSubs)) {
				$this->curSubDomain = $arrSub[0];
			}
		}
	}

	/**
	 * 获取当前子域名
	 * @return string
	 */
	protected function getSubDomain() {
		return $this->curSubDomain;
	}

	/**
	 * 判断是否需要登录
	 */
	protected function verify() {
		if($this->needLogin) {
		   if ($this->notHasUser()) {
				redirect("auth/index");
		   }
		}
	}

	/**
	 * 获取当前登录用户的id
	 */
	protected function getUid() {
		if(!$this->needLogin) {
			return 0;
		}

		return intval($this->session->userdata('user_id'));
	}

	/**
	 * 当前登录的用户
	 * @return bool
	 */
	protected function getUser() {
		if(!$this->needLogin) {
			return false;
		}

		return $this->session->userdata('cur_user');
	}
	protected function notHasUser(){
		return $this->session->has_userdata('cur_user')=="";
	}
	protected function removeUserSession(){
		$this->session->unset_userdata('user_id');
		$this->session->unset_userdata('cur_user');
	}
	protected function setUser($user) {
		$this->session->set_userdata('user_id', $user["id"]);
		$this->session->set_userdata('cur_user', $user);
	}

	
	protected function view($view, $data = array()) {
		$this->load->view($view, $data);
	}
	protected function render($view = '', $data = array()) {
		if(is_array($view)) {
			$data = $view;
			$view = '';
		}

		$view = strtolower($view);
		$view = str_replace('/\/+/', '/', trim($view, '/'));
		$arrSeg = array_filter(explode('/', $view));

		$viewPath = '';
		$pathCount = count($arrSeg);
		switch($pathCount) {
			case 1:
				$viewPath = $this->router->fetch_class().'/'.$view;
				break;

			case 2:
				$viewPath = implode('/', $arrSeg);
				break;

			default:
				$viewPath = $this->router->fetch_class().'/'.$this->router->fetch_method();
				break;
		}
		if($this->loadBasePublic){
			$js = array_merge($this->js_base,$this->js);
			$css = array_merge($this->css_base,$this->css);
		}else{
			$js = $this->js;
			$css = $this->css;
		}
		$data = array_merge($data, array(
			'publicPath' => $this->publicPath,
			'frontResPath' => $this->frontResPath,
			'adminResPath' => $this->adminResPath,
			'curUser' => $this->getUser(),
			'curSubDomain' => $this->curSubDomain,
			'js'=>$js,
			'css'=>$css
		));
		$this->load->view($viewPath, $data);
	}

	/**
	 * 返回json数据
	 * @param $code
	 * @param $msg
	 * @param array $data
	 */
	protected function retJson($code, $msg, $data = array()) {
		$ret = array(
			'meta' => array(
				'code' => $code,
				'msg' => $msg
			),

			'data' => $data
		);

		echo json_encode($ret);
		exit;
	}

	/**
	 * 操作成功， 返回数据
	 * @param $data
	 */
	protected function success($data = array(),$msg ="") {
		$this->retJson(200, $msg, $data);
	}

	/**
	 * 操作失败， 返回失败的信息
	 * @param $msg
	 */
	protected function fail($msg) {
		$this->retJson(-1, $msg);
	}

	/**
	 * 加载model文件
	 * @param $name
	 */
	protected function loadModel($name) {
		$this->load->model($name);
	}

	/**
	 * 加载某个类
	 * @param $name
	 */
	protected function loadLibrary($name) {
		$this->load->library($name);
	}

	/**
	 * 获取传过来的参数($_GET or $_POST)
	 * @param $name
	 * @param  $default
	 * @return null
	 */
	protected function param($name = false, $default = '') {
        $_REQUEST = addslashes_deep($_REQUEST, true);
        if(count($_REQUEST)==0){
            $_REQUEST = json_decode(file_get_contents('php://input'),true);
        }
		if(!$name) return $_REQUEST;
		if(isset($_REQUEST[$name])) {
			return $_REQUEST[$name];
		}
		return $default;
	}



}