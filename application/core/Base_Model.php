<?php
/**
 * 
 * User: Hiko Qiu
 * Date: 2015/6/17
 * Time: 17:29
 */

class Base_Model extends CI_Model{
    const STATUS_DELETE = -1;
    const STATUS_NORMAL = 1;

    // 当前表
    protected $_table = '';

    public function __construct() {
        parent::__construct();

        $this->_init();
    }

    protected function _init() {}

    /**
     * 查询全部
     * @return mixed
     */
    public function selectAll($where = '') {
        if(!empty($where)) {
            $this->db->where($where);
        }

        $query = $this->db->get($this->_table);
        return $query->result_array();
    }

    public function select($where = '', $limit, $offset, $orderBy = '') {
        if(!empty($where)) {
            $this->db->where($where);
        }

        if(!empty($orderBy)){
            $this->db->order_by($orderBy);
        }

        $query = $this->db->get($this->_table, $limit, $offset);
        return $query->result_array();
    }
    public function selectOne($where = '') {
        if(!empty($where)) {
            $this->db->where($where);
        }
        $query = $this->db->get($this->_table, 1);
        return $query->result_array();
    }

    /**
     * 添加一条记录
     * @param $data
     * @return mixed
     */
    public function insert($data) {
        if(!is_array($data)) {
            return false;
        }

        $flag = $this->db->insert($this->_table, $data);
        if(!$flag) {
            return false;
        }

        return $this->db->insert_id();
    }

    /**
     * 更新记录
     * @param $rowId
     * @param $data
     * @return bool
     */
    public function update($rowId, $data) {
        if(!is_array($data)) {
            return false;
        }

        $this->db->where("id = {$rowId}");
        return $this->db->update($this->_table, $data);
    }

    /**
     * 字段值 + $incNum
     * @param $rowId
     * @param $field
     * @param int $incNum
     * @return mixed
     */
    public function inc($rowId, $field, $incNum = 1) {
        return $this->updateCounter($rowId, $field, '+', $incNum);
    }

    /**
     * 字段值 - $decNum
     * @param $rowId
     * @param $field
     * @param int $decNum
     * @return mixed
     */
    public function dec($rowId, $field, $decNum = 1) {
        return $this->updateCounter($rowId, $field, '-', $decNum);
    }

    private function updateCounter($rowId, $field, $op, $num) {
        $this->db->where("id = {$rowId}");
        $this->db->set($field, $field.$op.$num, false);

        return $this->db->update($this->_table);
    }

    /**
     * 根据表字段进行更新
     * @param $arrAttr
     * @param $data
     * @return bool
     */
    public function updateByAttr($arrAttr, $data) {
        if(!is_array($arrAttr) || !is_array($data)) {
            return false;
        }

        $this->db->where($arrAttr);
        return $this->db->update($this->_table, $data);
    }

    /**
     * 返回统计行数
     * @param string $where
     * @return bool
     */
    public function count($where = array()) {
        if(!empty($where)) {
            $this->db->where($where);
            return $this->db->count_all_results($this->_table);
        }else {
            return $this->db->count_all($this->_table);
        }
    }

    /**
     * 创建分页的链接
     * @param $totalRows
     * @param int $perPage
     * @param string $uri
     * @return mixed
     */
    public function createPageLink($totalRows, $perPage = 20,  $uri = 'site/view') {
        $config = array(
            'base_url' => site_url($uri),
            'total_rows' => $totalRows,
            'per_page' => $perPage,

            'first_link' => '首页',
            'last_link' => '尾页',

            // 配置分页的样式
            'first_tag_open' => '<li>',
            'first_tag_close' => '</li>',
            'cur_tag_open' => '<li class="active">',
            'cur_tag_close' => '</li>',
            'prev_tag_open' => '<li>',
            'prev_tag_close' => '</li>',
            'num_tag_open' => '<li>',
            'num_tag_close' => '</li>',
            'next_tag_open' => '<li>',
            'next_tag_close' => '</li>',
        );

        $this->pagination->initialize($config);
        return $this->pagination->create_links();
    }

    /**
     * 获取分页的数据
     * @param array $where
     * @param int $pageSize
     * @param int $offset
     * @return mixed
     */
    public function getPageData($where = array(), $pageSize = 10, $offset = 0, $orderBy = 'id desc') {
        $total = $this->count($where);
        if($offset < 0) {
            $offset = 0;
        }else if($offset > $total){
            $offset = $total;
        }

        return $this->select($where, $pageSize, $offset, $orderBy);
    }

    /**
     * @param $rowId
     * @return null
     */
    public function findById($rowId) {
        $where = array('id' => $rowId);
        $this->db->where($where);

        $query = $this->db->get($this->_table);
        $ret = $query->result();
        return empty($ret) ? null : $ret[0];
    }
    /*
     * para correr un sql
     *
    */
    public function query($sql){
        return $this->db->query($sql)->result_array();
    }

    /**
     * 加载model文件
     * @param $name
     */
    protected function loadModel($name) {
        $this->load->model($name);
    }

    /**
     * 加载某个类
     * @param $name
     */
    protected function loadLibrary($name) {
        $this->load->library($name);
    }
}