<?php
/**
* 
*/
class MyValidador 
{
	protected $array_campos; # ['nombre'=>'rules'] where rule = required
	protected $array_values; # ['nombre'=>'rules'] where rule = required
	function __construct($array=[],$values=[])
	{
		$this->array_campos = $array;
		$this->array_values = $values;
	}

	public function run(){
		foreach ($this->array_campos as $nombre => $rule) {
			if($rule=="required"){
				if (isset($this->array_values[$nombre]) && $this->required($this->array_values[$nombre])) {
					continue;
				}else{
					return false;
				}
			}
		}
		return true;
	}

	function required($str)
	{
		return is_array($str) ? (bool) count($str) : (trim($str) !== '');
	}

}