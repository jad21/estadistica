<?php

class Mantenimiento extends Base_Controller{

	protected $js  = [
		"admin/vendor/angular/angular.js",
		"admin/app/js/ng-valtexto.js",
		"admin/app/js/mantenimineto.js",
	];
	
	protected function _init() {
		parent::_init();
        // $this->loadModel('article_model');
        $this->loadModel('usuario_model');
        $this->loadModel('roles_model');
    }

    public function usuarios() {
        $roles = new Roles_model();
        $this->render([
            "roles"=>json_encode($roles->selectAll()),
        ]);
    }
    
    public function cambiarClave() {
        $this->render('cambioclaveusuario');
    }
    
    public function cambiarmiclave() {
        $this->render();
    }

    public function guardarMiNuevaClave(){
        $id_user = intval($this->getUid());
        $password = $this->param("password");
        $password_actual = $this->param("password_actual");
        $u = new Usuario_model();
        $res_users = $u->selectOne("id = {$id_user}");
        // $this->success($res_users);
        if(count($res_users)>0 AND $res_users[0]['password'] == md5($password_actual) ){
            $res = $u->update($id_user,[
                "password" => md5($password)
                ]);
            if($res){
                $this->success(["cambio"=>$res]);
            }else{
                $this->fail("No se pudo cambiar");
            }
        }else{
            $this->fail("La Clave actual es invalidad");
        }
    }

    public function guardarNuevaClave(){
        $id_user = intval($this->param("id"));
        $password = $this->param("password");
        $u = new Usuario_model();
        $res = $u->update($id_user,[
            "password" => md5($password)
        ]);
        if($res){
            $this->success(["cambio"=>$res]);
        }else{
            $this->fail("No se pudo cambiar");
        }
    }

    public function buscarUsuario(){
        $u = new Usuario_model();
        $user = $u->selectOne(["username"=>$this->param("username")]);
        if (count($user)>0) {
            unset($user[0]["password"]);
        }
        $this->success($user);
    }

    public function guardarUsuario(){
        $id_user = intval($this->param("id",0));
        $user["activo"] = $this->param("activo",false);
        // print_r($user);exit;
        $user = $this->param();
        $u = new Usuario_model();
        if ($id_user>0) {
            $flag = $u->modificar($user);
            if(!$flag) {
                $this->fail("no se actualizo");
            }
            $this->success();
        }else{
            $user_single = $u->selectOne(["username"=>$this->param("username")]);
            if (count($user_single)>0) {
                $this->fail("Usuario duplicado");
            }else{
                $id_user = $u->save($user);
            }
            if(!$id_user) {
                $this->fail("no se guardo");
            }
            $this->success(["id_user"=>$id_user]);
        }
    }
	
	public function backup() {
		$this->load->database();
		// exit($this->db->port);
		$dbname = FCPATH.'dumps/estadistica'.date("_Ymdhis").".backup";
		$cmd = "pg_dump --host \"{$this->db->hostname}\" --port {$this->db->port} -U \"{$this->db->username}\" --dbname \"{$this->db->database}\" --format custom --blobs --verbose --file \"".$dbname."\"";
		// exit($cmd);	
		putenv("PGPASSWORD=" . $this->db->password);
		exec($cmd,$cmdout, $cmdresult);
		putenv("PGPASSWORD");
		if ($cmdresult == " -0" OR $cmdresult !== 0){
			$this->dl_file("{$dbname}");
		}
	}

	public function dl_file($file){
		if (!is_file($file)) { die("<b>404 File not found!</b>"); }
		$len = filesize($file);
		$filename = basename($file);
		$file_extension = strtolower(substr(strrchr($filename,"."),1));
		$ctype="application/force-download";
		header("Pragma: public");
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Cache-Control: public");
		header("Content-Description: File Transfer");
		header("Content-Type: $ctype");
		$header="Content-Disposition: attachment; filename=".$filename.";";
		header($header );
		header("Content-Transfer-Encoding: binary");
		header("Content-Length: ".$len);
		@readfile($file);
		exit;
	}
}