<?php

class Consultas extends Base_Controller{

		protected $js  = [
			 // "admin/vendor/angular/angular.js",
			"admin/vendor/angular-ui/ui-bootstrap-tpls-0.12.1.min.js",
			"admin/vendor/angular/angular-route.min.js",
			"admin/vendor/bootstrap-datepicker/js/bootstrap-datepicker.min.js",
			"admin/vendor/bootstrap-datepicker/locales/bootstrap-datepicker.es.min.js",
			"admin/app/js/ng-valtexto.js",
			"admin/vendor/angular-chart.js/bower_components/Chart.js/Chart.min.js",
			"admin/vendor/angular-chart.js/dist/angular-chart.min.js",
			"admin/app/js/consultas.js",
			"admin/vendor/datatable/media/js/jquery.dataTables.min.js",
			"admin/app/js/directivas-consulta.js",
			/*Para Consultas*/
			"admin/vendor/jsPDF/libs/html2canvas/dist/html2canvas.js",
			"admin/vendor/jsPDF-1.0.272/dist/jspdf.debug.js",
			"admin/vendor/jsPDF-AutoTable/dist/jspdf.plugin.autotable.js",

		];
		protected $css  = [
			"admin/vendor/angular-chart.js/dist/angular-chart.min.css",
			"admin/vendor/datatable/media/css/jquery.dataTables.min.css",
			"admin/vendor/bootstrap-datepicker/css/bootstrap-datepicker.min.css"
		];

		
		protected function _init() {
				parent::_init();

				// $this->loadModel('article_model');
				$this->loadModel('catalogo');
				$this->loadModel('planificacion_model');
		}

		public function index() {
			$c = new Catalogo();
			$this->render([
				// "periodos" => json_encode($c->getPeriodos())
			]);
		}
		public function reportes($tpls) {
			$this->view("consultas/reportes/{$tpls}",[]);
		}
		public function p() {
			// $c = new Catalogo();
			// $this->render([
			// 	// "periodos" => json_encode($c->getPeriodos())
			// ]);
			$an_array = array(
				'item1' => 0,
				'item2' => 0,
				'item3' => 0,
				'item4' => 0,
				'item5' => 0,
				);

			$items_to_modify = array('item1', "item3");

			$res = array_map(function ($value) use (&$an_array ) {
				return   $an_array; //example operation:
			}, $items_to_modify);
			print_r($res);
		}

}

