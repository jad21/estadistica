<?php

class Auth extends Base_Controller{

    protected $css = [
        // <!-- Bootstrap CSS -->
        "admin/app/css/bootstrap.css",
        // <!-- Vendor CSS -->
        "admin/vendor/fontawesome/css/font-awesome.min.css",
        "admin/vendor/animo/animate_animo.css",
        // <!-- App CSS -->
        "admin/app/css/app.css",
    ];
    protected $js  = [
       "admin/vendor/jquery/jquery.min.js",
       "admin/vendor/bootstrap/js/bootstrap.min.js",
       "admin/vendor/animo/animo.min.js",
       "admin/app/js/pages.js",
       "admin/app/js/auth.js"
    ];

    protected function _init() {
        parent::_init();

        $this->needLogin = false;
        $this->loadBasePublic = false;
        $this->loadModel('usuario_model');
    }

    public function index() {
        if($this->session->userdata('user_id')){
            redirect("dashboard/index");
        }else{
            $this->render();
        }
    }
    public function logout() {
        $this->removeUserSession();
        $this->render("index");
    }
    public function login() {
        $user  = new Usuario_model();
        $res = $user->verificar($this->param());
        if($res["code"]==1){
            $this->setUser($res["user"]);
            // $this->success($res["user"]);
            redirect("dashboard/index");
        }else{
            $this->session->set_flashdata('msg::error', $res["msg"]);
            $this->render("index");
        }
    }
    public function base(){
        echo site_url("/");
    }

    public function solicitarMiClaveEmail($email = "estadistica.mercal@gmail.com"){
        $email = $this->param("email");
        $this->loadLibrary('CI_PHPMailer');
        $u           = new Usuario_model();
        $mailConfig  = new CI_PHPMailer();
        $nueva_clave = uniqid('ES');
        $mail        = $mailConfig->configEmail();
        
        
        $mail->SetFrom("estadistica.mercal@gmail.com","Estadistica mercal");  //Who is sending the email
        $mail->Subject    = "Solicitud de clave";
            
        $mail->Body       = 
            "<div style='padding:100px 100px;margin:100px 100px;border:solid black 1px'>".
                "<center>Cambio de Conrtaseña</center><br>".
                "<p>Estimado, Usuario: </p>".
                "<p>Has solicitado una nueva contraseña </p>".
                "<p>Su contraseña es la siguiente : <b>{$nueva_clave}</b> </p>".
                "<p>Recuerde cambiar su contraseña luego de ingresar, </p>".
                "<p>Gracias. </p>".
            "</div>";

        $mail->AddAddress($email,"Usuario");
        if($mail->send()){
            $user_single = $u->selectOne(["correo"=>$email]);
            if (count($user_single)>0) {
                $user_single = $user_single[0];
                $nombre = $user_single["nombre"];
                $res = $u->update($user_single['id'],[
                    "password" => md5($nueva_clave)
                    ]);
                if($res){
                    $this->success(["cambio"=>$res],"Revice su correo, {$nombre}");
                }else{
                    $this->fail("No se pudo cambiar");
                }
            }else{
                $this->fail("Disculpe, estimado Usuario,usted no esta en nuestra base de datos");
            }
        }else{
            $this->fail("Verifique su conexion de internet");
        }
    }
}