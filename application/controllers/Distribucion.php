<?php

class Distribucion extends Base_Controller{

    protected $js  = [
      "admin/vendor/AngularFileUpload/angular-file-upload.js",
      "admin/vendor/angular-ui/ui-bootstrap-tpls-0.12.1.min.js",
      "admin/vendor/bootstrap-datepicker/locales/bootstrap-datepicker.es.min.js",
      "admin/vendor/bootstrap-datepicker/js/bootstrap-datepicker.min.js",
      "admin/app/js/ng-valtexto.js",
      "admin/app/js/distribucion.js",
      "admin/vendor/datatable/media/js/jquery.dataTables.min.js",
      "admin/app/js/directivas-consulta.js",
    ];
    protected $css  = [
      "admin/vendor/datatable/media/css/jquery.dataTables.min.css",
      "admin/vendor/bootstrap-datepicker/css/bootstrap-datepicker.min.css"
    ];

    
    protected function _init() {
      parent::_init();
      $this->loadModel('catalogo');
      $this->loadModel('distribucion_model');
      $this->loadModel('documentos');
    }

    public function guardar() {
      // $this->retJson(1,"",$this->param());
      $d = new Distribucion_model();
      $res = $d->save($this->param(),$this->getUid());
      
      if( !isset($res["code"]) or $res["code"]==-1){
        $this->fail("Ha ocurrido algo inesperado");
      }
      if($res["code"]==0){
        $this->fail("Ya Existe una distribución con ese año y mes");
      }
      $this->success($res);
    }

    /*
     * tipo de red: otras redes materia prima
     */
    public function guardar_prima() {
      $d = new Distribucion_model();
      $res = $d->guardar_prima($this->param(),$this->getUid());
      if( !isset($res["code"]) or $res["code"]==-1){
        $this->fail("Ha ocurrido algo inesperado");
      }
      if($res["code"]==0){
        $this->fail(" Ya Existe una distribución con ese año, mes y tipo de red ");
      }
      $this->success($res);
    }

    public function guardar_ventas() {
      $d = new Distribucion_model();
      $res = $d->guardar_ventas($this->param(),$this->getUid());
      if( !isset($res["code"]) or $res["code"]==-1){
        $this->fail("Ha ocurrido algo inesperado");
      }
      if($res["code"]==0){
        $this->fail(" Ya Existe una distribución con ese año y mes ");
      }
      $this->success($res);
    }

    public function l() {
      $d = new Distribucion_model();
      $res = $d->getDistById(31);
      $this->success($res);
    }
    public function actualizar_mercal($id_dist="") {
      if($id_dist==""){
        redirect('distribucion/index');
      }else{
        $p = new Distribucion_model();
        $distribucion = $p->getDistById($id_dist);
        if($distribucion){
          $c = new Catalogo();
          $this->render([
            "anos" => json_encode($c->getYear()),
            "meses" => json_encode($c->getMeses()),
            "estados" => $c->getEstados(),
            "rubros" => $c->getRubros(),
            "distribucion" => json_encode($distribucion)
          ]);          
        }else{
          redirect('distribucion/index');
        }
      }
    }
    public function actualizar_otras_redes_primas($id_dist="") {
      if($id_dist==""){
        redirect('distribucion/otras_redes_primas');
      }else{
        $p = new Distribucion_model();
        $distribucion = $p->getDistOtrasRedesById($id_dist,$prima=1);
        if($distribucion){
          $c = new Catalogo();
          $this->render([
            "anos" => json_encode($c->getYear()),
            "meses" => json_encode($c->getMeses()),
            "distribucion" => json_encode($distribucion)
          ]);          
        }else{
          redirect('distribucion/otras_redes_primas');
        }
      }
    }
    public function actualizar_otras_redes_terminados($id_dist="") {
      if($id_dist==""){
        redirect('distribucion/otras_redes_primas');
      }else{
        $p = new Distribucion_model();
        $distribucion = $p->getDistOtrasRedesById($id_dist,$terminado=2);
        if($distribucion){
          $c = new Catalogo();
          $this->render([
            "anos" => json_encode($c->getYear()),
            "meses" => json_encode($c->getMeses()),
            "distribucion" => json_encode($distribucion)
          ]);          
        }else{
          redirect('distribucion/otras_redes_primas');
        }
      }
    }
    public function actualizar_ventas_otras_redes($id_venta="") {
      if($id_venta==""){
        redirect('distribucion/ventas_otras_redes_pp');
      }else{
        $p = new Distribucion_model();
        $venta = $p->getVentaOtrasRedesById($id_venta);
        if($venta){
          $c = new Catalogo();
          $this->render([
            "anos" => json_encode($c->getYear()),
            "meses" => json_encode($c->getMeses()),
            "venta" => json_encode($venta)
          ]);          
        }else{
          redirect('distribucion/ventas_otras_redes_pp');
        }
      }
    }
    public function eliminar() {
      $m = new Distribucion_model();
      $res = $m->eliminar($this->param("id_dist"));
      if($res['delete']==TRUE){
        $this->success($res);
      }else{
        $this->fail("no se elimino");
      }
    }

    public function eliminar_otras_redes() {
      $m = new Distribucion_model();
      $res = $m->eliminarOtrasRedes($this->param("id_dist"));
      if($res['delete']==TRUE){
        $this->success($res);
      }else{
        $this->fail("no se elimino");
      }
    }

    public function eliminar_venta_otras_redes() {
      $m = new Distribucion_model();
      $res = $m->eliminarVentaOtrasRedes($this->param("id_venta"));
      if($res['delete']==TRUE){
        $this->success($res);
      }else{
        $this->fail("no se elimino");
      }
    }
    
    public function ver($id_dist = 9) {
      $p = new Distribucion_model();
      $this->render("todas",[
        "lista" => $p->allFull($id_dist)
      ]);
    }
    public function listar_red_materia_prima() {
      $p = new Distribucion_model();
      $this->render([
        "lista" => $p->list_dist_materia_prima()
      ]);
    }
    public function listar_red_productos_terminados() {
      $p = new Distribucion_model();
      $this->render([
        "lista" => $p->list_dist_productos_terminados()
      ]);
    }
    public function list_ventas_otras_redes() {
      $p = new Distribucion_model();
      $this->render([
        "lista" => $p->list_ventas_otras_redes()
      ]);
    }
    public function index() {
      $c = new Catalogo();
      $this->render("index",[
        "anos" => json_encode($c->getYear()),
        "meses" => json_encode($c->getMeses()),
        "estados" => $c->getEstados(),
        "rubros" => $c->getRubros($hasta=14),
      ]);
    }
    public function otras_redes() {
      $c = new Catalogo();
      $this->render([
        "anos" => json_encode($c->getYear()),
        "meses" => json_encode($c->getMeses()),
        "estados" => $c->getEstados(),
        "rubros_terminados" => $c->getRubros_materia_prima($hasta=14),
        "rubros_prima" => $c->getRubros_producto_terminado($hasta=33),
      ]);
    }
    public function otras_redes_primas() {
      $c = new Catalogo();
      $this->render([
        "anos" => json_encode($c->getYear()),
        "meses" => json_encode($c->getMeses()),
        "estados" => $c->getEstados(),
        "rubros_terminados" => $c->getRubros_materia_prima($hasta=14),
        "rubros_prima" => $c->getRubros_producto_terminado($hasta=33),
      ]);
    }
    public function otras_redes_terminados() {
      $c = new Catalogo();
      $this->render([
        "anos" => json_encode($c->getYear()),
        "meses" => json_encode($c->getMeses()),
        // "rubros_terminados" => $c->getRubros_materia_prima($hasta=14),
        // "rubros_prima" => $c->getRubros_producto_terminado($hasta=33),
      ]);
    }
    public function ventas_otras_redes_pp() {
      $c = new Catalogo();
      $this->render([
        "anos" => json_encode($c->getYear()),
        "meses" => json_encode($c->getMeses()),
        "estados" => $c->getEstados(),
      ]);
    }
    public function mercal(){
      $this->index();
    }   
    public function upload() {
      $config['allowed_types']    = 'application/pdf';
      $config['max_size']         = 1024*4; //4MB
      $path                       = 'upload/';
      $config['upload_path']      = FCPATH.$path;
      $new_name_file              = $this->getUid() . date("-Ymdhis-") . basename($_FILES["file"]["name"]);
      $config['upload_path_full'] = $config['upload_path'] . $new_name_file;
      $path_new                   = $path . $new_name_file;
      
      if ($config['allowed_types']!=$_FILES["file"]["type"]) {
        $this->fail("Solo se aceptan PDF");
      }

      $b_movio = move_uploaded_file($_FILES["file"]["tmp_name"], $config['upload_path_full']);
      if($b_movio){
        $d = new Documentos();
        $id = $d->insert([
          "path" => $path_new,
          "nombre" => $new_name_file,
        ]);
        $this->success([
          "id_documento" => $id
        ]);
      }
      $this->fail("Error al subir el archivo");
    }
    /**
     *   des: lista _dist_mercal_estado_y_producto
     *  @param $ano
     *  @return mixed
     */
    public function c_list_dist_mercal_estado_y_producto($ano=false) {
      $this->loadModel('planificacion_model');
      
      $c = new Catalogo();
      $d = new Distribucion_model();
      $p = new Planificacion_model();
      
      if ($ano) {
        $this->success([
            "lista"  => $d->list_dist_mercal_estado_y_producto($ano), 
            "meses"  => $c->getMeses(),
            "rubros" => $c->getRubros($hasta=14),
            "plan"   => $p->list_totales_plan_totales($ano)
          ]);
      }else{
        $this->fail("Falta el año");
      }
    }
    public function c_list_dist_mercal_estado_y_mes($ano=false) {
      $c = new Catalogo();
      $d = new Distribucion_model();      
      
      if ($ano) {
        $this->success([
            "lista"  => $d->list_dist_mercal_estado_y_mes($ano), 
            "meses"  => $c->getMeses(),
            "estados"  => $c->getEstados(),
          ]);
      }else{
        $this->fail("Falta el año");
      }
    }
    public function c_list_dist_producto_terminado($ano=false) {
      $c = new Catalogo();
      $d = new Distribucion_model();      
      if ($ano) {
        $res = $d->list_dist_producto_terminado($ano);
        $this->success([
            "lista"  => $res["dist"], 
            "meses"  => $c->getMeses(),
            "rubros" => $res["rubros"], 
          ]);
      }else{
        $this->fail("Falta el año");
      }
    }
    public function c_list_dist_materia_prima($ano=false) {
      $c = new Catalogo();
      $d = new Distribucion_model();      
      if ($ano) {
        $res = $d->list_dist_materia_prima_rep($ano);
        $this->success([
            "lista"  => $res["dist"], 
            "meses"  => $c->getMeses(),
            "rubros" => $res["rubros"], 
          ]);
      }else{
        $this->fail("Falta el año");
      }
    }
    public function c_list_dist_mercancia_arribada($ano=false) {
      $c = new Catalogo();
      $d = new Distribucion_model();      
      if ($ano) {
        $res = $d->list_dist_mercancia_arribada($ano);
        $this->success([
            "lista"  => $res["dist"], 
            "meses"  => $c->getMeses(),
            "rubros" => $res["rubros"], 
            "area"   => $d->area_distribion_otras_resdes($ano), 
          ]);
      }else{
        $this->fail("Falta el año");
      }
    }

}