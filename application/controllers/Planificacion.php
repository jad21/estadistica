<?php

class Planificacion extends Base_Controller{

		protected $js  = [
			 // "admin/vendor/angular/angular.js",
			 "admin/vendor/angular-ui/ui-bootstrap-tpls-0.12.1.min.js",
			 "admin/vendor/bootstrap-datepicker/js/bootstrap-datepicker.min.js",
			 "admin/vendor/bootstrap-datepicker/locales/bootstrap-datepicker.es.min.js",
			 "admin/app/js/ng-valtexto.js",
			 "admin/app/js/planificacion.js",
			 "admin/vendor/datatable/media/js/jquery.dataTables.min.js",
			 "admin/app/js/directivas-consulta.js",
		];
		protected $css  = [
			"admin/vendor/datatable/media/css/jquery.dataTables.min.css",
			"admin/vendor/bootstrap-datepicker/css/bootstrap-datepicker.min.css"
		];

		
		protected function _init() {
				parent::_init();

				// $this->loadModel('article_model');
				$this->loadModel('catalogo');
				$this->loadModel('planificacion_model');
		}

		public function guardar() {
			$this->load->library('MyValidador');
			$validation = new MyValidador([
				"acciones"=>"required",
				"nombre"=>"required",
				"fecha_inicio"=>"required",
				"fecha_fin"=>"required",
				"cant_fisica"=>"required",
				"meta"=>"required",
			],$this->param());
			// $this->success($this->param());
			if ($validation->run() == FALSE){
				$this->fail("error en en la validacion");
			}else{
				$p = new Planificacion_model();
				$this->success(
					$p->save($this->param(),$this->getUid())
					);
			}
		}
		public function index() {
			$c = new Catalogo();
			$this->render([
				"periodos" => json_encode($c->getPeriodos())
			]);
		}
		public function actualizar($id_plan="") {
			if($id_plan==""){
				redirect('planificacion/index');
			}else{
				$p = new Planificacion_model();
				$planificacion = $p->getPlanificaionFull($id_plan);
				if(count($planificacion)>0){
					$c = new Catalogo();
					$this->render([
						"planificacion" => json_encode($planificacion),
						"periodos" => json_encode($c->getPeriodos())
					]);          
				}else{
					redirect('planificacion/index');
				}
			}
		}
		public function eliminar() {
			$p = new Planificacion_model();
			$res = $p->eliminar($this->param("id_plan"));
			if($res['delete']==TRUE){
				$this->success($res);
			}else{
				$this->fail("no se elimino");
			}
		}
		public function ver() {
			$c = new Planificacion_model();
			$this->render("todas",[
				"lista" => $c->all()
			]);
		}
		public function modalAccion() {
			$this->view("planificacion/modal");
		}
		public function t($id_plan=8){
			$p = new Planificacion_model();
			echo json_encode($p->getPlanificaionFull($id_plan));
		}
		public function c_list_totales_plan_totales($ano=false){
			$p = new Planificacion_model($ano);
			if ($ano) {
				$this->success($p->list_totales_plan_totales($ano));
			}else{
				$this->fail("error: falta el año");
			}
		}

}