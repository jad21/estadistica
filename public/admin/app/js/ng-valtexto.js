(function(angular){
	angular.module('ngValtexto', [])
		.directive('ngValtexto', ['$parse', function($parse) {
			var exp = {
        numero:/^[0-9\x00\b]+$/,
				nombre:/^[a-zA-Z \u00e1\u00e9\u00ed\u00f3\u00fa\u00c1\u00c9\u00cd\u00d3\u00da\u00f1\u00d1\u00FC\u00DC\x00\b]+$/,
			}
			return function(scope, element, attr) {
				var val = attr['ngValtexto'];

				element.bind('keypress', function(event) {
					if (event.ctrlKey || event.altKey){return true;}
        			return validar(String.fromCharCode(event.which), exp[val]);
				});
				element.bind('blur', function(event) {
        			return element.val(priLetraMayAll(trim(cambio(element.val(), exp[val]))));
				});
			};
			function validar(cadena, correcta){
				if (correcta.test(cadena)){
					return true;
				}else {
					return false;
				}
			};
			function trim(myString){
				return myString.replace(/([\ \t]+(?=[\ \t])|^\s+|\s+$)/g, '').replace(/^\s+/g,'');
			};
			function cambio(cadena, correcta){
				var cadena_correcta = "";
				for (var i = 0; i< cadena.length; i++) {
					var caracter = cadena.charAt(i);
					if (validar(caracter, correcta)){cadena_correcta = cadena_correcta + caracter;}
				};
				return cadena_correcta;
			}
			function priLetraMayAll(cadena){
				function priLetraMay($1,$2) { return $1.toUpperCase();};
				var expreg=/(^| )([\wáéíóúñ])/g;
				return cadena.replace(expreg, priLetraMay);
			};
		}])
})(angular);