var auth = (function(window,base){
	
	var alerta = $("#alert");

	var solicitarClave = function(email,cb){
		$.post(base +'index.php/auth/solicitarMiClaveEmail' ,{email:email}, cb);
	}
	return {
		solicitarClave:function(){
			var email   = $("#email").val();
			var btn     = $("#btnSolicitud");
			var ventana = $("#modal");
			btn.attr("disabled","disabled");
			solicitarClave(email,function(response){
				response = JSON.parse(response);
				if (response.meta) {
					if(response.meta.code==-1){
						alerta.addClass("alert-danger");
						console.info("alert-danger")
					}else if(response.meta.code==200){
						alerta.addClass("alert-info");	
						console.info("alert-info")
					}else{
						alerta.addClass("alert-default");	
						console.info("alert-default")
					}
					alerta
						.text(response.meta.msg)
						.show();
				};
				btn.removeAttr("disabled");
				ventana.modal("hide");
				setInterval(function(){
					alerta
						.removeClass("alert-danger")	
						.removeClass("alert-info")
						.hide();
				},10000);
			});
		}
	}
})(window,window.BASE); 