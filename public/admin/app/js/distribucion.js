(function(angular,prefix){
angular.module('#app.distribucion', ["ui.bootstrap","angularFileUpload"])
	.service('CrubSrv', ['$http','$log','$q',function ($http,$log,$q) {    
		this.guardar = function(data){
			return $http.post(prefix + 'distribucion/guardar',data);
		}
		this.guardar_prima = function(data){
			return $http.post(prefix + 'distribucion/guardar_prima',data);
		}
		this.guardar_ventas = function(data){
			return $http.post(prefix + 'distribucion/guardar_ventas',data);
		}
		this.eliminar = function(data){
			return $http.post(prefix + 'distribucion/eliminar',data);
		}
		this.eliminar_otras_redes = function(tipo,id_dist){
			return $http.post(prefix + 'distribucion/eliminar_otras_redes',{tipo:tipo,id_dist:id_dist});
		}
		this.eliminar_venta_otras_redes = function(data){
			return $http.post(prefix + 'distribucion/eliminar_venta_otras_redes',data);
		}
	}])
	.controller('DistribucionCtrl', ['$scope', 'CrubSrv',"$timeout","$modal","$upload",function ($scope,CrubSrv,$timeout,$modal,$upload) {    
		$timeout(function() {
			$("#tabla_rubros").dataTable({
				"scrollX":true,
				"language":{
					"sProcessing": "Procesando...",
					"sLengthMenu": "Ver listado de _MENU_",
					"sZeroRecords": "No hay registros seleccionados",
					"sInfo": "_START_ al _END_ de _TOTAL_ registros",
					"sInfoEmpty": "0 al 0 de 0 registros",
					"sInfoFiltered": "(filtrado de _MAX_ registros)",
					"sInfoPostFix": "",
					"sSearch": "Filtrar : ",
					"sUrl": "",
					"oPaginate": {
						"sFirst":    "Primero",
						"sPrevious": "Anterior",
						"sNext":     "Siguiente",
						"sLast":     "\u00daltimo"
					}
				},
			});
			$("#tabla_datatable").dataTable({
				"language":{
					"sProcessing": "Procesando...",
					"sLengthMenu": "Ver listado de _MENU_",
					"sZeroRecords": "No hay registros seleccionados",
					"sInfo": "_START_ al _END_ de _TOTAL_ registros",
					"sInfoEmpty": "0 al 0 de 0 registros",
					"sInfoFiltered": "(filtrado de _MAX_ registros)",
					"sInfoPostFix": "",
					"sSearch": "Filtrar : ",
					"sUrl": "",
					"oPaginate": {
						"sFirst":    "Primero",
						"sPrevious": "Anterior",
						"sNext":     "Siguiente",
						"sLast":     "\u00daltimo"
					}
				},
			});
		});
		$timeout(function() {
			console.info($scope.estadosRubros);
		},2000);

		$scope.mensajes = [];
		$scope.form = {};
		$scope.rubros = [
			{id_rubro:1,cantidad:null,nombre:"Aceite"},
			{id_rubro:2,cantidad:null,nombre:"Arroz"},
			{id_rubro:3,cantidad:null,nombre:"Arvejas"},
			{id_rubro:4,cantidad:null,nombre:"Azucar"},
			{id_rubro:5,cantidad:null,nombre:"Caraotas"},
			{id_rubro:6,cantidad:null,nombre:"Carne"},
			{id_rubro:7,cantidad:null,nombre:"Harina P"},
			{id_rubro:8,cantidad:null,nombre:"Harina T"},
			{id_rubro:9,cantidad:null,nombre:"Lentejas"},
			{id_rubro:10,cantidad:null,nombre:"Leche"},
			{id_rubro:11,cantidad:null,nombre:"Margarina"},
			{id_rubro:12,cantidad:null,nombre:"Mortadela"},
			{id_rubro:13,cantidad:null,nombre:"Pasta"},
			{id_rubro:14,cantidad:null,nombre:"Pollo"},
		];

		$scope.estadosRubros = {
			// 1:{id_estado:1,des_estado:"Amazonas", data:[{id_rubro:1,cantidad:1,nombre:"Aceite"}, {id_rubro:2,cantidad:1,nombre:"Arroz"}, {id_rubro:3,cantidad:1,nombre:"Arvejas"}, {id_rubro:4,cantidad:1,nombre:"Azucar"}, {id_rubro:5,cantidad:1,nombre:"Caraotas"}, {id_rubro:6,cantidad:1,nombre:"Carne"}, {id_rubro:7,cantidad:1,nombre:"Harina P"}, {id_rubro:8,cantidad:1,nombre:"Harina T"}, {id_rubro:9,cantidad:1,nombre:"Lentejas"}, {id_rubro:10,cantidad:1,nombre:"Leche"}, {id_rubro:11,cantidad:1,nombre:"Margarina"}, {id_rubro:12,cantidad:1,nombre:"Mortadela"}, {id_rubro:13,cantidad:1,nombre:"Pasta"}, {id_rubro:14,cantidad:1,nombre:"Pollo"}, ] },
			// 1:{id_estado:1,des_estado:"anzo", data:[{id_rubro:1,cantidad:1,nombre:"Aceite"}, {id_rubro:2,cantidad:1,nombre:"Arroz"}, {id_rubro:3,cantidad:1,nombre:"Arvejas"}, {id_rubro:4,cantidad:1,nombre:"Azucar"}, {id_rubro:5,cantidad:1,nombre:"Caraotas"}, {id_rubro:6,cantidad:1,nombre:"Carne"}, {id_rubro:7,cantidad:1,nombre:"Harina P"}, {id_rubro:8,cantidad:1,nombre:"Harina T"}, {id_rubro:9,cantidad:1,nombre:"Lentejas"}, {id_rubro:10,cantidad:1,nombre:"Leche"}, {id_rubro:11,cantidad:1,nombre:"Margarina"}, {id_rubro:12,cantidad:1,nombre:"Mortadela"}, {id_rubro:13,cantidad:1,nombre:"Pasta"}, {id_rubro:14,cantidad:1,nombre:"Pollo"}, ] },
			// 2:{id_estado:2,des_estado:"anzo", data:[{id_rubro:1,cantidad:1,nombre:"Aceite"}, {id_rubro:2,cantidad:1,nombre:"Arroz"}, {id_rubro:3,cantidad:1,nombre:"Arvejas"}, {id_rubro:4,cantidad:1,nombre:"Azucar"}, {id_rubro:5,cantidad:1,nombre:"Caraotas"}, {id_rubro:6,cantidad:1,nombre:"Carne"}, {id_rubro:7,cantidad:1,nombre:"Harina P"}, {id_rubro:8,cantidad:1,nombre:"Harina T"}, {id_rubro:9,cantidad:1,nombre:"Lentejas"}, {id_rubro:10,cantidad:1,nombre:"Leche"}, {id_rubro:11,cantidad:1,nombre:"Margarina"}, {id_rubro:12,cantidad:1,nombre:"Mortadela"}, {id_rubro:13,cantidad:1,nombre:"Pasta"}, {id_rubro:14,cantidad:1,nombre:"Pollo"}, ] },
			// 3:{id_estado:3,des_estado:"apure", data:[{id_rubro:1,cantidad:1,nombre:"Aceite"}, {id_rubro:2,cantidad:1,nombre:"Arroz"}, {id_rubro:3,cantidad:1,nombre:"Arvejas"}, {id_rubro:4,cantidad:1,nombre:"Azucar"}, {id_rubro:5,cantidad:1,nombre:"Caraotas"}, {id_rubro:6,cantidad:1,nombre:"Carne"}, {id_rubro:7,cantidad:1,nombre:"Harina P"}, {id_rubro:8,cantidad:1,nombre:"Harina T"}, {id_rubro:9,cantidad:1,nombre:"Lentejas"}, {id_rubro:10,cantidad:1,nombre:"Leche"}, {id_rubro:11,cantidad:1,nombre:"Margarina"}, {id_rubro:12,cantidad:1,nombre:"Mortadela"}, {id_rubro:13,cantidad:1,nombre:"Pasta"}, {id_rubro:14,cantidad:1,nombre:"Pollo"}, ] },

		};
		// console.info("estadosRubros['1'].data[1].cantidad",$scope.estadosRubros[1].data[0].cantidad)  ;
		var iniciar = function(init){
			// if(angular.isUndefined(init)){
			// 	init = 1;
			// }else{
			// 	init = init + 1;
			// }
			// for (var i = init; i < 25; i++) {
			// 	$scope.estadosRubros[i] = {id_estado:i,data:[{id_rubro:1,cantidad:null,nombre:"Aceite"}, {id_rubro:2,cantidad:null,nombre:"Arroz"}, {id_rubro:3,cantidad:null,nombre:"Arvejas"}, {id_rubro:4,cantidad:null,nombre:"Azucar"}, {id_rubro:5,cantidad:null,nombre:"Caraotas"}, {id_rubro:6,cantidad:null,nombre:"Carne"}, {id_rubro:7,cantidad:null,nombre:"Harina P"}, {id_rubro:8,cantidad:null,nombre:"Harina T"}, {id_rubro:9,cantidad:null,nombre:"Lentejas"}, {id_rubro:10,cantidad:null,nombre:"Leche"}, {id_rubro:11,cantidad:null,nombre:"Margarina"}, {id_rubro:12,cantidad:null,nombre:"Mortadela"}, {id_rubro:13,cantidad:null,nombre:"Pasta"}, {id_rubro:14,cantidad:null,nombre:"Pollo"}, ]};
			// };
		}
		$scope.iniciar = iniciar;
		// 

		$scope.eliminar = function(id_dist){
			CrubSrv.eliminar({id_dist:id_dist}).success(function(data,status){
				if(status!=200){
					$scope.mensajes.push({type:'danger',text:'algo va mal :('});
				}if(data.meta.code==-1){
					$scope.mensajes.push({type:'warning',text:'No se pudo eliminar'});
				}else{
					$scope.mensajes.push({text:'Cambios exitoso!'});
					window.location.reload();
				}
			});
		}
		var borrarIndexVacios = function(){
			return
			for (var i = 1; i < 25 ; i++) {
				var contador = 0;
				console.info("paso 0 existe",$scope.estadosRubros[i]);
				console.info("paso 1 data",$scope.estadosRubros[i].data);
				console.info("paso 2 length",$scope.estadosRubros[i].data.length);
				for (var j = 0; j < $scope.estadosRubros[i].data.length; j++) {
					if(angular.isDefined($scope.estadosRubros[i].data[j]) && $scope.estadosRubros[i].data[j].cantidad==null)contador++;
				}
				if(contador==$scope.estadosRubros[i].data.length){
					delete $scope.estadosRubros[i];
				}
			};
		}
		
		$scope.inicializar = function(i){
			if(angular.isUndefined($scope.estadosRubros[i])){
				$scope.estadosRubros[i] = {id_estado:i,data:[{id_rubro:1,cantidad:null,nombre:"Aceite"}, {id_rubro:2,cantidad:null,nombre:"Arroz"}, {id_rubro:3,cantidad:null,nombre:"Arvejas"}, {id_rubro:4,cantidad:null,nombre:"Azucar"}, {id_rubro:5,cantidad:null,nombre:"Caraotas"}, {id_rubro:6,cantidad:null,nombre:"Carne"}, {id_rubro:7,cantidad:null,nombre:"Harina P"}, {id_rubro:8,cantidad:null,nombre:"Harina T"}, {id_rubro:9,cantidad:null,nombre:"Lentejas"}, {id_rubro:10,cantidad:null,nombre:"Leche"}, {id_rubro:11,cantidad:null,nombre:"Margarina"}, {id_rubro:12,cantidad:null,nombre:"Mortadela"}, {id_rubro:13,cantidad:null,nombre:"Pasta"}, {id_rubro:14,cantidad:null,nombre:"Pollo"}, ]};
			}
		}

		$scope.onFileSelect = function($files) {
			for (var i = 0; i < $files.length; i++) {
				var $file = $files[i];
				$upload.upload({
					url: prefix + 'distribucion/upload',
					file: $file,
					progress: function(e){}
				}).then(function(response) {
					$timeout(function() {
						if (response.status==200) {
							if(response.data.meta.code == -1){
								$scope.mensajes.push({type:'danger',text:response.data.meta.msg});
							}else{
								$scope.form.id_documento = response.data.data.id_documento;
							}
							
						};
						console.info(response);
						// $scope.uploadResult.push(response.data);
					});
				});
			}
		}
		$scope.limpiar = function(){
			$scope.form = {
				radio : 'preliminar',
				id_estado : $scope.estados[0].id_estado
			};
			$scope.estadosRubros = {};
			
			iniciar();
		}
		$scope.guardar = function(form){
			if(angular.isUndefined( $scope.form.ano ) || angular.isUndefined( $scope.form.mes ) ){
				$scope.mensajes.push({type:'warning',text:'Los campos año y mes son obligatorios'});
				return;
			}
			if($scope.form.radio == "consolidado" && angular.isUndefined( $scope.form.id_documento ) ){
				$scope.mensajes.push({type:'warning',text:'Debe subir el documento consolidado'});
				return;
			}
			borrarIndexVacios();//para borrar los indices vacios
			CrubSrv.guardar({form:form,estadosRubros:$scope.estadosRubros}).success(function(data,status){
				if(status!=200){
					$scope.mensajes.push({type:'danger',text:'algo va mal :('});
				}else if(data.meta.code==-1){
					$scope.mensajes.push({type:'warning',text:'No se guardo la distribución! ' + data.meta.msg});
				}else{
					$scope.mensajes.push({text:'Registro exitoso'});
					$scope.limpiar();
				}
				$timeout(function(){
					$scope.mensajes = [];
				},8000);
			});
		}
		$scope.actualizar = function(form){
			if(angular.isUndefined( $scope.form.ano ) || angular.isUndefined( $scope.form.mes ) ){
				$scope.mensajes.push({type:'warning',text:'Los campos año y mes son obligatorios'});
				return;
			}
			if($scope.form.radio == "consolidado" && angular.isUndefined( $scope.form.id_documento ) ){
				$scope.mensajes.push({type:'warning',text:'Debe subir el documento consolidado'});
				return;
			}
			borrarIndexVacios();//para borrar los indices vacios
			CrubSrv.guardar({form:form,estadosRubros:$scope.estadosRubros}).success(function(data,status){
				if(status!=200){
					$scope.mensajes.push({type:'danger',text:'algo va mal :('});
				}else if(data.meta.code==-1){
					$scope.mensajes.push({type:'warning',text:'No se guardo la distribución! ' + data.meta.msg});
				}else{
					$scope.mensajes.push({text:'Actualizacion exitoso'});
					$scope.limpiar();
					window.history.back();
				}
				$timeout(function(){
					$scope.mensajes = [];
				},8000);
			});
		}   
	}])
	.controller('DistribucionOtrasRedesCtrl', ['$scope', 'CrubSrv',"$timeout","$modal","$upload",function ($scope,CrubSrv,$timeout,$modal,$upload) {    

		$scope.mensajes = [];
		$scope.form = {};
		$scope.rubros = [];
		$scope.limpiar = function(){
			$scope.form = {};
			$scope.rubros = [];
		}
		$scope.eliminar_terminado = function(id_dist){
			CrubSrv.eliminar_otras_redes(2,id_dist).success(function(data,status){
				if(status!=200){
					$scope.mensajes.push({type:'danger',text:'algo va mal :('});
				}if(data.meta.code==-1){
					$scope.mensajes.push({type:'warning',text:'No se pudo eliminar'});
				}else{
					$scope.mensajes.push({text:'Cambios exitoso!'});
					window.location.reload();
				}
			})
		}
		$scope.eliminar_prima = function(id_dist){
			CrubSrv.eliminar_otras_redes(1,id_dist).success(function(data,status){
				if(status!=200){
					$scope.mensajes.push({type:'danger',text:'algo va mal :('});
				}if(data.meta.code==-1){
					$scope.mensajes.push({type:'warning',text:'No se pudo eliminar'});
				}else{
					$scope.mensajes.push({text:'Cambios exitoso!'});
					window.location.reload();
				}
			})	
		}
		$scope.atras = function(){
			window.history.back();
		}
		$scope.guardar = function (form){
			if(angular.isUndefined( $scope.form.ano ) || angular.isUndefined( $scope.form.mes ) || angular.isUndefined( $scope.form.id_tipo_red ) ){
				$scope.mensajes.push({type:'warning',text:'Los campos año, mes y tipo de red son obligatorios'});
				return;
			}
			CrubSrv.guardar_prima(form).success(function(data,status){
				if(status!=200){
					$scope.mensajes.push({type:'danger',text:'algo va mal :('});
				}else if(data.meta.code==-1){
					$scope.mensajes.push({type:'warning',text:'No se guardo la distribución! ' + data.meta.msg});
				}else{
					$scope.limpiar();
					$scope.mensajes.push({text:'Registro exitoso'});
				}
				$timeout(function(){
					$scope.mensajes = [];
				},8000);
			});
		}
		$scope.actualizar = function (form){
			if(angular.isUndefined( $scope.form.ano ) || angular.isUndefined( $scope.form.mes ) || angular.isUndefined( $scope.form.id_tipo_red ) ){
				$scope.mensajes.push({type:'warning',text:'Los campos año, mes y tipo de red son obligatorios'});
				return;
			}
			CrubSrv.guardar_prima(form).success(function(data,status){
				if(status!=200){
					$scope.mensajes.push({type:'danger',text:'algo va mal :('});
				}else if(data.meta.code==-1){
					$scope.mensajes.push({type:'warning',text:'No se guardo la distribución! ' + data.meta.msg});
				}else{
					$scope.limpiar();
					$scope.mensajes.push({text:'Actualizacion exitosa'});
				}
				$scope.atras();
				$timeout(function(){
					$scope.mensajes = [];
				},8000);
			});
		}
	}])
	.controller('DistribucionOtrasRedesVentasCtrl', ['$scope', 'CrubSrv',"$timeout","$modal","$upload",function ($scope,CrubSrv,$timeout,$modal,$upload) {
		$scope.mensajes = [];
		$scope.form = {};
		$scope.limpiar = function(){
			$scope.form = {};
			// $scope.rubros = [];
		}
		$scope.eliminar = function(id_venta){
			CrubSrv.eliminar_venta_otras_redes({id_venta:id_venta}).success(function(data,status){
				if(status!=200){
					$scope.mensajes.push({type:'danger',text:'algo va mal :('});
				}if(data.meta.code==-1){
					$scope.mensajes.push({type:'warning',text:'No se pudo eliminar'});
				}else{
					$scope.mensajes.push({text:'Listo!'});
					window.location.reload();
				}
			})
		}
		$scope.atras = function(){
			window.history.back();
		}
		$scope.guardar = function (form){
			if(angular.isUndefined( $scope.form.ano ) || angular.isUndefined( $scope.form.mes ) ){
				$scope.mensajes.push({type:'warning',text:'Los campos año y mes son obligatorios'});
				return;
			}
			CrubSrv.guardar_ventas(form).success(function(data,status){
				if(status!=200){
					$scope.mensajes.push({type:'danger',text:'algo va mal :('});
				}else if(data.meta.code==-1){
					$scope.mensajes.push({type:'warning',text:'No se guardo la distribución! ' + data.meta.msg});
				}else{
					$scope.limpiar();
					if(angular.isDefined(form.id_venta)){
						$scope.mensajes.push({text:'Actualización exitosa'});

						$scope.atras();
					}else{
						$scope.mensajes.push({text:'Registro exitoso'});
					}
				}
				$timeout(function(){
					$scope.mensajes = [];
				},8000);
			});
		}
	}])
	.directive('toneladas', [function () {
			return {
				priority: 0,
				restrict: 'C',
				link: function postLink(scope, iElement, iAttrs) {
			 // $(".toneladas")
					iElement.inputmask("decimal",{
						 radixPoint:",", 
						 groupSeparator: ".", 
						 // digits: 2,
						 autoGroup: true,
				 });
				}
			};
	}])
	.directive('dataTable', ["$timeout",function ($timeout) {
		return {
			restrict: 'C',
			link: function (scope, iElement, iAttrs) {
				$timeout(function() {
					iElement.dataTable({
						"scrollX":true,
						"language":{
							"sProcessing": "Procesando...",
							"sLengthMenu": "Ver listado de _MENU_",
							"sZeroRecords": "No hay registros seleccionados",
							"sInfo": "_START_ al _END_ de _TOTAL_ registros",
							"sInfoEmpty": "0 al 0 de 0 registros",
							"sInfoFiltered": "(filtrado de _MAX_ registros)",
							"sInfoPostFix": "",
							"sSearch": "Filtrar : ",
							"sUrl": "",
							"oPaginate": {
								"sFirst":    "Primero",
								"sPrevious": "Anterior",
								"sNext":     "Siguiente",
								"sLast":     "\u00daltimo"
							}
						},
					});
				});	
			}
		};
	}])
})(angular,window.prefixDomain);





