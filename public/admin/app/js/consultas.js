(function(angular,prefix){
	angular.module('#app.consultas', ["ui.bootstrap","chart.js","ngRoute"])
		// Optional configuration
	.config(['ChartJsProvider', function (ChartJsProvider) {
		// Configure all charts
		ChartJsProvider.setOptions({
			colours: ['#FF5252', 'blue'],
			responsive: true
		});
		// Configure all line charts
		ChartJsProvider.setOptions('Line', {
			datasetFill: true
		});
	}])
	.config(['$routeProvider', function ($routeProvider) {
		$routeProvider.otherwise("/");
		$routeProvider
			.when('/', {
				templateUrl: prefix + 'consultas/reportes/seleccion',
			})
			.when('/plan_comercializacion/:ano', {
				templateUrl: prefix + 'consultas/reportes/plan_comercializacion',
				controller: 'Plan_ComercializacionCtrl'
			}).when('/plan_comercializacion/:ano/estado_x_mes', {
				templateUrl: prefix + 'consultas/reportes/plan_comercializacion_estado_y_mes',
				controller: 'Plan_ComercializacionEstadoMesCtrl'
			}).when('/distribucion_productos_terminados/:ano', {
				templateUrl: prefix + 'consultas/reportes/distribucion_productos_terminados',
				controller: 'Distribucion_productos_terminadosCtrl'
			}).when('/distribucion_materia_prima/:ano', {
				templateUrl: prefix + 'consultas/reportes/distribucion_materia_prima',
				controller: 'Distribucion_materia_primaCtrl'
			}).when('/distribucion_total_otras_redes/:ano', {
				templateUrl: prefix + 'consultas/reportes/distribucion_mercancia_arribada',
				controller: 'Distribucion_mercancia_arribadaCtrl'
			})
	}]).controller('ResportesCtrl', ['$scope','$location', function ($scope,$location) {
		$scope.reportes = [
			{"id_reporte":1,"des_reporte":"Plan Comercializacion Mercal"},
			{"id_reporte":2,"des_reporte":"Plan Comercializacion Mercal Estado x Mes"},
			{"id_reporte":3,"des_reporte":"Producto Terminados"},
			{"id_reporte":4,"des_reporte":"Materia Prima"},
			{"id_reporte":5,"des_reporte":"Distribución total otras redes publicas y privadas"},
		];
		$scope.Ir = function(form){
			var uri = "/";
			switch(form.id_reporte){
				case 1: uri = "plan_comercializacion/" + form.ano; break;
				case 2: uri = "plan_comercializacion/" + form.ano + "/estado_x_mes"; break;
				case 3: uri = "distribucion_productos_terminados/" + form.ano; break;
				case 4: uri = "distribucion_materia_prima/" + form.ano; break;
				case 5: uri = "distribucion_total_otras_redes/" + form.ano; break;
			}
			$location.path(uri);
		}		
		$scope.atras = function(){
			window.location.back();
		}

	}]).controller('Plan_ComercializacionCtrl', ['$scope','$routeParams', "$timeout","Plan_Comercializacion",function ($scope,$routeParams,$timeout,Plan_ComercializacioSrv) {
		$scope.grafico = {
			labels:[],
			data:[],
			series:["Planificadas","Distribuidas"],

		};
		Plan_ComercializacioSrv.getDatos({ano:$routeParams.ano}).success(function (res){
			if(res.data.lista.length==0){
				$scope.atras();
			}
			$scope.rubros = res.data.rubros;
			$scope.meses  = res.data.meses;
			$scope.lista  = res.data.lista;
			
			$scope.grafico.labels = jQuery.map(res.data.meses,function(obj){
				return obj.corto;
			});
			var planificacion = {};
			jQuery.map(res.data.plan,function(plan){
				planificacion[plan.id_mes] = plan.total;
			});

			$scope.grafico.data[0] = jQuery.map(res.data.meses,function(mes){
				console.info(planificacion,planificacion[mes.id_mes]);
				var res = 0;
				if(planificacion[mes.id_mes]){
					res = planificacion[mes.id_mes];
				}
				return res;
			});
			$scope.grafico.data[1] = jQuery.map(res.data.lista,function(lista){
				return $scope.sumaTotalCol(lista);
			});
		});
		$scope.sumaTotalCol = function(lista){
			var total = 0;
			angular.forEach(lista,function(rubro){
				total += parseInt(rubro.total);
			});
			return total;
		}

		$scope.exportar = function(){
			var pdf = new jsPDF('p','pt','letter');

			var tabla = pdf.autoTableHtmlToJson($(".table").get(0));
			var tabla2 = pdf.autoTableHtmlToJson($(".table").get(1));
			
			var header = function (data) {
				console.info('info',data);
				pdf.setFontSize(20);
				pdf.setTextColor(40);
				pdf.setFontStyle('normal');
				// pdf.addImage(headerImgData, 'JPEG', data.settings.margin.left, 40, 25, 25);
				pdf.addImage(img_mercal_small, 'PNG', data.settings.margin.left, 25,75,50);
				pdf.text("Proyecto: Plan de Comercialización de Alimentos ", data.settings.margin.left + 80, 60);
				pdf.setFontSize(10);
				pdf.text("Distribución de Alimentos a la Red Mercal ", data.settings.margin.left + 330, 80);
			};

			var options = {
				beforePageContent: header,
				// afterPageContent: footer,
				margin: {top: 90},
				// {startY: 60}
			};
			pdf.autoTable(tabla.columns, tabla.data, options);
			
			var res = html2canvas($("#lineWrap"), {
				onrendered: function(canvas) {
					var width  = (canvas.width * 70) / 100, // DPI
					height = (canvas.height * 70) / 100; // DPI
					console.info(width,height);
					// pdf.addImage(canvas, 'PNG', 0, pdf.autoTableEndPosY()+20 ,canvas.width,canvas.height);
					pdf.addImage({
						imageData:canvas,
						format:'PNG',
						x:10,y:pdf.autoTableEndPosY()+2,
						w:width, h:height,
						compression:"NONE"
					});
					var yTabla2 = pdf.autoTableEndPosY() + 2 + height + 2;
					pdf.autoTable(tabla2.columns, tabla2.data,{startY:yTabla2});

					pdf.save("file.pdf");
				}
			});
		}
	}]).controller('Plan_ComercializacionEstadoMesCtrl', ['$scope','$routeParams', "$timeout","Plan_Comercializacion",function ($scope,$routeParams,$timeout,Plan_ComercializacioSrv) {
		Plan_ComercializacioSrv.getDatosEstadoMes({ano:$routeParams.ano}).success(function (res){
			if(res.data.lista.length==0){
				$scope.atras();
			}
			$scope.meses   = res.data.meses;
			$scope.lista   = res.data.lista;
			$scope.estados = res.data.estados;
		});
		$scope.sumaTotalCol = function(lista){
			var total = 0;
			angular.forEach(lista,function(item){
				total += parseInt(item.total);
			});
			return total;
		}
		$scope.exportar = function(){
			var pdf = new jsPDF('p','pt','letter');
			var tabla = pdf.autoTableHtmlToJson($(".table").get(0));
			var header = function (data) {
				
				pdf.setFontSize(20);
				pdf.setTextColor(40);
				pdf.setFontStyle('normal');
				// pdf.addImage(headerImgData, 'JPEG', data.settings.margin.left, 40, 25, 25);
				pdf.addImage(img_mercal_small, 'PNG', data.settings.margin.left, 25,75,50);
				pdf.text("Proyecto: Plan de Comercialización de Alimentos ", data.settings.margin.left + 80, 60);
				pdf.setFontSize(10);
				pdf.text("Distribución de Alimentos a la Red Mercal ", data.settings.margin.left + 330, 80);
			};

			var options = {
				beforePageContent: header,
				// afterPageContent: footer,
				margin: {top: 90},
				// {startY: 60}
			};
			pdf.autoTable(tabla.columns, tabla.data, options);
			
			pdf.save("reporte.pdf");
		}
	}]).controller('Distribucion_mercancia_arribadaCtrl', ['$scope','$routeParams', "$timeout","Plan_Comercializacion",function ($scope,$routeParams,$timeout,Plan_ComercializacioSrv) {
		$scope.grafico = {
			labels:[],
			data:[],
			series:["Distribuidas"],
		};
		$scope.bar = {
			labels : [],
			data   : [],
			series:["Distribuidas"],
		};
		
		$scope.pie = {
			labels : ["Producto Terminado", "Materia Prima"],
			data   : [5,5],
		};


		Plan_ComercializacioSrv.getDatosMercancia_arribada({ano:$routeParams.ano}).success(function (res){
			if(res.data.lista.length==0){
				$scope.atras();
			}
			$scope.rubros = res.data.rubros;
			$scope.meses  = res.data.meses;
			$scope.lista  = res.data.lista;

			$scope.pie.data = [res.data.area.terminado, res.data.area.prima];
			
			$scope.bar.labels = jQuery.map(res.data.meses,function(obj){
				return obj.corto;
			});
			
			$scope.bar.data[0] = jQuery.map(res.data.meses,function(m){
				return $scope.sumaTotalCol($scope.lista[m.id_mes]);
			});
		});
		$scope.sumaTotalCol = function(lista){
			var total = 0;
			angular.forEach(lista,function(rubro){
				total += parseInt(rubro.total);
			});
			return total;
		}

		$scope.exportar = function(){
			var pdf = new jsPDF('p','pt','letter');

			var tabla = pdf.autoTableHtmlToJson($(".table").get(0));
			
			var header = function (data) {
				console.info('info',data);
				pdf.setFontSize(20);
				pdf.setTextColor(40);
				pdf.setFontStyle('normal');
				// pdf.addImage(headerImgData, 'JPEG', data.settings.margin.left, 40, 25, 25);
				pdf.addImage(img_mercal_small, 'PNG', data.settings.margin.left, 25,75,50);
				pdf.text("Proyecto: Plan de Comercialización de Alimentos ", data.settings.margin.left + 80, 60);
				pdf.setFontSize(10);
				pdf.text("Distribución total otras redes publicas y privadas ", data.settings.margin.left +200, 80);
			};

			var options = {
				beforePageContent: header,
				// afterPageContent: footer,
				margin: {top: 90},
				// {startY: 60}
			};
			pdf.autoTable(tabla.columns, tabla.data, options);
			pdf.addPage();
			pdf.text("Mercancía Arribada ", 200, 50);
			
			var res = html2canvas($("#lineWrap"), {
				onrendered: function(canvas) {
					var width  = (canvas.width * 70) / 100, // DPI
					height = (canvas.height * 70) / 100; // DPI
					// pdf.addImage(canvas, 'PNG', 0, pdf.autoTableEndPosY()+20 ,canvas.width,canvas.height);
					
					pdf.addImage({
						imageData:canvas,
						format:'PNG',
						x:10,y:30,
						w:width, h:height,
						compression:"NONE"
					});
					// html2canvas($("#lineWrap"), {
					// 	onrendered: function(canvas) {
					// 		var width  = (canvas.width * 70) / 100, // DPI
					// 		height = (canvas.height * 70) / 100; // DPI
					// 		console.info(width,height);
					// 		console.info(pdf.autoTableEndPosY());

					// 		pdf.addPage();
					// 		pdf.addImage({
					// 			imageData:canvas,
					// 			format:'PNG',
					// 			x:10,y:25,
					// 			w:width, h:height,
					// 			compression:"NONE"
					// 		});

					// 		pdf.save("file.pdf");
					// 		}
					// });

					pdf.save("file.pdf");
				}
			});
		}
	}]).controller('Distribucion_productos_terminadosCtrl', ['$scope','$routeParams', "$timeout","Plan_Comercializacion",function ($scope,$routeParams,$timeout,Plan_ComercializacioSrv) {
		$scope.grafico = {
			labels:[],
			data:[],
			series:["Distribuidas"],
		};
		Plan_ComercializacioSrv.getDatosProductosTerminados({ano:$routeParams.ano}).success(function (res){
			if(res.data.lista.length==0){
				$scope.atras();
			}
			$scope.rubros = res.data.rubros;
			$scope.meses  = res.data.meses;
			$scope.lista  = res.data.lista;
			
			$scope.grafico.labels = jQuery.map(res.data.meses,function(obj){
				return obj.corto;
			});
			
			$scope.grafico.data[0] = jQuery.map(res.data.meses,function(m){
				return $scope.sumaTotalCol($scope.lista[m.id_mes]);
			});
		});
		$scope.sumaTotalCol = function(lista){
			var total = 0;
			angular.forEach(lista,function(rubro){
				total += parseInt(rubro.total);
			});
			return total;
		}

		$scope.exportar = function(){
			var pdf = new jsPDF('p','pt','letter');

			var tabla = pdf.autoTableHtmlToJson($(".table").get(0));
			
			var header = function (data) {
				console.info('info',data);
				pdf.setFontSize(20);
				pdf.setTextColor(40);
				pdf.setFontStyle('normal');
				// pdf.addImage(headerImgData, 'JPEG', data.settings.margin.left, 40, 25, 25);
				pdf.addImage(img_mercal_small, 'PNG', data.settings.margin.left, 25,75,50);
				pdf.text("Proyecto: Plan de Comercialización de Alimentos ", data.settings.margin.left + 80, 60);
				pdf.setFontSize(10);
				pdf.text("Distribución de Productos Terminados ", data.settings.margin.left + 330, 80);
			};

			var options = {
				beforePageContent: header,
				// afterPageContent: footer,
				margin: {top: 90},
				// {startY: 60}
			};
			pdf.autoTable(tabla.columns, tabla.data, options);
			
			var res = html2canvas($("#lineWrap"), {
				onrendered: function(canvas) {
					var width  = (canvas.width * 70) / 100, // DPI
					height = (canvas.height * 70) / 100; // DPI
					console.info(width,height);
					console.info(pdf.autoTableEndPosY());
					// pdf.addImage(canvas, 'PNG', 0, pdf.autoTableEndPosY()+20 ,canvas.width,canvas.height);
					pdf.addPage();
					pdf.addImage({
						imageData:canvas,
						format:'PNG',
						x:10,y:25,
						w:width, h:height,
						compression:"NONE"
					});

					pdf.save("file.pdf");
				}
			});
		}
	}]).controller('Distribucion_materia_primaCtrl', ['$scope','$routeParams', "$timeout","Plan_Comercializacion",function ($scope,$routeParams,$timeout,Plan_ComercializacioSrv) {
		$scope.grafico = {
			labels:[],
			data:[],
			series:["Distribuidas"],
		};
		Plan_ComercializacioSrv.getDatosMareiaPrima({ano:$routeParams.ano}).success(function (res){
			if(res.data.lista.length==0){
				$scope.atras();
			}
			$scope.rubros = res.data.rubros;
			$scope.meses  = res.data.meses;
			$scope.lista  = res.data.lista;
			
			$scope.grafico.labels = jQuery.map(res.data.meses,function(obj){
				return obj.corto;
			});
			// 
			$scope.grafico.data[0] = jQuery.map(res.data.meses,function(m){
				return $scope.sumaTotalCol($scope.lista[m.id_mes]);
			});
		});
		$scope.sumaTotalCol = function(lista){
			var total = 0;
			angular.forEach(lista,function(rubro){
				total += parseInt(rubro.total);
			});
			return total;
		}

		$scope.exportar = function(){
			var pdf = new jsPDF('p','pt','letter');

			var tabla = pdf.autoTableHtmlToJson($(".table").get(0));
			
			var header = function (data) {
				console.info('info',data);
				pdf.setFontSize(20);
				pdf.setTextColor(40);
				pdf.setFontStyle('normal');
				// pdf.addImage(headerImgData, 'JPEG', data.settings.margin.left, 40, 25, 25);
				pdf.addImage(img_mercal_small, 'PNG', data.settings.margin.left, 25,75,50);
				pdf.text("Proyecto: Plan de Comercialización de Alimentos ", data.settings.margin.left + 80, 60);
				pdf.setFontSize(10);
				pdf.text("Materia Prima: Redes Públicas y Privadas  ", data.settings.margin.left + 330, 80);
			};

			var options = {
				beforePageContent: header,
				// afterPageContent: footer,
				margin: {top: 90},
				// {startY: 60}
			};
			pdf.autoTable(tabla.columns, tabla.data, options);
			
			var res = html2canvas($("#lineWrap"), {
				onrendered: function(canvas) {
					var width  = (canvas.width * 70) / 100, // DPI
					height = (canvas.height * 70) / 100; // DPI
					console.info(width,height);
					console.info(pdf.autoTableEndPosY());
					// pdf.addImage(canvas, 'PNG', 0, pdf.autoTableEndPosY()+20 ,canvas.width,canvas.height);
					// pdf.addPage();
					pdf.addImage({
						imageData:canvas,
						format:'PNG',
						x:10,y:pdf.autoTableEndPosY(),
						w:width, h:height,
						compression:"NONE"
					});

					pdf.save("file.pdf");
				}
			});
		}
	}])
	.service('Plan_Comercializacion', ["$http",function ($http) {
		this.getDatos = function(data){
			return $http.post(prefix + "distribucion/c_list_dist_mercal_estado_y_producto/"+data.ano);
		}
		this.getDatosProductosTerminados = function(data){
			return $http.post(prefix + "distribucion/c_list_dist_producto_terminado/"+data.ano);
		}
		this.getDatosMareiaPrima = function(data){
			return $http.post(prefix + "distribucion/c_list_dist_materia_prima/"+data.ano);
		}
		this.getDatosEstadoMes = function(data){
			return $http.post(prefix + "distribucion/c_list_dist_mercal_estado_y_mes/"+data.ano);
		}
		this.getDatosMercancia_arribada = function(data){
			return $http.post(prefix + "distribucion/c_list_dist_mercancia_arribada/"+data.ano);
		}
	}])
	.controller("LineCtrl", ['$scope', '$timeout', "Resporte",function ($scope, $timeout,Resporte) {
			$scope.labels = ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"];
			$scope.series = ['Planificadas', 'Distribuidas'];
			$scope.data = [
				[65, 59, 80, 81, 56, 55, 40, 40, 40, 40, 40, 40],
				[28, 48, 40, 19, 86, 27, 86, 27, 86, 27, 86, 27]
			];
			$scope.onClick = function (points, evt) {
				console.log(points, evt);
			};

			// Simulate async data update
			$timeout(function () {
				$scope.data = [
					[28, 48, 40, 19, 86, 27, 86, 27, 86, 27, 86, 27],
					[65, 59, 80, 81, 56, 55, 40, 40, 40, 40, 40, 40],
				];
			}, 3000);
			$scope.abrir = function(){
				Resporte.open();
			}
	}])
	.service('Resporte', [function () {
		this.open = function(){
			$("line").css("background-color", "gray");
			var line = document.getElementById("line");
			var image = line.toDataURL("image/png"),
				width  = (line.width * 25.4) / 200, // DPI
				height = (line.height * 25.4) / 200; // DPI
			var pdf = new jsPDF();
			pdf.setFontSize(40);
			pdf.addImage(image, 'PNG',35, 10, width,height);
			pdf.text(35, height+25, 'Pie chart');
			// pdf.save("line.pdf");
			var string = pdf.output('datauri');
			return;
			var string = pdf.output('datauristring');
			$("iframe").attr("src", string);
		}
	}])

})(angular,window.prefixDomain,window);
// panel
var open = function(){	  
	var pdf = new jsPDF('p','pt','letter');

	var tabla = pdf.autoTableHtmlToJson($(".table").get(0));
	
	var header = function (data) {
		console.info('info',data);
		pdf.setFontSize(20);
		pdf.setTextColor(40);
		pdf.setFontStyle('normal');
		// pdf.addImage(headerImgData, 'JPEG', data.settings.margin.left, 40, 25, 25);
		pdf.addImage(img_mercal_small, 'PNG', data.settings.margin.left, 25,75,50);
		pdf.text("Proyecto: Plan de Comercialización de Alimentos ", data.settings.margin.left + 80, 60);
	};

	var options = {
		beforePageContent: header,
		// afterPageContent: footer,
		margin: {top: 80},
		// {startY: 60}
	};
	pdf.autoTable(tabla.columns, tabla.data, options);
	
	var res = html2canvas($("#lineWrap"), {
		onrendered: function(canvas) {
			var width  = (canvas.width * 70) / 100, // DPI
			height = (canvas.height * 70) / 100; // DPI
			console.info(width,height);
			// pdf.addImage(canvas, 'PNG', 0, pdf.autoTableEndPosY()+20 ,canvas.width,canvas.height);
			pdf.addImage({
				imageData:canvas,
				format:'PNG',
				x:0,y:pdf.autoTableEndPosY()+20,
				w:width, h:height,
				compression:"NONE"
			});
			pdf.save("file.pdf");
		}
	});
}
var open_add_html = function(){	  
	var pdf = new jsPDF('p','pt','letter');

	pdf.addHTML($('.panel-body').get(0),function() {
		pdf.text(35, 100, 'Pie chart');
		// $('.preview-pane').attr('src', string);
		var string = pdf.save("file.pdf");
		// var string = pdf.output('save',"file.pdf");
	});
}
var open_funciona = function(){	      
	$("line").css("background-color", "gray");
	var line = document.getElementById("line");
	var image = line.toDataURL("image/png"),
		width  = (line.width * 25.4) / 100, // DPI
		height = (line.height * 25.4) / 100; // DPI
	var pdf = new jsPDF();


	pdf.addImage(image, 'PNG', 35, 10, width,height);
	pdf.fromHTML($('#tabla').get(0), 35, height+15, {
		'width': 150, 
	});
	// pdf.setFontSize(40);
	// pdf.text(35, height+25, 'Pie chart');
	pdf.save("line.pdf");
	// var string = pdf.output('datauri');
	return;
	var string = pdf.output('datauristring');
	$("iframe").attr("src", string);

}


function convertToDataURLviaCanvas(url, callback, outputFormat){
	var img = new Image();
	img.crossOrigin = 'Anonymous';
	img.onload = function(){
		var canvas = document.createElement('CANVAS');
		var ctx = canvas.getContext('2d');
		var dataURL;
		canvas.height = this.height;
		canvas.width = this.width;
		ctx.drawImage(this, 0, 0);
		dataURL = canvas.toDataURL(outputFormat);
		callback(dataURL);
		canvas = null; 
	};
	img.src = url;
}

var img_mercal_small = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIwAAABdCAYAAAB3oo60AAAgAElEQVR4nO2dd3RVV5an94TVvdZMz5o1NV017XJ3lT1Vq7uqy6FcNhgEimQRJBQINhiwARGNwYATmGAwxoBNMAaMbbDJGGMyJttksMkimiQhCUko6ym99M0f55z77nt6T4FkT6Oz1l6I++7d94Tf2Xufvfc5V/j/oHi84PVWv+72QJDLDeUeFvm5K1BbMUApr/KybbebhYs8rFjjIi1d/eBpAM19Lb9owHg0En665qaLuEkUSNYULzBjuhuv14vXe3ug8ep3eDxait3Nyv8HLb9YwBjJUloKSeIkWSCyxQ+8sGQsLd5eTJy4SBQYOdwFeCxw1aV4vOByVVdzXq9Scw0ldPnFAsblUv9+MN1NskDLYWu5huDQ9EX647SWTJIEPp7nBmof7EBAuD1e0jLcHEt189N1Ny79Y33A96CVXyRgzMx3upwkCcSKg+/KFVCuI6Rr0CxL/yMdpJx4gQtXtU0TYrA9NrDcyHbz/vsuugp0FkjU/3YRF1t2ugBvA2hClF8kYDx6YE+eV3ZL9MiVODRQsjRd16Dpt3okyQKvDlIPBZMybo8X8FJV5WHGLCdx4iVZIEGghaQR3u4AMXKVJIE48bB2Y2heD3r5RQLGqKM1610kCwzfNKQaYDIR8hBSEaLkIgkCaVlKNdmlgxn0azecdHuqgiRtMLcZt5TFVxtzGiEN4SLCK5sGEy9ekgQqnYpXsOX8g1x+0YCZv8BDssAHqW1wINywASbLppp6L32DZIFPv3T5Pa/A4uHEGTfx2nCOCTvBlzf+Sol+Nl/zytP/bznmc5IEDh9z2ng0FFN+0YCZ+SEkCXyW3iQoYDL0IK/J+xXx4qZfewAvHo9voE9dcBMnVSQJtHv7S37Sz2RqfpmaV5q+Pnpnb5IFNmzTgHH9LF3wiy2/aMB8vEABZsHVpkEBk4mQi3AZofnDx0kUcFS4LT7ZeW46SyVJAonT55GvJUl6AB+7tBq5/SWSBHburQIaJExg+UUDZtVaD0kCU36Ir2bDBEqZmNe/IEHgp6tOzcXLoG7KBmrz1nLyEbL1/YE87IBpP306iQJXMxVgnM7gPpsHtfzsgPF6bd5WTQYwh46pZfWAr0aFBIwZ6J5Lx5Ak8P1hBZivN6hno/50lnPaVgmUUHZJlYPwE0JzOc1zAmB0kdeqZ4O0+ZkAY0ARataa6zkFThIEooerZXUw6WAAM0yrks073IDHCiV8fKFZSLAF8pjyY2sS9Sqqi0B3gaG93Xy50kNhsceq+4Nc7itgjARRRflGHOUert5wc+qsh2On3Zw57+Z6poeyCg/g4sUnIUaucVHbK5khBvu1vd1JFti938PJc0q6RKesp6AGNWRfnp9CiJYzJAp0kAo6SiWdddwqSaCzeNi8vQE09w0wHo9Pclz4ycv0mU56h3tI0A60BO1xtf89uIeLntFVJArMSY2scWk95NsUkgVOnnPy4QwVTnj3x9C2j90GKkb4NPMRBmzux+LMP7OuRNhULizL+R1j93cnPGWjJXl27leNeFDV030BjJqRXi5fczO4h8tv5sZKCZFygbA2BwhL3ENYy8NEyAXaSxlJNvC0fHUZJUEAc0MDJmHeZBIF0rKq6NcS2kkh33uEolokjAFNDr44VSCVILyy/mUSBXo8qiTjg2oE33PAGPG9e5+TOFEzv43k0vGD2UxPjWKHSziH8oPcQLiGcAZhW4Uw5UQsbSZ+QqyUkiSw/PpfcOh77YOdi/BswmaSBNxU0U2guZzhUgg1Fko1pes6ZNgoDbW6KkYI7/Q9CaKW6/BgrpzuKWCMi/5qmoc4bYR2mLKAg7bZW6wHNdtGefq6md27PEL0wK9pIee4ilCgB9LEkzaXCJ2knNH9AVx0Ewj7/XGuaMlRF8DUREbttRn9uQJMvlqJNQDmLhfjJR37hnLLd58/FQdCoZ7JZjZnBqEM24wv0gPW9q1FxHTdyVUb4PIQYgZ/TaLAqfNOwMOAttBacjiiwXUngDFL7usIkXKO7n5L7gev3FPAqBnooYdAa8nlpB7AmozQUDO8AOE4QnspIer/nGX4tud573hrmvdYR7LARwuqAKX/lqxSMaiJB5JrNXprA4sJGQzfNJhkgUXLFVge1JDBfVBJHnr9DqLkCpeo2dta08BloyLKEY+e9VtJJQms/kq/Twcby6oqVIaeXOCCllD1BY2Rfg6EeWejiZNK+oQBaPvlXnbcL7jcF5Vkos7v7O9uOeDS66AqTIDQ2BCzTsaSLDBzKjg4z4m8T7jFcMrpSyEjqGC3FaHeulv5YtoMWssNlC2UVgtYzfuMGixBeGP3i8SLk+4CeSWqQQ+i7WLKfVBJUOFy8rx46SRlTDzS3S+1wA4KO2Xp30v1vfPPtyRWCkmSKm7QlxJ8qQnXbOC7RSIuSgH4ZEkVyQJRXbZZhnYRws0g77yBMr6NbfRtqRA18BuSBVI6eihyKLA86Jl493xZrUDj5Vahm96NlAqJ7LKVD0/EchrfaiiY7+MiwsIrzYl4ZZnaMfCrQn5wP2StkhxpAvMFXhOYKuS5DGiewumtALys2VSp/T15PPflWL5zKaAFe+cVhEU/hRE5+gs6STlJAl+s8ABuPN6GtE24T447rxe8Xi8ur4dlX7lUuoFAa7lJo/ittJw8h66LR9Jn1VC6LnmVdtNm0KjnWqLlsmWrzF3g4hZPUqilCSMFpDo5soy7v6u2adyk33QxvA/a7e+gccs9xE79kD6rX2HAN8NIXvg2YUOWEyUXSdTvmz7DS0GJL8XhQVZD9nLfQgNer8/j6/K4OHrCzXuT4KUnsfJr7YZs77+Hia/B9/uhqgrKiCdbqw9GBAeLoXyPkjQlfK5DEiogmZHjZsF86NfIl/htqLvAm0Nh9/deKpxOjDf3QQ0BhCr3NfjoRRnCXq8XswQGL26uUcRGMplBJiMo4AXK6KKpN/mEka4NVmbXDBZEIE7ZJDkITm6qfUgel66BVjE4KatyUVru0r+5bXVqyIEJVX6G9AYP4MXtgWLWkUk4N0PYFMYxZ1Yw3g11AIsmzzYFmkJe1u9VdojL7da7CPyLV29ua1A/NZf7DBi94YxCbpFkLXdzEHZ4hNkXWvDqjpfovfINun05ng4z5jLuYAIFCI7UuoPF0E2U/6aAsZRyEC+Vuh5ePLi1bdUAkPqU+wgYBRYXaWTx3yypMudiJM16b6KtFPmlOCQJtJUMNlcJ5VX1BwsieLYrNVaoV2OZ/AMFfISbYludGtBSn3KfAGPAcsVKJchHeP6LMX47D+MEOomHTuKhgzhYmfEIDgQeuj3AEK/eszbnYTY5FGhKtIorZa2umwe77dJQai73ATAGLNfIwheZbjN1NokCceKlk3iIswDjJVHgja09FFh63CZYzDIbYeSuXsSJh9YTFvL1rYdx6Drk0AuvdvI1gKZu5R4DRm85JZss7WHNQWgzYT6JWprEKUFAvP5/okD7cfMoQWDhnYGFf1WA+fB4a8u/0lEqSPh4ChcxyVW/w0maX30bSuhyDwGjUxkpJoeHyNIDFDt1pgWWeBtY4rTtEvkvJziNUHX5NkHyicAywbtCeX4dCPHTZ2qwVBGvgRPe5Chbi8SKbbnI8Kt3Qwle7hFgvBblEmVFfZ+f/64Gi7saWOK1evrs2uNKFd2mVHF/q5bgJlb1xu7udBK3ZSMZOylBVObfyhtPaRUVY6t7QwlV7hFglN2ST18LLEPXDrJsFjtY4m12S/IHUxRYRtUAiuECb9Xw+2xl1A5YPJzwiKPVQGlXfwkCMXKFo9rf4zOE3aGb9oCXewAYFdUt4l3ffp9DnUmwzfBgqqilXOcQgvtKDWBIUZHprJok0IfKToppvJ8kLc0C32moo04bfWHROBwIWbTVbWiwZUKVuwwYNTNLWW1lqi1Pe8w69CfYwBlDt/+KYUq6hIUAQpJSNcUmRBAZ4r5FKpId0X4PCVp6BQOLn930+CkuaYPcRY5uS4NqClbuImDUrKziJDf0wO53CS0lzZIuwQass6gtId8ieK6HAMHzajAzED48Hq4A81GIe48IJxBi5FLI9wa+v4NUsLFUdJ7O97o9DWopWLlLgDErokJual/LVYSIjt/rWe4JOmCdxKtm+AvryUPg7eA2S5aWVkkzPqKN3OQ0QmlpcMBUIGxyCu3FYRm6oQBjlzJzLobrBKuluk0NgAlW7hJglHTJo60VH4qdND/o8jmYOnp5fd/gK6NZSrLcQoibPscKG0zZF6fuHxhwf4wC1ri9nbSBXTNY7Ab35KOJemU1R7fpAc3yrqXcBcAYI3ecZbe8vD4l5IoocNXSSVwsK/+feG/5D375duW3uY7QauRy7UdxKYnUdh+ZGkh+Xt3TShXGDF1W6/sDATPpcLLeAjPDr10Nxb/cIWBUp1aw2QLLpz89ZflZarMfEgRayQ0OI1Rc0wPfTMjNV7wOO4XIzt/5SSozwCPWp+BAuFUg0EdwbFbPLE17pE7vt9cjUWDKqQ44EAqY7de2huJf7gAwSg25SCNde3EPe4UYnVZZF9vBLG1X3fyNGnx8m9Y+vfQMLeV6NbVmluYdpZIPjrW1ErsdCIcQIp84Uaf326mzwKdpT2heS3T7GmyYYOUOAKMM3RwaW3knUf3W1mq3BF3WPn2Cz9P/TCrCznKhy+z3iBN3yGWxWd10Ejexk+YxcV8nBqwZVmewBvJpL2Vs1dHsUrbp9jUAJli5TcCozixgjHV6wgtLRvmBpT6DZgY5Ui7STgqtozVqU2nxWp0YUiCqu2Qx746RK5zU3t5Kzuo2NjjvgpXbAIwCSznbLbvl4wuNqw1kQj0HzqiGBKFOy2G70arUlLdeaihefOGBiN6byNWOO19y1Z067nz5w9XJUw/+6mAlRcF4BfKtjbfbxq+u5JO29QSM8bfkkoba7/wDYm0HMfksCQKt351FK8mosz1jjNlmbfcRMXJ5ve2Q2yGzrO/+uQkNNLN1qun4+gDHqzu4rs+YwQt1f33ff+9LPQFj/C2dydKAaTFiqRWBNmBpnrwLB0KnyXPr5IsxWz3aTZ5HFsLAla9WU292CpRMtYEx8BmfRPLSWWDW+SidcP6WbmfgCslLzemc5ndfcZNNJYco5yscfEYp83GwhAp24OQSWPnFofirvnawg0LGUsoHlDAHB4soZyXlfEU566lgCxXsooJ9VPEDVZzCgyNkPUtZSQmzKOVjypgbkkqZSwkfU8IcSpiPhzKgXoBRHVLMQiuoOGprH8vf4XPzF7CtRB+4nPUIncQVdHDtUqWFXGf6sVbWLsjETyZVA1rg0WbBKNQ9CUF+M3nDLeUap7R3upSVeCjFRSZubmrKweN3omagbeNL8XSRQTHTyOOvfufS2I8xMSdo5SLkEYeDz3CTbePlkypucqs9bz/46EYA30xUkloZ3/iNmS9sc56b+v03g/ANJN9BCH+x2l9HwKgXOrliGbkbioRYKbHl4qrBH7Onizpp26M8vi2HfaUH32uTKl4rl7flm4s45lU8b1YK5QjtPvblzXQWdSji+2cimXk+nOkXI5lxqY50MZJplyL54EIkMy8044PzEUxPjWba6ZZMTm3DmGNxLEh7ijxqPhggB6GY8Xgp8usP3y6IW+TxijUYZgCzEcrSBe9egfUCmwXnKaGgXMXFbtjuK2asTTIogBaQYtWrOFtgqcAmwbtd8OwVXEeFyjNC5UWh4ich32sCtB/48TH/ljDdqmNxvsAyga8F1tjoa4HlgvOgD4DlbLXaW0fAqI7JJZKbqMaGJ++ylr2ddJpAy7ELKdEzgI4Cc4VZhWF+qQ3Gbmgt2Yw7kEixXp0UnxPluLsghM37yMqQSxRIeG+OdVpVqP1LoahMU6jfSzUgcoPQLXwnWJmDhaq4qPtEnUJVzrd+QMlC8HwvarlWW8LXi4L7oG+25/Jn3OQDUMlhbtokAY/Ugd/r6t5CUvzGzUisbJ4k2/ALDKsE0hRTp19j33RYB8CYlIU5lirqt3KEJQHM0jRaLnISleXm3OR78U2EqKQdFmgSBWIGreVAhS89kom++715QvP3lln8EwRmn4xRg7u7Dp0WhPKzfWI8HQXkevH4vVB+0nfCg1lJlTLfGtQshMrb2DtlqDRD8c+nt56cj1sSji/ryKe94pFHtB47n7qs4oKfOqqN1y1LWs33w0EtgDEv+8lSRWtzfkMHKfdb+nYWmJuqPiCRWxzw8l3C+MsdSBaVxjByWx8KUAZzYVr1inqKhOg3v7b8Ki3kOif1QN3WDoLmPtFa184KRVUnFa8i3sbBGj81xszb52soy1JPMy2+eZ768TCg9hWljoqZYfWB88da+IzwSdRAN0OdAJNDa+tMlYi4PTZVpNRL8vwJOFDZcEQEvPwR4SxC7Huz2Jjr+5IIs4JXtrJMiBi10TJQ205YqJe8tzcI7r2+kzYzEdh8Z4OaoQfVeLczERhz52Chne+kLT/eQ+rHx2zjMarNjGEuz5Bt+nFozTwcmUa1jfGTLrUARt3kYLkvCr12qG0vkVpCh7faxxVdyVCi3n1IxXsKEApyBR4LXdniKqHR4E0WYKYdbYcDoeLg7Q+wny3w5zsb2IozvoHNRGDxXQCLCCU5PgloJEH5ufrzKSxS9XKSao2kk8uWNMyubeLF+iSykyt+oKsBMGZZV2Blz+1yCG2lwE8VtZdS1mb9nTJIa9oWEqnSJvm8bjOkUc+dermdzjFtF9VqpAWjhb4lbCZC2dka7n1LdVatPD/wnfxZkn13wMIYHwgNaDKQ4BPraYGU0LyM2ixjszWaxcy2jPKq4zXXpeqoOf3ruWrSpQbAmKz/waRpwLQavsryjRhVNOSbFJ/hWlun/GvdOu8GQlinwyQJtHpjsbW1tcbn+oqyb3raaIhaZppZlVmTDdTYB4LCwlrq+pLimYGoleBdAEwu/tIlE4EVwe8tuVGLcf2VBrOVCAa5NLIAw/Aanv2tzyar4FA16RICMMbQPWapopk/trEy2AxYIvuv5ZapxGt3p+MQZQdFNk0lSWD8vgQcCOXHQt/v3eA7Ey/Q8WQX7yVFoXk4D/okUZYBTag6apFddrGObYpUKrnQIRQ4hZJ8wXlYYIL+fYm/2lRHroXgNVy1Na+mCTRRtFYYpsfxYt0N/lXm/WFBwVIjYG4RRS7qnLnIP52wcnNV0tNN9lYou6Ry/90DC6K+WRQjV2gvZRyq1Oqopn1Kb4hSJ2+KMj6HC6z0dZAlXSaHeL6pv9MuAwWgkO/rpCXegDq0Z2x1r6qReObfnIDrNR3H5ieBXgrxzq5mad0GgEI+tABTdarm+pp6lbHapmm8fhQAGGPorrF8Lva4jpWdtr+T2i1YzyVfXegsQivJJqbPJopRja83n3f9bYLcmnhMEwrLhdIcofyKhFy9WfRJHVds/8t3xGsOQhG9ySPRWsUEGrgGCOWhbMElQqFLKMtQkiqksf2wWRL/Wqujpj4g1nTU21TfUjrw42K1SpgsfksBwqEqoY3kkiAqnzZRoOOkudbp2iTffcAcRWgjeYzc8pIypm9jpVCcHbCS+eru1S8ToTSrDvcuEivv2EmWn/R2kUER7wQFDI3vvI7m0IMqTlr8awN5YYXZYmxPUfVJFi9ePDjtgFHSpYRFlu2S/MF0ffJklcqMeyiVC1qHekMYZXdE/0nYgxAhl9hdpGJRjK8njz/42y+ZCPzm7tTPeUx3/PY63L/cJ91U6OEfyaczRYyjnE042EgOjf3B8vXdqWdhuehsglct3jWqo34+6RIYHsmxqc4c/mQA49X4r7J2F+4o8u3tMdHlxZfUxvW8nHsAFhH4q7DWKYQPWElOHWZFUJrtbyfU6tWsI5Vdts3UtXV7xgyW+dc8b7dpzN8hDd3boIrzPgDUZhchgiM9eGQ9MCBbwMsGMEa6fG5Jl8T3ZvkFAPssGosDte2DJ+4RYKKFZY5HGLHxOZWQfbWGe98Tyq8JzhOC84xQdVZwHRNr5WaMSeLusE6TfbwMCF0H6v68d4OvTvZBKawQa1JkIfD63etH78bqfp3b5rfUHibI8ZcwWTxCMcLOUiFWiq3AYkTCbl/DptbygtFqMIP+VoPDCRFoIyxD2JinQBuSjwiu74SSWyp9wJGmwvtFt/xncJ1sjVA01l8iGMpGyK+4TZ6NxLd3vLEPRCU1TYzboWn+gKltdVQTFZeYIOQ7xug1K6NN1sro+fmTLL9LOyni23ylpsrO1P6Ckhsq3yPwesUJoby2jmmvvs6WrhvLv9SzgV19syELDd476PjyS/4d72dvdL0z3pUnfdKmWvztTqmPSU3Q/G9Xeo31nXfs5pY/YHLoRB5qI3sLuWYlOL2+vYdvVVTbCyJsndDFd915WBte52t5vpvvq2y35Xbf4hvYGpfSmspqOlpEk2ejv01kVh2FNTgCa6XXfEtuz7Y63D9USVLnMRVf8u6q5f6n/e2P261nfqWRLuOtlZMAOLmhl1TCW3u7WimMLUcvI8+A5fnaX+D+zvc9xkrtnXWd9znEHLWBoL+qYI2nM9RABZU2t31t/hTtd2BOHXjPqZ4ikYlQXgeJG4xybLzyPAJTar6/9Lq//VOXUIyxuypuN0dnnN12ybX8MgJQzGdkoqLJ0QNVLkqMpHHArVMnETyf1v6SMwjxi0ZZx7w7XcL6gofpv3qw2uheUguPET69zuP1bODjvgZmIDWfUiVCQaltBtZFJaT4g8YMSHG+wF/rXk/Xd/6Syvyb7xVfuCCA7G4Ca9VTU50n2tRyPdMjDBnpUsQkP7eLKHUUTwHCXq9ympkzUyJf3Mp7xztaH+PMQ6jaJfBkkJesFqZlRaqvv978vzgQtpUJsVJI7NC1OBByavMMv6WN1bzbaKTN0DMSoOKMwH8Jcm8Xf8deYWDSVyANFSpPiJ8TzC5pshDc3wv8rRY+PYLbRHYHXr5HVJjDPPO6/zMWYOzhkr8JfCSUZvqkkGVrTRGlHQYLvCzK2/uGKP/WO6IWFtMEZoiStjOEqv32fN7v/JyO4qGULA2I6Sd9QUb7rsLIv55kxIYBnNb3FSCUXhT1CRBd6WKEqNHL1ZdJZr/Pt06hlaSrzPzRqyjWs7LGDh1bR3UShIyBehN/myMTwRvg6a08EiTWFOg0e0JdMzM1ECyBA25UYWGhwBcS1CjOrwrNyw6cTIQ8p0CKUH46uESqPCOwXEkmE2kPrI+djOQO/Cir/Vk72Cy1yx5/CVPBD1b65XOfvu2X4W+2j5itGa0lkx4fT2JHmfLCFqPTLI8Iq4tEnYEr0FbyrU1sCQLhfbZYmfnM0jNonAIIYzXaRwgVx9QAui4IrNMfo1irB3OlpqWicly/EFgk8KnquOwgnWRveBaiApP/Vv1j6dZsHCYwQCUz2fkZqRUMMMGAY+/4PFS+bnF+zcALxiezlvvtIAlVR/Nbfcj+fp+E0YAp5VMyUFHhiL7rg26ANxu/zMqpvTjo8NYilmc8bn32rtOMD6odqWqSuMM7fU+RfkcoysP3Mc8TKHvI3jFpmszfxk66FvB/+6f5zErEfGfpWkCn2rd5ZGk+hp994EwCuVENGQHXDD/71hEz2Oa9we43dTBpn5n4wGwG2jxjb0+6jb+9vZkB7zC87Z8qzLDxSw/ybvu3OIMCJo+h5CKkIkT+3bkat6ga4Bh1FS8Q03czb37fhdaS7bedxJ4g3k4KaZ6ynrABGwh/0U4brb8jem8kfMA6msV/TxvJpeeXI63krBwNykLdkBINrsBP8eXjO/rDHANiBtn+OR0jjots9+QE8LK77M21W5jMft81o7IyA64X6etFAXzt3t1M3ZZ8fHnHJbbfcvT/zaQ0nwIyZkFg+83zJiRQou+z31MQ0B/5uk2B7TSgqQaYbCIpQtjhEmKl1Iod1WVfstmRmFiH+wN3H4aizqJ2JE482slK/r6KMGTjyyy6EoYDYcyeXsw42ZF9CP1XvsWglW/z4ueT2VomzD7bkr6LJ9Nv+VhWZf0LRbqDRm5Nof3UuVaOTT7CnDOtmH8uGgfCBYRR2wbSb/G7jD+YTJq+JxWh28JJJM+fbNlwR9xC7IzZ9F052pIIBQjfFP4P+q8YR8rycXyV+SgOhCUZ/0a/pRMYuHwC/Va8yY82gOQjvHOwCyvS/10doIQw5WQLZcMgnEZ4Z38ChxAGrhnNpiIFhmHrhrEk7Qk2lwp9l0xkwMpxDFw+nn0I4/d34ThKqk091p7l2Y/Qb/FkXtn0Cr0XTeGzq03IRUhZPYoOUz9ij97ucw6h28J3SP7oPVJtdawGGIPY5dm/rdMRX9WB46315IQ4v/u8VhJ5dVIg7CAVrCv2zZazCPHipf2I5eSjPnYeO2ANC9IfJV68NA0/QqT8xKysR2k9eA2tJZcouUhrucklhLe2vUR7KaaFXCT6Tye4omd+9B+PER211zqMqKWk0+zfjxMnbgasehUHQov+X9NGcmgp14l57lt1bdhXRP3bGdpLEb0+eduamcO3v0AncRLxz2eIlUIOI7y06mU6iYuwsKM8+7cj7NKJZ9kIlxHayS1iR32OA+HLjCdoHLXTOidwR5nQLGoPiwtFfd121SjOIHQReGHpSN7e15UEgbCwo4T/4RSry4RouciIDQO5htCo6zcMO9iOcEmlo5TTUrIYdbgj/Ze8SUepIPqRs0RLKjkI3edNJkYu01rSaTl8CQ4teaoBxoQDZl9qqlVK/UFzt8h3jFkOBzXKjbqMkss0f3EzuzxCK8klbupcPrwURsywJX67Ip9NWcfnF/6KAyH82R+Yd+UpIhJ3MOdEW4oRmr28goNudepEhJyj+SOnOIVwHKFx6z2kI8w9E0WzFvv4qlSI1mfHXENlAn6W9hfCOuzkUKWwPuvXdP90gqUaBm5JocV7s3AgRA1axaD1g0hZP4Cei9626pijB8KBsDrrUWLkOk3jdpOHsObWH3i26zYrY2BnpRDWbQfLC4W2UkjHOTNYcYc69icAAAn3SURBVOtXJAgM2tqPETtSSJg+HQe+73JHykWiww9zEuGZnus5jrJLmrU4wkG3Amm0XOXrjH/FgRDx2HHeO9GaiCGrWZDalNMIHWZMsyRnSMB8cLpdvU6PupeAaSEZHNNiOQ/hFMLT8TtpNGQlbx/oxpOt9xA/ZyofX/sb0XKFnosmkDx7GocRogato+eK15h/KYwYuchH158grNU+DuIzqh0IEw50J2rMAsKGL2f2yXacR3g2/CAXEE4iPNt1Gylb+hHZf531ne3IQV8x5mAiHd9cTEepZOSunpZh6kAYsGkAERPVPqrh61NoN20mA7/tQ/gTJ+i1aAK9lo7hMiZRSRiwcjRtpn5Ik16b2FwobCoRGsfvtHZq7KwUwrrs5PObj/K3lnuJmjqTkd/2JEzOkbI5hbf29yRKLtBz0TheWjyWwwhPv7iOKPmJ98804ZkhS/lRAymsyY8cQdhU8GuaPnvY8q0lfTiTvsvG8NLykSQKvLjyNa7qfg+qkkxjZ52PuC9nstQGmM4CbSSfvV5lF+ShpEGT3huJGDePyPFzCRu1iKSZM5iX/hixUkKThB00jt7HNq/Q+rXVdNLZgSO29OcYQuTDF/gBZcwZadB+6lxe3dGDPmtfJnnOFH5CaBJ2mFMIxxD+1nMDz68cRqthqy3DNeKNxby2ZSDXESL6rSdZoMf8SRagFGA+wYEwestAYiZ9zJAdL9BW8mkUt5tnBq7mImJ9jS5y8Bpmnguj3fQZvLGtLzudQuOOu62zd3ZUCM267mRe+l9o3ncdMe98TLOJs2nefwN9177M+MNdiZUSGsftoknfNexCaDZ2Hq2mT6HZq5/zzKiFHNXSMazJjxxGWHPzCSKaH+OqrsMLn0yj+7SPcCC0eWceSQJthi8NbcOYj0d8cfN3PytYAqXMvIvPWKuKIwjNBq2h44IJtJAMOi8ZReeZM5l+IYLIIcv8VFLTFzcy5VwTooasYcjaYdxAaPbQaQ5U6Y9tbX+OEwhRXXfSVgqJlVJiem1WoIzZzw2EY14hLG4Xr+3vTHiHPRYgIrpu5ZXtvZhzTm0LnnMmmibh+7imf0/ZPIAWk+fhQOi/cjgd506m1/oB9Fg4zlrl5Wi6iBAtF2glN4kTD0nvz2RDudAkfof1vj2VQtNuO5h7/Umi3lpE9KR5hMt54ue/S59VrzJ8Zz/i3v/QsqEuIzydspLPsv6RKDlP42eOcBq1aGjW7Af2I+ypEJo9fIqL+pm24z/huc/GM+fiM2QgfOsQmslpjmjQlgUCJoc/kI/wnUdoI7fqdVxYXQFgDFpjHPuTV+918vptY4kdrzo+WwOm+YB1DN4ygA5SxrB9iXSavIBZV54lrP1uNpYKS9OeJBUhrPdmvkh/jCWXn6Kt3FK2x/Nb6PXpO3ya9jhtJYPFGX8k/C/HeHlrP17eMJioh1JZkfcPNGt+mCXZ/5tucycT0W0LJxHaSA7v/ZjARxeb00ay+cYhtJKfmJ0aw9v7utG05V5LzQ3e0p+mg9aw3StEyxkm/dCWvusHE/n6YnZUCMsy/52z+t5l6U8QIam8fSCZHovGEdF6H59l/56w8IOsuPUr1hf8d1bm/Ybm3Xcw++rTtJjwCV0XTqSFXGHQpiE8/+UYXvuuN5Ep69heLqzM/BP7EZ7uvpFjCO3f/oR2Usg5CzA/sqtCgSBKUum/4jW+KRXaSR6Ls39HVPQuXvlqKF/c+DMRcpbTWrpXA0wBfa11f/OEXXeslnzfbfRaZ9aFOvgn2DX1vJs48TLvfFMcCAfcQtOhq5me2pJm7fYy/UJT2r4zn89y/okOUk4ryaGzwOgj7Wg+7hOmn26JA6H5748z/mBndhQpozFRYOTGgcxKDadJ73XWzGz04joG7+lCWMRh2ksZLSSdzy83woHwytrBxImHOPHw0orXlCRZPdK6Nu67HpZEeP1gZzqKk3ZSTPOuW8lD6Ls+hXjx0kpyaCfFfHFDLaH7rHiN1hNVQn0awrMd9vDqoVii5SptpZC2UsDg7xNpNnIZM688Rct35zDqu+40e3kVbx7oRtIXb/HuyVj9JZhsYqWYdy49zVPDF3MSYdHlpwmT05zVgGnU4Tu+LdP26vH2Vvptx4kLcCBMORpHByknQaDXkjdDr5JKWWUZbUPXDdRpme7bAklcABA6SDkRfzlB1Otf0v3LsYza1ZN3T7bng0vhfHgpjCnHYhm6vQ/xc98lvNcWWsgN4m3Pt5Fc1uT+PQ6Eg1qMH9L/HtYi+FunsNOj/j2j7zuvxelhhO89qm37qoTVub8lH7UiOoTPo3oYtVvhO4+wWfMxjrcChI3FwvoiNeOyUbbQN/n/mc0l6neTpX8GYYuuT7p+9kfbte1Ote8qT7/zBD4n3T6UrbbTLWx3C9tcytg/iPIRmXb9qN9zDOU7Mby3uZT74YBXpdHeQNij63FT/30dn8NyT6WwMvdhK8E7H+HbUuHrWw/55QJXA4ybYpWXoSsS9V9T9baSKtsJlf7UyVIj/s67RIGWkkH06GWM3v0cG0pVB5kZWBMZ43b+1afpumgczVofIU48dJRyhm4cYHlY83UDC/B5Qg3l6uu5+BxjBahOK8R3Hk2e/s14fM19RZqPWSEYd7/9a7RWWiXm08a+67m262Z/dL7tmvErmVQSu9fZeG7Nffb2mPYaoz3X1g+Gd7HtfhMPMqA3HmdzPQOfB9p4tAPbaagaYACKmGJJmRUZf6S15AQ9Gy7YGXLxAlFyiXaTPmbKyXYctVWm2IZUE0sJjJaa2EsW/q7u6whbyoXpp1vTY8lbDDwQxyFbh9ujrRlB+GYF/J1he4/9emAkNzB6m2W7Xtu1YDwC6xj4Tju/YO3JCLg/kIK1384z2N+B/RGqTUElDKjtJdn8kwWaPRVCl4UTiBj4Dc2jjxAjV2ghabSQNCL/+SzNkvbQauJnDNgwiC9uPMZZfFLExCZUHCaKQiZRzhaqOI+bAjw4Abf6Mj0OXFyhnO0U8C45xFgBsGz8JdNNlC4OFpVuoHtDIQBjDjxMsyKxRnwXoQyyVJQz6xTKfrAHr0zQy0Q58+hOGV/Z0vrqV9zcooxV3KIXOTxmSSd7wtPP3ZEPCoWUMAY0HgrIo58VEjehb3VEqCKTj+oLpf+BfIZRzlY8lGjmZk+uOcU62EHJ5m9zWraLYKcFeCnCSSplbOQWKUEblcXtS567IbH+I0u9EICxDyC4ycLBDor5jELeIY9XyGMI+QyjgAkUs5BKduMkC2+1DdvmGPPbPcHaHHIcyMP3HexytlLIKHJ5xi9Jyr6jsIHujMxkdLA9FGDsg1XfATZS5F4ccx54Zn/grw6quEA5uyhlKSXMxcFHOJhbJyrRVNf77xWPXyKVMKvaB+Cl2ghYP9b2EQOjZu53sdet4Ysj97v8PwAYcbCeU8tRAAAAAElFTkSuQmCC";
