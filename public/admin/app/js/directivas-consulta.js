/**
* directivas Desarrolladas en la dem Module
* author: Jose Angel Delgado
* Description
* params: campos = [{title:'titulo',data:'nombre_data'}]
* params: data = [{'nombre_data':'valor'}]
* <consulta data="" campos="" />
**/
(function(){
  $.extend( $.fn.dataTableExt.oSort, {
      "date-ro-asc": function ( a, b ) {
          return ((a < b) ? -1 : ((a > b) ? 1 : 0));
      },
      "date-ro-desc": function ( a, b ) {
          return ((a < b) ? 1 : ((a > b) ? -1 : 0));
      }
  });
})();
(function(angular){
  angular.module('dem.directivas.consulta', [])
  .directive('consulta', ['$log','$compile', function($log,$compile){
    return {
      template: '<table class="table table-bordered"></table>',
      scope: {
        data : '=', campos : '=', fnCreatedRow:'=',
      }, // {} = isolate, true = child, false/undefined = no change
      restrict: 'E', // E = Element, A = Attribute, C = Class, M = Comment
      priority: 1,
      replace: true,
      transclude: true,
      link: function($scope, iElm, iAttrs, controller) {
        // console.info(iElm)
        // var $table = $(iElm.children()[0]);
        var $table = iElm;
        var databaseOpts = {};
        $scope.$watch('campos',function(){
          if($scope.campos)
          databaseOpts = {
              columns:$scope.campos ,
              // data:$scope.data || [],
              language:{
                "sProcessing": "Procesando...",
                "sLengthMenu": "Ver listado de _MENU_",
                "sZeroRecords": "No hay registros seleccionados",
                "sInfo": "_START_ al _END_ de _TOTAL_ registros",
                "sInfoEmpty": "0 al 0 de 0 registros",
                "sInfoFiltered": "(filtrado de _MAX_ registros)",
                "sInfoPostFix": "",
                "sSearch": "Filtrar : ",
                "sUrl": "",
                "oPaginate": {
                  "sFirst":    "Primero",
                  "sPrevious": "Anterior",
                  "sNext":     "Siguiente",
                  "sLast":     "\u00daltimo"
                }
              },
              lengthMenu:[[4,6,-1],[4,6,"Todo"]],
            }
          if($scope.fnCreatedRow) {
            databaseOpts.fnCreatedRow = $scope.fnCreatedRow;
          };
          $table.dataTable(databaseOpts);
        });
        $scope.$watch('data', function(newValue, oldValue, scope) {
          if ($scope.data.length > 0){
            if($table.fnGetData().length != 0){
              $table.fnClearTable();
            }
            $table.fnAddData($scope.data || []);
          }
        });
        var seleccionRowTabla = function(evt) {
          var tabla = evt.data.t;
          if ( $(this).hasClass('selected') ) {
            $(this).removeClass('selected');
          }
          else {
            tabla.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
          }
        }
        $table.on('click','tr',{t:$table},seleccionRowTabla)
        $table.on('dblclick','tr',function(e){
          var val = $table.fnGetData(this);
          $scope.$emit('evt::cosulta-traer',val);
        });
      }
    };
  }]);
})(angular);

