(function(angular){
 
  angular.module('#app.mantenimiento', [])
  .service('CrubSrv', ['$http','$log','$q',function ($http,$log,$q) {
    var prefix = window.prefixDomain;
    
    this.guardar_usuario = function(data){
      data.activo = data.activo?"t":"f";
      return $http.post(prefix + 'mantenimiento/guardarUsuario',data);
    }
    this.cambiarClave = function(data){
      return $http.post(prefix + 'mantenimiento/guardarNuevaClave',data);
    }
    this.cambiarMiClave = function(data){
      return $http.post(prefix + 'mantenimiento/guardarMiNuevaClave',data);
    }
    this.actualizar_usuario = function(data){
      data.activo = data.activo?"t":"f";
      return $http.post(prefix + 'mantenimiento/guardarUsuario',data);
    }
    this.buscar_usuario = function(data){
      return $http.post(prefix + 'mantenimiento/buscarUsuario',data);
    }
  }])
  .controller('UsuarioCtrl', ['$scope', 'CrubSrv','$timeout',function ($scope,UsuarioSrv,$timeout) {
    var acciones = $scope.acciones = ['Guardar','Modificar'];
    $scope.accion = acciones[0];
    $scope.mensajes = [];
    $scope.form = {};
    $scope.guardar = function(form){
      UsuarioSrv.guardar_usuario(form).success(function(data,status){
        if(status!=200){
          $scope.mensajes.push({type:'danger',text:'algo va mal :('});
        }else if(data.meta.code==-1){
          $scope.mensajes.push({type:'warning',text:'El usuario ya existe'});
        }else{
          $scope.form.id = data.data.id_user;
          $scope.mensajes.push({text:'Registro exitoso'});
        }
      });
      $timeout(function(){
          $scope.mensajes = [];
        },10000);
    }
    $scope.actualizar = function(form){
      UsuarioSrv.actualizar_usuario(form).success(function(data,status){
        if(status!=200){
          $scope.mensajes.push({type:'danger',text:'algo va mal :('});
        }else{
          $scope.mensajes.push({text:'Cambios exitoso!'});
        }
      });
      $timeout(function(){
          $scope.mensajes = [];
        },10000);
    }
    $scope.cambiarClave = function(form){
      if(form.password != $scope.password2){
        $scope.mensajes.push({type:'warning',text:'Las Contraseñas deben ser iguales'});
      }else{
        UsuarioSrv.cambiarClave({password:form.password,id:form.id}).success(function(res,status){
          if(status!=200){
            $scope.mensajes.push({type:'danger',text:'algo va mal :('});
          }else if(res.meta.code == 200){
            $scope.mensajes.push({text:'Cambio exitoso'});
          }
        });
      }
      $timeout(function(){
          $scope.mensajes = [];
        },10000);
    }
    $scope.cambiarMiClave = function(form){
      if(form.password != $scope.password2){
        $scope.mensajes.push({type:'warning',text:'Las Contraseñas deben ser iguales'});
      }else{
        UsuarioSrv.cambiarMiClave({password:form.password,password_actual:form.password_actual}).success(function(res,status){
          if(status!=200){
            $scope.mensajes.push({type:'danger',text:'algo va mal :('});
          }else if(res.meta.code == -1){
            $scope.mensajes.push({type:'danger',text:res.meta.msg});
          }else if(res.meta.code == 200){
            $scope.mensajes.push({text:'Cambio exitoso'});
          }
        });
      }
      $timeout(function(){
          $scope.mensajes = [];
        },10000);
    }
    $scope.buscar = function(form){
      UsuarioSrv.buscar_usuario(form).success(function(res,code){
        if(res.data.length==0){
          $scope.accion = acciones[0];
          $scope.mensajes.push({type:'warning',text:'No Se Encontro el Usuario'});
          $scope.seEncontro = false;
        }else{
          $scope.seEncontro = true;
          $scope.accion = acciones[1];
          $scope.form = res.data[0];
          $scope.form.activo = res.data[0].activo=="t";
        }
        $timeout(function(){
          $scope.mensajes = [];
        },10000);
      });
    };
    $scope.cancelarCambioUsuario = function(){
      $scope.seEncontro = false;
      $scope.form = {};
      $scope.accion = acciones[0];
    }
    $scope.atras = function(){
      window.location.back();
    }
  }]);
})(angular);