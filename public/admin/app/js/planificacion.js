(function(angular,prefix){
  angular.module('#app.planificacion', ["ui.bootstrap","dem.directivas.consulta"])
  .service('CrubSrv', ['$http','$log','$q',function ($http,$log,$q) {
    
    this.guardar_planificacion = function(data){
      return $http.post(prefix + 'planificacion/guardar',data);
    }
    this.eliminar = function(data){
      return $http.post(prefix + 'planificacion/eliminar',data);
    }
    this.actualizar_usuario = function(data){
      return $http.post(prefix + 'mantenimiento/guardarUsuario',data);
    }
  }])
  .controller('PlanificacionCtrl', ['$scope', 'CrubSrv',"$timeout","$modal",function ($scope,CrubSrv,$timeout,$modal) {
    $timeout(function(){
    	$('.datepicker').datepicker({
    		format: 'yyyy-mm-dd',
    		startDate: '-1d'
    	});
      $("#tabla_datatable").dataTable({
        "language":{
          "sProcessing": "Procesando...",
          "sLengthMenu": "Ver listado de _MENU_",
          "sZeroRecords": "No hay registros seleccionados",
          "sInfo": "_START_ al _END_ de _TOTAL_ registros",
          "sInfoEmpty": "0 al 0 de 0 registros",
          "sInfoFiltered": "(filtrado de _MAX_ registros)",
          "sInfoPostFix": "",
          "sSearch": "Filtrar : ",
          "sUrl": "",
          "oPaginate": {
            "sFirst":    "Primero",
            "sPrevious": "Anterior",
            "sNext":     "Siguiente",
            "sLast":     "\u00daltimo"
          }
        },
      });
    });
    
    $scope.mensajes = [];
    // $scope.form = {acciones:[{des_accion:"accion 1abcdfghtjklmnopk"},{des_accion:"accion 2"},{des_accion:"accion 3"}]};
    if(!$scope.form){
      $scope.form = {acciones:[]};
    }

    $scope.$watch("form.cant_fisica_mascara",function(nuevo){
      if(angular.isDefined(nuevo)) {
        $scope.form.cant_fisica = (nuevo.replace(".","")).replace(",",".");
      };
    }, true);

    $scope.guardar = function(form){
      CrubSrv.guardar_planificacion(form).success(function(data,status){
        if(status!=200){
          $scope.mensajes.push({type:'danger',text:'algo va mal :('});
        }else if(data.meta.code==-1){
          $scope.mensajes.push({type:'warning',text:'No se guardo la planificación! ' + data.meta.msg});
        }else{
          $scope.form = {acciones:[]};
          $scope.mensajes.push({text:'Registro exitoso'});
        }
        $timeout(function(){
          $scope.mensajes = [];
        },8000);
      });
    }
    $scope.actualizar = function(form){
      CrubSrv.guardar_planificacion(form).success(function(data,status){
        if(status!=200){
          $scope.mensajes.push({type:'danger',text:'algo va mal :('});
        }else if(data.meta.code==-1){
          $scope.mensajes.push({type:'warning',text:'No se actualizó la planificación! ' + data.meta.msg});
        }else{
          $scope.form = {acciones:[]};
          $scope.mensajes.push({text:'Edición exitosa'});
          window.history.back();
        }
        $timeout(function(){
          $scope.mensajes = [];
        },8000);
      });
    }
    $scope.eliminarAccion = function(index){
      $scope.form.acciones.splice(index,1);
    }
    // $scope.actualizar = function(form){
    //   CrubSrv.actualizar_usuario(form).success(function(data,status){
    //     if(status!=200){
    //       $scope.mensajes.push({type:'danger',text:'algo va mal :('});
    //     }if(data.meta.code==-1){
    //       $scope.mensajes.push({type:'warning',text:'No se modificó la planificación! ' + data.meta.msg});
    //     }else{
    //       $scope.form.id = res.data.id_user;
    //       $scope.mensajes.push({text:'Cambios exitoso!'});
    //     }
    //   });
    // }
    $scope.eliminar = function(id_plan){
      CrubSrv.eliminar({id_plan:id_plan}).success(function(data,status){
        if(status!=200){
          $scope.mensajes.push({type:'danger',text:'algo va mal :('});
        }if(data.meta.code==-1){
          $scope.mensajes.push({type:'warning',text:'No se pudo eliminar'});
        }else{
          $scope.mensajes.push({text:'Cambios exitoso!'});
          window.location.reload();
        }
      });
    }
    $scope.cortarAccion = function(palabra){
      palabra = palabra || "";
      var num = palabra.length;
      var res = palabra;
      if (num>20) {
        res = palabra.substring(0,20) + "..."
      }
      return res;
    }
    $scope.openModalAgregar = function(index){
      $modal.open({
        // size:'lg',
        templateUrl:prefix + './planificacion/modalAccion',
        controller:["$scope","$modalInstance","params",function($scope,$modalInstance,params){
          $scope.formModal = params.accion || {};
          $scope.item = params.item || -1;
          var const_total = params.total || 0;
          $scope.total = const_total;
          $scope.cancel = $modalInstance.dismiss;
          var array_suma = [];
          
          $scope.guardar = function(){
            $modalInstance.close($scope.formModal);
          }
          $scope.inspeccionarCantidad2 = function(){
            console.info("h");
          }
          $scope.inspeccionarCantidad = function(){
            var suma = 0;
            $scope.total = const_total;
            angular.forEach($scope.formModal.meses,function(item,index){
              if (item) {
                item = item.replace(".","").replace(",",".");
                suma = parseFloat(item)+parseFloat(suma);
              };
            });
            $scope.total = parseFloat($scope.total) - parseFloat(suma);
            $scope.formModal.meta_fisica = suma>0 ? suma : 0;
          }
          $scope.limpiarMeses = function(){
            angular.forEach($scope.formModal.meses,function(item,index){
              delete $scope.formModal.meses[index];
            });
          }

        }],
        resolve:{
          params:function(){ 
            if(index>=0){
              return {
                item:parseInt(index+1),accion:$scope.form.acciones[index],
                total:_getTotalActual()
              };
            }else{
              return {
                item:parseInt($scope.form.acciones.length+1),
                total:_getTotalActual()
              };
            }
          }
        }
      }).result.then(function(accion){
        if(index>=0){
          $scope.form.acciones[index] = accion;
        }else{
          $scope.form.acciones.push(accion);
        }
      });
      function _getTotalActual(){
        var total = $scope.form.cant_fisica;
        angular.forEach($scope.form.acciones,function(accion, index){
          total = total - accion.meta_fisica;
        });
        return total;
      }
    }
  }])
  .directive('jkeypress', ["$parse",function ($parse) {
    return function(scope, element, attr) {
      var fn = $parse(attr['jkeypress']);
      element.bind('keypress', function(event) {
        scope.$apply(function() {
          fn(scope, {$event:event});
        });
      });
    }
  }])
  .directive('toneladas', [function () {
    return {
      priority: 0,
      restrict: 'C',
      link: function postLink(scope, iElement, iAttrs) {
     // $(".toneladas")
        iElement.inputmask("decimal",{
           radixPoint:",", 
           groupSeparator: ".", 
           // digits: 2,
           autoGroup: true,
       });
      }
    };
  }])
})(angular,window.prefixDomain);

// ####################JQUERY
