
$(document).ready(function() {
    window.urlTool = {
        
        baseUrl: '',
        i: function(action) {
            return this.baseUrl + action;
        }
    }

    // 提醒 工具类
    window.hinotify = {
        info: function(msg) {
            this.show(msg, 'hinotify-info');
        },

        error: function(msg) {
            this.show( '错误: ' + msg, 'hinotify-error');
        },

        show: function(msg, type) {
            $('body').append('<span class="hinotify-item ' + type + '">' + msg + '</span>');
            $('.hinotify-item').fadeIn(500);

            setTimeout(function() {
                $('.hinotify-item').fadeOut(500, function() {
                    $('.hinotify-item').remove();
                });
            }, 2000);
        }
    }

    // 1. 文字相关
    window.Article = {};

    // 保存文字
    Article.save = function() {

        // 判断是否定时调用
        var isAutoCall = false;
        if(arguments.length > 0) {
            isAutoCall = arguments[0];
        }

        var title = $.trim($('.article-title-input').val());
        var content = $.trim($('.article-content-input').val());
        var subDomain = $('.article-sub-domain option:selected').val();
        var articleIdEle = $('#cur-article-id');
        var articleId = parseInt(articleIdEle.html());

        if(title == '' || content == '' || subDomain == '') {
            hinotify.error('参数不完整');
            return ;
        }

        $.post(urlTool.i('article/save'), {
            title: title,
            content: content,
            sub_domain: subDomain,
            article_id: articleId
        }, function(res) {
            console.log(res);
            res = $.parseJSON(res);
            if(res.meta.code != 200) {
                return ;
            }

            articleIdEle.html(res.data.article_id);

            // 定时保存处理
            if(isAutoCall) {
                hinotify.info(res.data.article_id + ': 已保存');
            }else {
                hinotify.info(res.data.article_id + ': 已保存');
            }
        });
    }


});